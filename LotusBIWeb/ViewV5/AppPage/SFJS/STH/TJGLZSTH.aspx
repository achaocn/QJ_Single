﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TJGLZSTH.aspx.cs" ValidateRequest="false" Inherits="LotusBIWeb.TJGLZSTH" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <link href="/ViewV5/CSS/bootstrap3.3.5/css/bootstrap.css" rel="stylesheet" />
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        body {
            font-size: 14px;
            text-align: center;
            color: #000;
        }

        li {
            list-style: none;
        }

        #pageGro {
            width: 450px;
            height: 25px;
            margin: 0 auto;
            padding-top: 30px;
        }

            #pageGro div, #pageGro div ul li {
                font-size: 12px;
                color: #999;
                line-height: 23px;
                float: left;
                margin-left: 5px;
            }

                #pageGro div ul li {
                    width: 22px;
                    text-align: center;
                    border: 1px solid #999;
                    cursor: pointer;
                }

                    #pageGro div ul li.on {
                        color: #fff;
                        background: #3c90d9;
                        border: 1px solid #3c90d9;
                    }

            #pageGro .pageUp, #pageGro .pageDown, #pageGro .pagestart, #pageGro .pageend {
                width: 63px;
                border: 1px solid #999;
                cursor: pointer;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid maindiv" style="margin-top: 20px" id="dvContent">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-9">
                            <div class="input-group">
                                <div class="input-group-addon">用水时间</div>
                                <asp:TextBox ID="txtStart" Width="20%" runat="server" onfocus="this.blur()" CssClass="form-control form_date"></asp:TextBox>
                                <asp:TextBox ID="txtEnd" Width="20%" runat="server" onfocus="this.blur()" CssClass="form-control form_date"></asp:TextBox>
                                <asp:DropDownList runat="server" Style="width: 15%" class="form-control hidden" ID="DDLGJ">
                                    <asp:ListItem Text="请选择灌季" Value="" />
                                    <asp:ListItem Text="自流" />
                                    <asp:ListItem Text="南干自流" />
                                    <asp:ListItem Text="秋" />
                                    <asp:ListItem Text="冬" />
                                </asp:DropDownList>
                                <asp:DropDownList runat="server" Style="width: 15%" class="form-control" ID="ddlJQ">
                                </asp:DropDownList>
                                <asp:DropDownList runat="server" Style="width: 15%" class="form-control" ID="ddlzfzt">
                                    <asp:ListItem Text="请选择支付状态" Value="" />
                                    <asp:ListItem Text="未支付"  Value="未支付" />
                                    <asp:ListItem Text="已支付" Value="已支付" />
                                </asp:DropDownList>
                                   <asp:DropDownList runat="server" Style="width: 15%" class="form-control" ID="ddlzffs">
                                    <asp:ListItem Text="请选择支付方式" Value="" />
                                    <asp:ListItem Text="微信支付"  Value="微信支付" />
                                    <asp:ListItem Text="手动支付" Value="手动支付" />
                                </asp:DropDownList>
                                <span class="input-group-btn">
                                    <%--                                    <input type="button" value="重置申请时间" class="btn btn-success resetdate" />--%>
                                    <asp:Button Text="查  询" ID="btQuery" runat="server" CssClass="btn btn-info" OnClick="btQuery_Click" />
                                </span>
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <asp:Button ID="btExport" runat="server" Text="导出" CssClass="btn btn-success" OnClick="btExport_Click" />
                            <a class="btn btn-info" onclick="Preview()">打印</a>

                        </div>
                    </div>
                </div>


            </div>
            <div id="princontent">

                <table id="FTGrid" class="table table-hover table-striped table-bordered table-responsive" style="margin-bottom: 0;">
                    <thead>
                        <tr>
                            <th colspan="37" style="text-align: center; font-size: 24px"><%=TJDATA.DCTITLE %> 
                            </th>

                        </tr>
                        <tr>
                            <th colspan="37" style="text-align: right; font-size: 14px">统计时间：<%=TJDATA.TJDATE %>
                            </th>
                        </tr>
                        <tr>
                            <th rowspan="4">
                                <div class="th-inner ">序号</div>
                            </th>
                            <th rowspan="4">
                                <div class="th-inner ">管理站</div>
                            </th>
                            <th rowspan="4">
                                <div class="th-inner ">价区</div>
                            </th>


                            <th colspan="11" style="text-align: center">
                                <div class="th-inner ">粮油作物</div>
                            </th>
                            <th colspan="11" style="text-align: center">
                                <div class="th-inner ">经济作物</div>
                            </th>
                            <th colspan="3" rowspan="3" style="text-align: center">
                                <div class="th-inner ">水费(元)</div>
                            </th>

                        </tr>
                        <tr>



                            <th rowspan="3">
                                <div class="th-inner ">亩数</div>
                            </th>

                            <th colspan="3" rowspan="2" style="text-align: center">
                                <div class="th-inner ">用水量</div>
                            </th>
                            <th colspan="3" style="text-align: center">
                                <div class="th-inner ">终端水价</div>
                            </th>
                            <th colspan="4" style="text-align: center">
                                <div class="th-inner ">水费</div>
                            </th>
                            <th rowspan="3">
                                <div class="th-inner ">亩数</div>
                            </th>

                            <th colspan="3" rowspan="2" style="text-align: center">
                                <div class="th-inner ">用水量</div>
                            </th>
                            <th colspan="3" style="text-align: center">
                                <div class="th-inner ">终端水价</div>
                            </th>
                            <th colspan="4" style="text-align: center">
                                <div class="th-inner ">水费</div>
                            </th>

                        </tr>

                        <tr>

                            <th rowspan="2">
                                <div class="th-inner ">国有水价</div>
                            </th>
                            <th colspan="2">
                                <div class="th-inner">末级渠系费用</div>
                            </th>

                            <th rowspan="2">
                                <div class="th-inner ">国有水费</div>
                            </th>
                            <th colspan="2">
                                <div class="th-inner ">末级渠系费用	</div>
                            </th>
                            <th rowspan="2">
                                <div class="th-inner ">小计</div>
                            </th>
                            <th rowspan="2">
                                <div class="th-inner ">国有水价</div>
                            </th>
                            <th colspan="2">
                                <div class="th-inner">末级渠系费用</div>
                            </th>

                            <th rowspan="2">
                                <div class="th-inner ">国有水费</div>
                            </th>
                            <th colspan="2">
                                <div class="th-inner ">末级渠系费用</div>
                            </th>
                            <th rowspan="2">
                                <div class="th-inner ">小计</div>
                            </th>

                        </tr>
                        <tr>


                            <th>
                                <div class="th-inner ">一档</div>
                            </th>
                            <th>
                                <div class="th-inner ">二档</div>
                            </th>

                            <th>
                                <div class="th-inner ">小计</div>
                            </th>


                            <th>
                                <div class="th-inner ">一档</div>
                            </th>
                            <th>
                                <div class="th-inner ">二档</div>
                            </th>

                            <th>
                                <div class="th-inner ">一档</div>
                            </th>
                            <th>
                                <div class="th-inner ">二档</div>
                            </th>


                            <th>
                                <div class="th-inner ">一档</div>
                            </th>
                            <th>
                                <div class="th-inner ">二档</div>
                            </th>

                            <th>
                                <div class="th-inner ">小计</div>
                            </th>

                            <th>
                                <div class="th-inner ">一档</div>
                            </th>
                            <th>
                                <div class="th-inner ">二档</div>
                            </th>

                            <th>
                                <div class="th-inner ">一档</div>
                            </th>
                            <th>
                                <div class="th-inner ">二档</div>
                            </th>

                            <th>
                                <div class="th-inner ">国有水费</div>
                            </th>
                            <th>
                                <div class="th-inner ">末级渠系费用</div>
                            </th>
                            <th>
                                <div class="th-inner ">合计</div>
                            </th>
                        </tr>

                    </thead>
                    <tbody id="tbdata" class="news">
                        <asp:Repeater runat="server" ID="repData">
                            <ItemTemplate>
                                <tr>

                                    <td style=""><%# Container.ItemIndex + 1%> </td>
                                    <td><%#Eval("RootName") %></td>
                                    <td style=""><%#Eval("JQName") %></td>
                                    <td style=""><%#Eval("LYMS") %></td>
                                    <td style=""><%#Eval("LYSL1") %></td>
                                    <td style=""><%#Eval("LYSL2") %></td>

                                    <td style=""><%#Eval("LYSLXJ") %></td>
                                    <td style=""><%#Eval("LYGYSJ") %></td>
                                    <td style=""><%#Eval("LYSJ1") %></td>
                                    <td style=""><%#Eval("LYSJ2") %></td>
                                    <td style=""><%#Eval("LYSF0") %></td>
                                    <td style=""><%#Eval("LYSF1") %></td>
                                    <td style=""><%#Eval("LYSF2") %></td>
                                    <td style=""><%#Eval("LYSFXJ") %></td>
                                    <td style=""><%#Eval("JJMS") %></td>
                                    <td style=""><%#Eval("JJSL1") %></td>
                                    <td style=""><%#Eval("JJSL2") %></td>

                                    <td style=""><%#Eval("JJSLXJ") %></td>
                                    <td style=""><%#Eval("JJGYSJ") %></td>
                                    <td style=""><%#Eval("JJSJ1") %></td>
                                    <td style=""><%#Eval("JJSJ2") %></td>
                                    <td style=""><%#Eval("JJSF0") %></td>
                                    <td style=""><%#Eval("JJSF1") %></td>
                                    <td style=""><%#Eval("JJSF2") %></td>
                                    <td style=""><%#Eval("JJSFXJ") %></td>
                                    <td style=""><%#Eval("GYSF") %></td>
                                    <td style=""><%#Eval("QJSF") %></td>
                                    <td style=""><%#Eval("ZJE") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>


                    </tbody>
                </table>
                <input type="hidden" id="start_page" />
                <input type="hidden" id="current_page" />
                <input type="hidden" id="show_per_page" />
                <input type="hidden" id="end_page" />

                <div id="pageGro" class="cb" style="display: none">
                    <div class="pagestart">首页</div>
                    <div class="pageUp">上一页</div>
                    <div class="pageList">
                        <ul>
                            <li>1</li>
                            <li>2</li>
                            <li>3</li>
                            <li>4</li>
                            <li>5</li>
                        </ul>
                    </div>
                    <div class="pageDown">下一页</div>
                    <div class="pageend">尾页</div>



                    <!-- Default panel contents -->

                    <!-- Table -->


                    <input type="hidden" id="tabledata" value="" runat="server" />
                    <input type="hidden" id="hidZID" value="" runat="server" />
                    <input type="hidden" id="hidZName" value="" runat="server" />

                </div>


            </div>
    </form>
    <script type="text/javascript" src="/ViewV5/JS/jquery-1.11.2.min.js"></script>
    <script src="/ViewV5/JS/jquery-migrate-1.1.0.min.js"></script>
    <script type="text/javascript" src="/ViewV5/CSS/bootstrap3.3.5/js/bootstrap.js"></script>
    <script src="/ViewV5/JS/layer/layer.js"></script>
    <script src="/ViewV5/JS/Print.js"></script>
    <script src="/ViewV5/JS/SZHLCommon.js"></script>

    <script>
        $(function () {
            //var show_per_page = 15; var number_of_items = $('.news').children().size(); var pageCount = Math.ceil(number_of_items / show_per_page); $('.news').children().css('display', 'none'); $('.news').children().slice(0, show_per_page).css('display', 'table-row'); $('#start_page').val(0); $('#current_page').val(0); $('#show_per_page').val(show_per_page); $('#end_page').val(pageCount); if (pageCount > 5) { page_icon(1, 5, 0); } else { page_icon(1, pageCount, 0); }
            //$("#pageGro li").live("click", function () { var pageNum = parseInt($(this).html()) - 1; var page = pageNum + 1; var show_per_page = parseInt($('#show_per_page').val()); start_from = pageNum * show_per_page; end_on = start_from + show_per_page; $('.news').children().css('display', 'none').slice(start_from, end_on).css('display', 'table-row'); if (pageCount > 5) { pageGroup(page, pageCount); } else { $(this).addClass("on"); $(this).siblings("li").removeClass("on"); } }); $("#pageGro .pageUp").click(function () {
            //    var pageNum = parseInt($("#pageGro li.on").html()); if (pageNum <= 1) { var page = pageNum; } else { var page = pageNum - 1; }
            //    var show_per_page = parseInt($('#show_per_page').val()); start_from = page * show_per_page - show_per_page; end_on = start_from + show_per_page; $('.news').children().css('display', 'none').slice(start_from, end_on).css('display', 'table-row'); if (pageCount > 5) { pageUp(pageNum, pageCount); } else { var index = $("#pageGro ul li.on").index(); if (index > 0) { $("#pageGro li").removeClass("on"); $("#pageGro ul li").eq(index - 1).addClass("on"); } }
            //}); $("#pageGro .pageDown").click(function () {
            //    var pageNum = parseInt($("#pageGro li.on").html()); var page = pageNum; if (pageNum === pageCount) { page = pageNum - 1; }
            //    var show_per_page = parseInt($('#show_per_page').val()); start_from = page * show_per_page; end_on = start_from + show_per_page; $('.news').children().css('display', 'none').slice(start_from, end_on).css('display', 'table-row'); if (pageCount > 5) { pageDown(pageNum, pageCount); } else { var index = $("#pageGro ul li.on").index(); if (index + 1 < pageCount) { $("#pageGro li").removeClass("on"); $("#pageGro ul li").eq(index + 1).addClass("on"); } }
            //}); $("#pageGro .pagestart").live("click", function () { var pageNum = $('#start_page').val(); start_from = pageNum * show_per_page; end_on = start_from + show_per_page; $('.news').children().css('display', 'none').slice(start_from, end_on).css('display', 'table-row'); if (pageCount > 5) { pageGroup(1, pageCount); } else { var index = $("#pageGro ul li.on").index(); if (index < pageCount) { $("#pageGro li").removeClass("on"); $("#pageGro ul li:first").addClass("on"); } } }); $("#pageGro .pageend").live("click", function () {
            //    var pageNum1 = $('#end_page').val(); var pagenum = pageNum1 - 2
            //    var page = pageNum1 - 1; start_from = page * show_per_page; end_on = start_from + show_per_page; $('.news').children().css('display', 'none').slice(start_from, end_on).css('display', 'table-row'); if (pageCount > 5) { pageGroup(pagenum, pageNum1); $("#pageGro ul li:last-child").addClass("on").siblings().removeClass("on"); } else { var index = $("#pageGro ul li.on").index(); if (index < pageCount) { $("#pageGro li").removeClass("on"); $("#pageGro ul li:last-child").addClass("on"); } }
            //});
        }); function pageGroup(pageNum, pageCount) { switch (pageNum) { case 1: page_icon(1, 5, 0); break; case 2: page_icon(1, 5, 1); break; case pageCount - 1: page_icon(pageCount - 4, pageCount, 3); break; case pageCount: page_icon(pageCount - 4, pageCount, 4); break; default: page_icon(pageNum - 2, pageNum + 2, 2); break; } }
        function page_icon(page, count, eq) {
            var ul_html = ""; for (var i = page; i <= count; i++) { ul_html += "<li>" + i + "</li>"; }
            $("#pageGro ul").html(ul_html); $("#pageGro ul li").eq(eq).addClass("on");
        }
        function pageUp(pageNum, pageCount) { switch (pageNum) { case 1: break; case 2: page_icon(1, 5, 0); break; case pageCount - 1: page_icon(pageCount - 4, pageCount, 2); break; case pageCount: page_icon(pageCount - 4, pageCount, 3); break; default: page_icon(pageNum - 2, pageNum + 2, 1); break; } }
        function pageDown(pageNum, pageCount) { switch (pageNum) { case 1: page_icon(1, 5, 1); break; case 2: page_icon(1, 5, 2); break; case pageCount - 1: page_icon(pageCount - 4, pageCount, 4); break; case pageCount: break; default: page_icon(pageNum - 2, pageNum + 2, 3); break; } }
        function Preview() {
            $("#princontent").printThis({
                debug: false,
                importCSS: true,
                importStyle: true,
                printContainer: true,
                loadCSS: "/View/CSS/bootstrap3.3.5/css/bootstrap.css",
                pageTitle: "通知公告",
                removeInline: false,
                printDelay: 333,
                header: null,
                formValues: true
            });
        };
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($(".form_date")[0]) {
                $(".form_date").datetimepicker({
                    format: "yyyy-mm-dd"
                });
                $(".icon-arrow-right").addClass("glyphicon glyphicon-chevron-right")
                $(".icon-arrow-left").addClass("glyphicon glyphicon-chevron-left")
            }

        });

    </script>

</body>
</html>
