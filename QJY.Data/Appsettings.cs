﻿
using System;
using System.Configuration;

namespace QJY.Data
{
    /// <summary>
    /// appsettings.json操作类
    /// </summary>
    public class Appsettings
    {
        
        /// <summary>
        /// 封装要操作的字符
        /// </summary>
        /// <param name="sections"></param>
        /// <returns></returns>
        public static string app(params string[] sections)
        {
            try
            {
                string val = string.Empty;
                //val = ConfigurationManager.ConnectionStrings[sections[0]].ToString();
                val = ConfigurationManager.AppSettings[sections[0]].ToString();
                return val;
            }
            catch (Exception)
            {
                return "";
            }

        }
    }
}