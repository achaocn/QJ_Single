﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;


namespace QJY.API
{
    /// <summary>
    /// 不需要验证即可使用的接口
    /// </summary>
    public class JWUSERManage
    {

        /// <summary>
        /// 根据学号获取待评教课程
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        [Description("Student")]
        public void GETPJKC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strxh = P1;
            XsInfo xs = new JWHelp().GetXSInfo(strxh);
            DataTable dtQX = new JWHelp().GetCZQX("ZLPJ001");
            if (dtQX.Rows.Count == 0)
            {
                msg.ErrorMsg = "学生评教功能未开启!";
                return;
            }
            else
            {
                string xq = dtQX.Rows[0]["xq"].ToString();
                if (xs != null)
                {
                    DataTable dtPJKC = new QJ_JWB().GetDTByCommand("select KBK_JSMC.IDN,KBK.XQ,KBK.BH,KBK.BJ,KBK.KCDM,KBK_JSMC.JSMC,KBK_JSMC.JSDM,KBK.KCMC,KBK.LBDH,BJ.XBDM,dbo.fn_xbdm_xbmc(BJ.XBDM) XBMC,BJ.NJ,(case KBK_JSMC.SFPJ  when '2' then '参加评教'  else '不参加评教'  end ) ISPJ FROM KBK_JSMC INNER JOIN KBK ON KBK_JSMC.XQ = KBK.xq and  KBK_JSMC.bh = KBK.bh and  KBK_JSMC.kcdm = KBK.kcdm INNER JOIN BJ ON KBK_JSMC.BH = BJ.BH WHERE KBK_JSMC.BH=@bh and  KBK_JSMC.xq=@xq and  KBK_JSMC.SFPJ='2'", new { bh = xs.bh, xq = xq });
                    dtPJKC.Columns.Add("dataid");
                    for (int i = 0; i < dtPJKC.Rows.Count; i++)
                    {
                        string strbh = dtPJKC.Rows[i]["bh"].ToString();
                        string jsdm = dtPJKC.Rows[i]["jsdm"].ToString();
                        string kcdm = dtPJKC.Rows[i]["kcdm"].ToString();

                        DataTable dtPJData = new QJ_JWB().GetDTByCommand("select * from qj_xspjdata where bh=@bh and  xq=@xq and  jsdm=@jsdm  and  kcdm=@kcdm  and  xh=@xh ", new { bh = strbh, xq = xq, jsdm = jsdm, kcdm = kcdm, xh = xs.xh });
                        if (dtPJData.Rows.Count > 0)
                        {
                            dtPJKC.Rows[i]["dataid"] = dtPJData.Rows[0]["ID"].ToString();
                        }
                        else
                        {
                            dtPJKC.Rows[i]["dataid"] = "0";
                        }
                    }
                    msg.Result = dtPJKC;
                    msg.Result1 = dtQX;
                }
            }

        }


        /// <summary>
        /// 学生获取评价信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        [Description("Student")]
        public void INITPJZB(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strxh = P1;
            XsInfo xs = new JWHelp().GetXSInfo(strxh);
            string xq = P2;
            string dataid = context.Request("dataid");

            string strType = "三年高职起点";
            if (!xs.sylb.Contains("三年高职"))
            {
                strType = "初中起点";
            }
            if (xs != null)
            {
                msg.Result1 = new JH_Auth_ZiDianB().GetEntities(D => D.Class == "指标");

                if (dataid != "0")
                {
                    DataTable dtPJData = new QJ_JWB().GetDTByCommand("select * from qj_xspjdata where id=@id", new { id = dataid });
                    msg.Result2 = dtPJData;
                }
                else
                {
                    DataTable dtPJKC = new QJ_JWB().GetDTByCommand("select pjnr,yjzbmc,qz,zdpx,xq,idn from pj_zbxx where kclxdm='" + strType + "' and dxdm='学生评教' and xq=@xq order by zdpx", new { xq = xq });
                    msg.Result = dtPJKC;
                }
            }
        }



        /// <summary>
        /// 保存学生评价信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        [Description("Student")]
        public void SAVEPJDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strxh = P1;
            XsInfo xs = new JWHelp().GetXSInfo(strxh);
            //DataTable dtQX = new JWHelp().GetCZQX("ZLPJ001");
            //if (dtQX.Rows.Count == 0)
            //{
            //    msg.ErrorMsg = "您目前没有权限进行学生评教!";
            //    return;
            //}
            //else
            //{

            //}
            string rwid = P2;
            string pjdata = context.Request("pjdata");
            string zf = context.Request("zf", "0");
            string dataid = context.Request("dataid", "0");
            string pjsm = context.Request("pjsm", "0");

            
            if (xs != null)
            {
                DataTable dtRWData = new QJ_JWB().GetDTByCommand("select * from KBK_JSMC where idn=@id", new { id = rwid });
                if (dtRWData.Rows.Count > 0)
                {
                    string kcdm = dtRWData.Rows[0]["kcdm"].ToString();
                    string kcmc = "";
                    string jsmc = dtRWData.Rows[0]["jsmc"].ToString();
                    string jsdm = dtRWData.Rows[0]["jsdm"].ToString();
                    string bh = dtRWData.Rows[0]["bh"].ToString();
                    string xq = dtRWData.Rows[0]["xq"].ToString();


                    string strCon = new QJ_JWB().GetDBString();
                    DBFactory db = new DBFactory(strCon);
                    var dt = new Dictionary<string, object>();
               
                    dt.Add("xh", xs.xh);
                    dt.Add("xm", xs.xm);
                    dt.Add("jsmc", jsmc);
                    dt.Add("zf", zf);
                    dt.Add("pjmx", pjdata);
                    dt.Add("kcdm", kcdm);
                    dt.Add("kcmc", kcmc);
                    dt.Add("pjid", rwid);
                    dt.Add("jsdm", jsdm);
                    dt.Add("pjtime", DateTime.Now.ToString());
                    dt.Add("bh", bh);
                    dt.Add("xq", xq);
                    dt.Add("pjsm", pjsm);

                    
                    if (dataid != "0")
                    {
                        dt.Add("id", dataid);
                        db.UpdateData(dt, "qj_xspjdata");

                    }
                    else
                    {
                        dt.Add("CRUser", xs.xh);
                        dt.Add("DCode", xs.xbdm);
                        dt.Add("DName", xs.xbmc);
                        dt.Add("CRUserName", xs.xm);
                        dt.Add("CRDate", DateTime.Now.ToString());
                        dt.Add("intProcessStanceid", "0");
                        dt.Add("ComID", "10334");
                        db.InserData(dt, "qj_xspjdata");

                    }
                }

            }
        }



    }
}