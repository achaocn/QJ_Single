﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;

namespace QJY.API
{
    //成绩管理
    public class CJGLManage
    {



        #region PC端



        public void GETCJDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {





            string strproname = "pr_cjsr_xsmd";


            JArray arrprojc = new JArray();
            arrprojc.Add(new JObject() { { "@_whfs", "检查成绩输入状态" } });
            arrprojc.Add(new JObject() { { "@_kclx", context.Request("kclx") } });
            arrprojc.Add(new JObject() { { "@_xq", context.Request("xq") } });
            arrprojc.Add(new JObject() { { "@_bh", context.Request("bh") } });
            arrprojc.Add(new JObject() { { "@_kcdm", context.Request("kcdm") } });
            arrprojc.Add(new JObject() { { "@_kch", context.Request("kch") } });
            arrprojc.Add(new JObject() { { "@_jsmc", context.Request("jsmc") } });
            arrprojc.Add(new JObject() { { "@_ksxzm", int.Parse(context.Request("ksxzm")) } });
            DataTable dtjc = new QJ_JWB().GetDataTableByPR(strproname, arrprojc);
            string iscansr = "Y";
            if (dtjc.Rows.Count > 0)
            {
                if (dtjc.Rows[0]["cjsrzt"].ToString() == "42" || dtjc.Rows[0]["cjsrzt"].ToString() == "43")
                {
                    iscansr = "N";//检查
                }
            }

            JArray arrpro = new JArray();
            arrpro.Add(new JObject() { { "@_whfs", "查询名单" } });
            arrpro.Add(new JObject() { { "@_kclx", context.Request("kclx") } });
            arrpro.Add(new JObject() { { "@_xq", context.Request("xq") } });
            arrpro.Add(new JObject() { { "@_bh", context.Request("bh") } });
            arrpro.Add(new JObject() { { "@_kcdm", context.Request("kcdm") } });
            arrpro.Add(new JObject() { { "@_kch", context.Request("kch") } });
            arrpro.Add(new JObject() { { "@_jsmc", context.Request("jsmc") } });
            arrpro.Add(new JObject() { { "@_ksxzm", int.Parse(context.Request("ksxzm")) } });
            DataTable dt = new QJ_JWB().GetDataTableByPR(strproname, arrpro);


            if (context.Request("kclx")=="补考")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                }
            }
           
            dt.Columns.Add("qjdrzt");
            dt.Columns.Add("qjdrmsg");

            msg.Result = dt;
            msg.Result1 = iscansr;//是否能输入成绩
        }


        public void SAVECJDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string m_cjsrzt = P2;
            JArray dt = JsonConvert.DeserializeObject(P1) as JArray;
            int m_cjhs = 0;
            //统计总评成绩为空或0的个数
            int m_cjwt = 0;

            if (m_cjsrzt == "42")
            {
                foreach (JObject item in dt)
                {
                    string bklb = (string)item["bklb"];
                    if (bklb != "" && !bklb.Contains("正常") & !bklb.Contains("非正常"))
                    {
                        msg.ErrorMsg = "补考类别只能是正常补考或非正常补考！！";
                        return;
                    }
                }
            }

            foreach (JObject item in dt)
            {

                JArray arrpro = new JArray();
                string strkscj = (string)item["kscj"];
                if (string.IsNullOrEmpty(strkscj))
                {
                    strkscj = "0";
                }
                arrpro.Add(new JObject() { { "@_whfs", "检查成绩格式NEW" } });
                arrpro.Add(new JObject() { { "@_xq", context.Request("xq") } });
                arrpro.Add(new JObject() { { "@_kclx", context.Request("kclx") } });
                arrpro.Add(new JObject() { { "@_ksxzm", int.Parse(context.Request("ksxzm")) } });
                arrpro.Add(new JObject() { { "@_xh", (string)item["xh"] } });
                arrpro.Add(new JObject() { { "@_idn", (string)item["idn"] } });
                arrpro.Add(new JObject() { { "@_pscj", (string)item["cpscj"] } });
                arrpro.Add(new JObject() { { "@_qzcj", (string)item["cqzcj"] } });
                arrpro.Add(new JObject() { { "@_qmcj", (string)item["cqmcj"] } });
                arrpro.Add(new JObject() { { "@_kscj", (string)item["kscj"] } });
                arrpro.Add(new JObject() { { "@_bklb", (string)item["bklb"] } });
                DataTable dtJC = new QJ_JWB().GetDataTableByPR("pr_cjsr_cjmd_bccj_bklb", arrpro);
                if (dtJC.Rows[0]["message"].ToString() == "成绩输入格式有问题")
                {
                    item["qjdrmsg"] = "成绩不符合要求：" + dtJC.Rows[0]["qcj"].ToString();
                    item["qjdrzt"] = "N";
                    m_cjhs = m_cjhs + 1;
                }
            }
            if (m_cjhs != 0)
            {
                msg.Result = dt;
                msg.Result1 = "发现" + m_cjhs + "个成绩输入格式有问题";
            }
            else
            {
                int succount = 0;
                string _whfs = "";
                if (m_cjsrzt == "41")
                {
                    _whfs = "保存成绩NEW";
                }
                if (m_cjsrzt == "42")
                {
                    _whfs = "提交成绩NEW";
                }
                foreach (JObject item in dt)
                {

                    //未发现问题成绩
                    JArray arrpro = new JArray();
                    arrpro.Add(new JObject() { { "@_whfs", _whfs } });
                    arrpro.Add(new JObject() { { "@_xq", context.Request("xq") } });
                    arrpro.Add(new JObject() { { "@_kclx", (string)item["kclx"].ToString() } });
                    arrpro.Add(new JObject() { { "@_ksxzm", int.Parse(context.Request("ksxzm")) } });
                    arrpro.Add(new JObject() { { "@_xh", (string)item["xh"].ToString() } });
                    arrpro.Add(new JObject() { { "@_idn", (string)item["idn"] } });
                    arrpro.Add(new JObject() { { "@_pscj", (string)item["cpscj"] } });
                    arrpro.Add(new JObject() { { "@_qzcj", (string)item["cqzcj"] } });
                    arrpro.Add(new JObject() { { "@_qmcj", (string)item["cqmcj"] } });
                    arrpro.Add(new JObject() { { "@_kscj", (string)item["kscj"] } });
                    arrpro.Add(new JObject() { { "@_bklb", (string)item["bklb"] } });
                    DataTable dtBC = new QJ_JWB().GetDataTableByPR("pr_cjsr_cjmd_bccj_bklb", arrpro);
                    if (int.Parse(dtBC.Rows[0]["rong"].ToString()) == 0)
                    {
                        succount = succount + 1;
                        item["qjdrmsg"] = "导入成功";
                        item["qjdrzt"] = "Y";
                    }

                    if (string.IsNullOrEmpty((string)item["kscj"]) && m_cjsrzt == "42")
                    {

                        item["qjdrzt"] = "N";
                        item["qjdrmsg"] = "总评成绩中不能输入0或负数或空值，特殊情况请按本界面右角上的成绩计分代码对照表中的成绩类别输入或转换！";
                        m_cjwt = m_cjwt + 1;
                    }
                }

                /// 保存成绩输入状态
                /// 
                string strKCDM = context.Request("kcdm");
                if (context.Request("kclx").IndexOf("选项") < 0)  ///非选项课程，存放成绩比例
                {
                    strKCDM = context.Request("cjbl"); ///平时、期中、期末成绩比例

                }
                if (m_cjsrzt == "42" && m_cjwt != 0)
                {
                    msg.Result = dt;
                    msg.Result1 = "发现" + m_cjwt + "个成绩输入格式有问题,无法完成成绩提交操作,请检查后再提交!";

                    return;
                }
                JArray arrprobczt = new JArray();
                arrprobczt.Add(new JObject() { { "@_whfs", "保存成绩输入状态" } });
                arrprobczt.Add(new JObject() { { "@_xq", context.Request("xq") } });
                arrprobczt.Add(new JObject() { { "@_kclx", context.Request("kclx") } });
                arrprobczt.Add(new JObject() { { "@_ksxzm", m_cjsrzt } });
                arrprobczt.Add(new JObject() { { "@_bh", context.Request("bh") } });
                arrprobczt.Add(new JObject() { { "@_kcdm", strKCDM } });
                arrprobczt.Add(new JObject() { { "@_kch", context.Request("kch") } });
                arrprobczt.Add(new JObject() { { "@_jsmc", context.Request("jsmc") } });
                DataTable dtBCZT = new QJ_JWB().GetDataTableByPR("pr_cjsr_xsmd", arrprobczt);
                msg.Result = dt;
                msg.Result1 = "已成功提交" + dt.Count + "条记录";

            }

        }




        /// <summary>
        /// 获取成绩接收数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETCJJSDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            JArray arrprojc = new JArray();
            arrprojc.Add(new JObject() { { "@_whfs", "查询所属教师信息ALL" } });
            arrprojc.Add(new JObject() { { "@_jsdm", "" } });
            arrprojc.Add(new JObject() { { "@_xq", context.Request("xq") } });
            arrprojc.Add(new JObject() { { "@_idn", 0 } });
            arrprojc.Add(new JObject() { { "@_jsdmlist", context.Request("kclx") } });
            DataTable dtjc = new QJ_JWB().GetDataTableByPR("pr_cjsr_jccjsr_new", arrprojc);

            if (context.Request("xbdm") != "")
            {
                dtjc = dtjc.Where("xsxbdm='" + context.Request("xbdm") + "'");
            }
            if (context.Request("srzt") != "-1")
            {
                dtjc = dtjc.Where("cjsrqk='" + context.Request("srzt") + "'");
            }
            msg.Result = dtjc;

        }


        /// <summary>
        /// 获取成绩明细数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETCJMXDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            JObject dt = JsonConvert.DeserializeObject(P1) as JObject;

            JArray arrprojc = new JArray();
            arrprojc.Add(new JObject() { { "@_whfs", "查询名单" } });
            arrprojc.Add(new JObject() { { "@_kclx", dt["kclx"].ToString() } });
            arrprojc.Add(new JObject() { { "@_xq", dt["xq"].ToString() } });
            arrprojc.Add(new JObject() { { "@_bh", dt["bh"].ToString() } });
            arrprojc.Add(new JObject() { { "@_kcdm", dt["kcdm"].ToString() } });
            arrprojc.Add(new JObject() { { "@_kch", dt["kch"].ToString() } });
            arrprojc.Add(new JObject() { { "@_jsmc", dt["jsmc"].ToString() } });
            arrprojc.Add(new JObject() { { "@_ksxzm", dt["ksxzm"].ToString() } });
            DataTable dtjc = new QJ_JWB().GetDataTableByPR("pr_cjsr_xsmd_new", arrprojc);
            msg.Result = dtjc;
        }


        /// <summary>
        /// 接收成绩
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void CJJS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JObject dtkc = JsonConvert.DeserializeObject(P1) as JObject;
            JArray dt = JsonConvert.DeserializeObject(P2) as JArray;
            string _whfs = "接收学生成绩";
            int m_idn = 0;

            foreach (var item in dt)
            {
                m_idn = int.Parse(item["cjidn"].ToString());
                JArray arrprojc = new JArray();
                arrprojc.Add(new JObject() { { "@_whfs", _whfs } });
                arrprojc.Add(new JObject() { { "@_jsdm", UserInfo.User.UserName } });
                arrprojc.Add(new JObject() { { "@_xq", item["xq"].ToString() } });
                arrprojc.Add(new JObject() { { "@_idn", m_idn } });
                arrprojc.Add(new JObject() { { "@_jsdmlist", item["kclx"].ToString() } });
                DataTable dtjc = new QJ_JWB().GetDataTableByPR("pr_cjsr_jccjsr_new", arrprojc);
            }
            _whfs = "接收成绩状态";
            JArray arrpro = new JArray();
            arrpro.Add(new JObject() { { "@_whfs", _whfs } });
            arrpro.Add(new JObject() { { "@_kclx", dtkc["kclx"].ToString() } });
            arrpro.Add(new JObject() { { "@_xq", dtkc["xq"].ToString() } });
            arrpro.Add(new JObject() { { "@_bh", dtkc["bh"].ToString() } });
            arrpro.Add(new JObject() { { "@_kcdm", dtkc["kcdm"].ToString() } });
            arrpro.Add(new JObject() { { "@_kch", dtkc["kch"].ToString() } });
            arrpro.Add(new JObject() { { "@_jsmc", dtkc["jsmc"].ToString() } });
            arrpro.Add(new JObject() { { "@_ksxzm", 43 } });
            DataTable dtRetu = new QJ_JWB().GetDataTableByPR("pr_cjsr_xsmd_new", arrpro);


            msg.Result = "";
        }



        /// <summary>
        /// 恢复权限
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void CJHFQX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            JObject dt = JsonConvert.DeserializeObject(P1) as JObject;

            if (dt["cjsrqk"].ToString() == "已提交" || dt["cjsrqk"].ToString() == "成绩已接收")
            {
                JArray arrprojc = new JArray();
                arrprojc.Add(new JObject() { { "@_whfs", "恢复修改权限" } });
                arrprojc.Add(new JObject() { { "@_kclx", dt["kclx"].ToString() } });
                arrprojc.Add(new JObject() { { "@_xq", dt["xq"].ToString() } });
                arrprojc.Add(new JObject() { { "@_bh", dt["bh"].ToString() } });
                arrprojc.Add(new JObject() { { "@_kcdm", dt["kcdm"].ToString() } });
                arrprojc.Add(new JObject() { { "@_kch", dt["kch"].ToString() } });
                arrprojc.Add(new JObject() { { "@_jsmc", dt["jsmc"].ToString() } });
                arrprojc.Add(new JObject() { { "@_ksxzm", dt["ksxzm"].ToString() } });
                DataTable dtjc = new QJ_JWB().GetDataTableByPR("pr_cjsr_xsmd_new", arrprojc);

                string m_bh = dt["bh"].ToString() + "," + dt["kch"].ToString() + "," + dt["kclx"].ToString() + "," + UserInfo.User.UserName + ",";

                JArray arrpro = new JArray();
                arrpro.Add(new JObject() { { "@_whfs", "恢复教师输入成绩权限" } });
                arrpro.Add(new JObject() { { "@_jsdm", m_bh } });
                arrpro.Add(new JObject() { { "@_xq", dt["xq"].ToString() } });
                arrpro.Add(new JObject() { { "@_idn", 0 } });
                arrpro.Add(new JObject() { { "@_jsdmlist", "192.168.0.1" } });
                DataTable dtRetu = new QJ_JWB().GetDataTableByPR("pr_cjsr_jccjsr_new", arrpro);
                msg.Result = dtjc;
            }

           
        }



        /// <summary>
        /// 导入成绩
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DRCJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            DataTable dtkc = JsonConvert.DeserializeObject<DataTable>(P1);

            string strCon = new QJ_JWB().GetDBString();
            DBFactory db = new DBFactory(strCon);
            for (int i = 0; i < dtkc.Rows.Count; i++)
            {

                int kkxqs = 0;
                int.TryParse(dtkc.Rows[i]["开课学期数量"].ToString(), out kkxqs);
                string strCCXH = dtkc.Rows[i]["层次序号"].ToString();
                string strXBXH = dtkc.Rows[i]["系部序号"].ToString();
                string strZYXH = dtkc.Rows[i]["专业序号"].ToString();
                for (int m = 1; m < kkxqs + 1; m++)
                {
                    var dt = new Dictionary<string, object>();

                    string strKCBM = new JWHelp().GetXH(strXBXH, strZYXH, strCCXH);
                    string strKCName = dtkc.Rows[i]["课程名称"].ToString().Trim();
                    if (kkxqs > 1)
                    {
                        strKCName = strKCName + "0" + m.ToString();
                    }
                    dt.Add("kcdm", strKCBM);
                    dt.Add("kcmc", strKCName);
                    dt.Add("qjxbmc", dtkc.Rows[i]["系部"].ToString().Trim());
                    dt.Add("qjxbdm", dtkc.Rows[i]["系部序号"].ToString().Trim());
                    dt.Add("jysdm", dtkc.Rows[i]["教研室代码"].ToString().Trim());
                    dt.Add("kcmc_e", "");
                    dt.Add("TPKC", "0");
                    dt.Add("RXSYCS", "0");
                    dt.Add("kcxz", dtkc.Rows[i]["学制"].ToString().Trim());
                    dt.Add("kclb", dtkc.Rows[i]["课程类型"].ToString().Trim());
                    dt.Add("kcsx", dtkc.Rows[i]["课程属性"].ToString().Trim());


                    dt.Add("qjzydm", dtkc.Rows[i]["专业序号"].ToString().Trim());
                    dt.Add("qjzyname", dtkc.Rows[i]["专业名称"].ToString().Trim());
                    dt.Add("qjfx", dtkc.Rows[i]["方向"].ToString().Trim());
                    dt.Add("qjxqs", kkxqs);
                    dt.Add("CRUser", "000000061");
                    dt.Add("CRDate", DateTime.Now);
                    dt.Add("ComID", "0");
                    dt.Add("DCode", UserInfo.User.BranchCode);
                    dt.Add("DName", UserInfo.BranchInfo.DeptName);

                    dt.Add("intProcessStanceid", "0");
                    //新增
                    db.InserData(dt, "kcdm");
                }
            }
            string strSQL = "Insert into cj(bh, xh, bj, xm, xn, kcdm, kcmc, xs, xf, jsmc, bklb, kclb, kscj, qzcj, qmcj, pscj, ksny, srxq, scbj, ksfsm, ksxzm) select bh, xh, bj, xm,'20-21-2',kcdm,kcmc,'32','2','超星平台',bklb,'H',zhcj,'0','0','0','2021-07-19 11:13:14','20-21-2','0','4','1' from cjdr20210725 where bj is not null";
            new QJ_JWB().ExsSclarSql(strSQL);

        }
        #endregion





    }
}