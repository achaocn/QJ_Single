﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using System;
using System.ComponentModel;
using System.Data;

namespace QJY.API
{
    //成绩管理
    public class ZLPJManage
    {



        #region PC端


        /// <summary>
        ///  是否评教
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETISPJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strISPJ = P1;
            string strIDS = P2;
            DataTable dtPJKC = new QJ_JWB().GetDTByCommand("select KBK_JSMC.IDN,KBK.XQ,KBK.BH,KBK.BJ,KBK.KCDM,KBK_JSMC.JSMC,KBK_JSMC.JSDM,KBK.KCMC,KBK.LBDH,BJ.XBDM,dbo.fn_xbdm_xbmc(BJ.XBDM) XBMC,BJ.NJ,(case KBK_JSMC.SFPJ  when '2' then '参加评教'  else '不参加评教'  end ) ISPJ FROM KBK_JSMC INNER JOIN KBK ON KBK_JSMC.XQ = KBK.xq and  KBK_JSMC.bh = KBK.bh and  KBK_JSMC.kcdm = KBK.kcdm INNER JOIN BJ ON KBK_JSMC.BH = BJ.BH WHERE KBK_JSMC.IDN IN (@ids)", new { ids = strIDS.SplitTOInt(',') });
            new QJ_JWB().ExsSql("UPDATE KBK_JSMC SET SFPJ=@sfpj WHERE IDN IN (@ids)", new { sfpj = P1, ids = strIDS.SplitTOInt(',') });
        }



        /// <summary>
        /// 湖南益阳-计算学生评教结果
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void JSXSPJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            // string strxbdm =P1;
            //DataTable dtPJKC = new QJ_JWB().GetDTByCommand("select * from IN (@ids)", new { ids = strIDS.SplitTOInt(',') });
            //new QJ_JWB().ExsSql("UPDATE KBK_JSMC SET SFPJ=@sfpj WHERE IDN IN (@ids)", new { sfpj = P1, ids = strIDS.SplitTOInt(',') });
        }





        /// <summary>
        /// 获取学生评教明细
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETXSPJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strxq = P1;
            string strxbdm =P2;
            DataTable dtPJKC = new QJ_JWB().GetDTByCommand("SELECT qj_xspjdata.*,bj.xbdm,dbo.fn_kcdm_kcmc(qj_xspjdata.kcdm) AS pjkcmc,dbo.fn_xbdm_xbmc(bj.xbdm) AS XBMC,bj.bj  FROM qj_xspjdata INNER JOIN BJ ON qj_xspjdata.bh=BJ.bh where qj_xspjdata.xq = @xq AND bj.xbdm  in (@xbdm)", new { xq= strxq, xbdm = strxbdm.Split(',') });
            msg.Result = dtPJKC;


            DataTable dtPJJG = new QJ_JWB().GetDTByCommand("SELECT pj_xspjsj_jsbjdf.*,bj.xbdm,dbo.fn_kcdm_kcmc(pj_xspjsj_jsbjdf.kcdm) AS pjkcmc,dbo.fn_xbdm_xbmc(bj.xbdm) AS XBMC,bj.bj  FROM pj_xspjsj_jsbjdf INNER JOIN BJ ON pj_xspjsj_jsbjdf.bh=BJ.bh where pj_xspjsj_jsbjdf.xq = @xq AND bj.xbdm  in (@xbdm)", new { xq = strxq, xbdm = strxbdm.Split(',') });
            msg.Result1 = dtPJJG;
            
        }

        #endregion





    }
}