﻿using Aspose.Words;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using System;
using System.Data;
using System.Linq;
using System.Web;

namespace QJY.API
{
    public class KSGLManage
    {


        #region 内部方法
        public string getbjs(DataTable dt, string bksj, string bkjse, DataTable dtbjkcxx)
        {
            string strBJ = "";
            string strKC = "";

            DataTable temp = dt.Where(" sj='" + bksj + "' and bkjse='" + bkjse + "'");
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                string tempbj = temp.Rows[i]["bj"].ToString();
                DataTable dtTemp = dtbjkcxx.Where(" bj='"+ tempbj + "' and sj='" + bksj + "' and bkjse='" + bkjse + "'");
                if (!strBJ.Contains(temp.Rows[i]["bj"].ToString()))
                {
                    strBJ = strBJ + temp.Rows[i]["bj"].ToString() + "(" + dtTemp.Rows.Count+ "人),";
                }
                if (!strKC.Contains(temp.Rows[i]["kcmc"].ToString()))
                {
                    strKC = strKC + temp.Rows[i]["kcmc"].ToString() + ",";
                }
            }
            return strBJ.TrimEnd(',') + "$" + strKC.TrimEnd(',');
        }


        /// <summary>
        /// 判断考试时间是否冲突,同一课程只能在同一时间考试
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="bksj"></param>
        /// <param name="bkjse"></param>
        /// <returns></returns>
        public string jcks(string kssj, string kskcdm, string kslb, string ksxq, DataTable dtids)
        {

            string strReturn = "";
            if (kslb == "补考" || kslb == "毕业补考")
            {
                //抓出所有得数据,将要修改得数据加上历史数据判断是不是超过一条
                DataTable dtReturnKS = new QJ_JWB().GetDTByCommand("SELECT * FROM bkbm WHERE  sfbk = 1 and sfjk = 1 and tjxq='" + ksxq + "' AND lb='" + kslb + "' AND kcdm='" + kskcdm + "' ");
                for (int i = 0; i < dtReturnKS.Rows.Count; i++)
                {
                    for (int m = 0; m < dtids.Rows.Count; m++)
                    {
                        if (dtids.Rows[m]["idn"].ToString() == dtReturnKS.Rows[i]["idn"].ToString())
                        {
                            dtReturnKS.Rows[i]["bkz"] = kssj.Split(',')[0];
                            dtReturnKS.Rows[i]["bkzc"] = kssj.Split(',')[1];
                            dtReturnKS.Rows[i]["bkjc1"] = kssj.Split(',')[2];
                            dtReturnKS.Rows[i]["bkjc2"] = kssj.Split(',')[3];

                        }
                    }
                }
                DataTable datemp = dtReturnKS.DefaultView.ToTable(true, new string[] { "tjxq", "lb", "kcdm", "bkz", "bkzc", "bkjc1", "bkjc2" }).Where("bkz<>'0'");//课程
                if (datemp.Rows.Count > 1)
                {
                    strReturn = "同一课程只能安排在一个时间内考试";
                }
            }
            return strReturn;
        }



        /// <summary>
        /// 判断人员时间是否冲突,同一人员只能在同一时间考一门课程
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="bksj"></param>
        /// <param name="bkjse"></param>
        /// <returns></returns>
        public string jcksry(string kssj, string xh, string kslb, string ksxq, DataTable dtids)
        {
            string strReturn = "";
            if (kslb == "补考" || kslb == "毕业补考")
            {
                //抓出所有得数据,将要修改得数据加上历史数据判断是不是超过一条
                DataTable dtReturnRY = new QJ_JWB().GetDTByCommand("SELECT * FROM bkbm WHERE  tjxq='" + ksxq + "' AND lb='" + kslb + "' AND xh='" + xh + "' ");
                for (int i = 0; i < dtReturnRY.Rows.Count; i++)
                {
                    for (int m = 0; m < dtids.Rows.Count; m++)
                    {
                        if (dtids.Rows[m]["idn"].ToString() == dtReturnRY.Rows[i]["idn"].ToString())
                        {
                            dtReturnRY.Rows[i]["bkz"] = kssj.Split(',')[0];
                            dtReturnRY.Rows[i]["bkzc"] = kssj.Split(',')[1];
                            dtReturnRY.Rows[i]["bkjc1"] = kssj.Split(',')[2];
                            dtReturnRY.Rows[i]["bkjc2"] = kssj.Split(',')[3];

                        }
                    }
                }
                DataTable datemp = dtReturnRY.DefaultView.ToTable(true, new string[] { "tjxq", "lb", "kcdm", "xh", "xm", "bkz", "bkzc", "bkjc1", "bkjc2" }).Where("bkz<>'0'");//课程

                DataTable temp = datemp.Where(" bkz='" + kssj.Split(',')[0] + "' and bkzc='" + kssj.Split(',')[1] + "' and bkjc1='" + kssj.Split(',')[2] + "' and bkjc2='" + kssj.Split(',')[3] + "'");
                if (temp.Rows.Count > 1)
                {
                    strReturn = temp.Rows[0]["xm"].ToString()+ "在一个时间内只能参加一门课程的考试";
                }
            }
            return strReturn;
        }


        /// <summary>
        /// 是否锁定
        /// </summary>
        /// <param name="kssj"></param>
        /// <param name="kskcdm"></param>
        /// <param name="kslb"></param>
        /// <param name="ksxq"></param>
        /// <returns></returns>
        public string issd(string bh, string kskcdm, DataTable dtSDData)
        {
            string strReturn = "0";
            DataTable dtReturnSD = dtSDData.Where(" bh='" + bh + "' and kcdm='" + kskcdm + "'");

            if (dtReturnSD.Rows.Count > 0)
            {
                strReturn = "1";//锁定
            }
            return strReturn;
        }
        #endregion

        #region PC端
        /// <summary>
        /// 生成补考名单
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SCBKMD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = context.Request("xq").ToString();
            string cjxq = context.Request("cjxq").ToString();

            string striscx = context.Request("iscx", "N").ToString();
            string strbjs = context.Request("bj").ToString();
            string strkclbs = context.Request("kclb").ToString();
            string kslb = context.Request("kslb").ToString();

            string strWhere = " AND cj.ksxzm=1 ";
            if (striscx == "Y")
            {
                //重修抓取补考成绩
                strWhere = " AND (cj.ksxzm=1 or cj.ksxzm=2 )";
            }
            new QJ_JWB().ExsSclarSql(" DELETE bkbm WHERE lb='" + kslb + "' and tjxq='" + strXQ + "' and kclb in ('" + strkclbs.ToFormatLike() + "') and  bh in ('" + strbjs.ToFormatLike() + "')");
            new QJ_JWB().ExsSclarSql(" insert into bkbm(tjxq,lb,bh,bj,xh,xm,kcdm,kcmc,kclb,xs,xf,jsmc,kscj,ksfsm,ksxzm,xn,sfbk,qmcj,kslb) SELECT '" + strXQ + "', '补考', bh, bj, xh, xm, kcdm, kcmc, kclb, xs, xf, jsmc, kscj, ksfsm, ksxzm, xn, '1', qmcj, bklb FROM( SELECT * from cj where  ISNUMERIC(kscj)=1   and   cast(kscj as DECIMAL(10,2)) < 60   AND XN = '" + cjxq + "'" + strWhere + "  UNION ALL SELECT * from cj where kscj  in ('','缓考','零分卷','缺考','无资格','作弊')  AND XN = '" + cjxq + "' " + strWhere + ")  A  where kclb in ('" + strkclbs.ToFormatLike() + "') and  bh in ('" + strbjs.ToFormatLike() + "')");
            new QJ_JWB().ExsSclarSql(" UPDATE bkbm SET bkbm.KCXBDM = dbo.fn_kcdm_xbdm(bkbm.kcdm) WHERE lb='" + kslb + "' and tjxq='" + strXQ + "' and kclb in ('" + strkclbs.ToFormatLike() + "') and  bh in ('" + strbjs.ToFormatLike() + "')");





        }


        /// <summary>
        /// 删除补考名单
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELBKMD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string ids = context.Request("ids").ToString();
            new QJ_JWB().ExsSclarSql(" delete bkbm  where idn in ('" + ids.ToFormatLike() + "')");

        }


        /// <summary>
        /// 设置补考资格
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETBKZG(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string ids = context.Request("ids").ToString();
            string bkzg = context.Request("bkzg").ToString();
            new QJ_JWB().ExsSclarSql(" update bkbm set sfjk='" + bkzg + "'  where idn in ('" + ids.ToFormatLike() + "')");
        }





        /// <summary>
        /// 获取待补考数据（学生数据）
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">为0时获取全部数据,为1时获取已审核数据</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETDBKSJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strKSLB = context.Request("kslb", "补考").ToString();
            string strXQ = context.Request("xq").ToString();
            string strWhere = " AND bkbm.lb = '" + strKSLB + "' and bkbm.tjxq = '" + strXQ + "' ";
            string strXB = context.Request("xb").ToString();
            string strXiaoQ = context.Request("xiaoq")??"";

            if (P1 == "1")
            {
                strWhere = strWhere + " and bkbm.sfbk = 1 and bkbm.sfjk = 1 ";
            }
            strWhere = strWhere + " and bj.bmmc='" + strXiaoQ + "' and bj.xbdm in ('" + strXB.ToFormatLike() + "') ";

            string strSQL = "select bkbm.kcxbdm,bkbm.lb,bkbm.tjxq,bkbm.bh,bkbm.bj,bkbm.xh,bkbm.xm,bkbm.kcdm,bkbm.kcmc,bkbm.xn,bkbm.kclb,bkbm.xs,bkbm.xf,bkbm.kscj,bkbm.jsmc,bkbm.fzjs,bkbm.scbj,bkbm.sfbk,bkbm.sfjk," +
                   "bkbm.idn,bkbm.kclb lbdh, dbo.fn_xbdm_xbmc(bkbm.kcxbdm) kcxbmc, dbo.fn_cj_xh_xq_kcdm_maxcj(bkbm.xh, bkbm.ksfsm, bkbm.kcdm) maxcj,bkbm.pscj,bkbm.qzcj ,bkbm.qmcj, dbo.fn_xbdm_xbmc(bj.xbdm) zbmc,ISNULL(sfbk, 0) bk,ISNULL(sfjk, 0) jk,bmqk =case when ISNULL(sfbk,0)= 1 then '报名' when ISNULL(sfbk,0)= 0 then '' end,bkbm.bkjse,bkbm.kslb as bklb,bkbm.ksfs,bksj.sj " +
                  "from dbo.bkbm bkbm  LEFT JOIN  dbo.bj bj  ON bkbm.bh = bj.BH LEFT JOIN bksj ON bkbm.bkz=bksj.bkz and bkbm.bkzc=bksj.bkzc and bkbm.bkjc1=bksj.bkjc1 and bkbm.bkjc2=bksj.bkjc2	where 1=1 " + strWhere + " order by bkbm.kcdm,bh,xh";
            DataTable dtReturn = new QJ_JWB().GetDTByCommand(strSQL);


            string strSQLBJ = "select DISTINCT  tjxq,kcmc,jsmc,bkbm.bh, bkbm.bj, kcdm, dbo.fn_xbdm_xbmc(bkbm.kcxbdm) kcxbmc, dbo.fn_xbdm_xbmc(bj.xbdm) zbmc, bkjse, sj " +
                  " from dbo.bkbm bkbm  LEFT JOIN  dbo.bj bj  ON bkbm.bh = bj.BH LEFT JOIN bksj ON bkbm.bkz=bksj.bkz " +
                 " and bkbm.bkzc=bksj.bkzc and bkbm.bkjc1=bksj.bkjc1 and bkbm.bkjc2=bksj.bkjc2	where 1=1 " + strWhere;
            //DataTable dabj = dtReturn.DefaultView.ToTable(true, new string[] { "tjxq", "kcmc", "jsmc", "bh", "bj", "kcdm", "kcxbmc", "zbmc", "bkjse", "sj" });//班级
            DataTable dabj = new QJ_JWB().GetDTByCommand(strSQLBJ);

            dabj.Columns.Add("bkry");
            dabj.Columns.Add("bkrs");
            dabj.Columns.Add("issd");//锁定字段
            dabj.Columns.Add("xsxx", Type.GetType("System.Object"));




            DataTable dtReturnSD = new QJ_JWB().GetDTByCommand("SELECT * FROM kskc WHERE   LB='" + strKSLB + "' AND  xq='" + strXQ + "'");

            for (int i = 0; i < dabj.Rows.Count; i++)
            {
                string strxm = "";

                string strtbh = dabj.Rows[i]["bh"].ToString();
                string strtkcdm = dabj.Rows[i]["kcdm"].ToString();
                string strtjsmc = dabj.Rows[i]["jsmc"].ToString();
                DataTable dtT = dtReturn.Where(" bh='" + strtbh + "' and  kcdm='" + strtkcdm + "' and  jsmc='" + strtjsmc + "'");
                dabj.Rows[i]["bkrs"] = dtT.Rows.Count;

                for (int m = 0; m < dtT.Rows.Count; m++)
                {
                    strxm = strxm + dtT.Rows[m]["xm"].ToString() + ",";
                }
                dabj.Rows[i]["bkry"] = strxm.TrimEnd(',');
                dabj.Rows[i]["xsxx"] = dtT;

                dabj.Rows[i]["issd"] = issd(strtbh, strtkcdm, dtReturnSD);//默认为未锁定

            }


            DataTable dakc = dtReturn.DefaultView.ToTable(true, new string[] { "tjxq", "kcxbmc", "kcmc", "kcdm" });//课程
            dakc.Columns.Add("score");
            dakc.Columns.Add("yps");
            dakc.Columns.Add("zt");

            var query = from t in dtReturn.AsEnumerable()
                        group t by new { t1 = t.Field<string>("tjxq"), t2 = t.Field<string>("kcdm") } into m
                        select new
                        {
                            tjxq = m.Key.t1,
                            kcdm = m.Key.t2,
                            score = m.Count()
                        };
            DataTable dtTemp = dtReturn.Where(" bkjse<>''");
            var query1 = from t in dtTemp.AsEnumerable()
                         group t by new { t1 = t.Field<string>("tjxq"), t2 = t.Field<string>("kcdm") } into m
                         select new
                         {

                             kcdm = m.Key.t2,
                             score = m.Count()
                         };

            for (int i = 0; i < dakc.Rows.Count; i++)
            {
                dakc.Rows[i]["score"] = "0";
                dakc.Rows[i]["yps"] = "0";
                dakc.Rows[i]["zt"] = "0";

                foreach (var item in query)
                {
                    if (item.kcdm == dakc.Rows[i]["kcdm"].ToString())
                    {
                        dakc.Rows[i]["score"] = item.score;
                    }
                }
                foreach (var item in query1)
                {
                    if (item.kcdm == dakc.Rows[i]["kcdm"].ToString())
                    {
                        dakc.Rows[i]["yps"] = item.score;
                    }
                }
                if (dakc.Rows[i]["score"].ToString() == dakc.Rows[i]["yps"].ToString())
                {
                    dakc.Rows[i]["zt"] = "1";

                }
            }

            msg.Result = dtReturn;
            msg.Result1 = dakc;
            msg.Result3 = dabj;

            JArray arrprobczt = new JArray();
            arrprobczt.Add(new JObject() { { "@_whfs", "查询时间" } });
            arrprobczt.Add(new JObject() { { "@_lb", strKSLB } });
            arrprobczt.Add(new JObject() { { "@_xq", strXQ } });
            DataTable dtReturnSJ = new QJ_JWB().GetDataTableByPR("pr_ksgl_kssj", arrprobczt);
            msg.Result2 = dtReturnSJ;




        }



        public void GETDBKSJBACKUP(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strKSLB = context.Request("kslb", "补考").ToString();
            string strXQ = context.Request("xq").ToString();
            string strWhere = " AND bkbm.lb = '" + strKSLB + "' and bkbm.tjxq = '" + strXQ + "' ";
            string strXB = context.Request("xb").ToString();
            if (P1 == "1")
            {
                strWhere = strWhere + " and bkbm.sfbk = 1 and bkbm.sfjk = 1 ";
            }
            strWhere = strWhere + " and bj.xbdm in ('" + strXB.ToFormatLike() + "') ";

            string strSQL = "select bkbm.kcxbdm,bkbm.lb,bkbm.tjxq,bkbm.bh,bkbm.bj,bkbm.xh,bkbm.xm,bkbm.kcdm,bkbm.kcmc,bkbm.xn,bkbm.kclb,bkbm.xs,bkbm.xf,bkbm.kscj,bkbm.jsmc,bkbm.fzjs,bkbm.scbj,bkbm.sfbk,bkbm.sfjk," +
                   "bkbm.idn,bkbm.kclb lbdh, dbo.fn_xbdm_xbmc(bkbm.kcxbdm) kcxbmc, dbo.fn_cj_xh_xq_kcdm_maxcj(bkbm.xh, bkbm.ksfsm, bkbm.kcdm) maxcj,bkbm.pscj,bkbm.qzcj ,bkbm.qmcj, dbo.fn_xbdm_xbmc(bj.xbdm) zbmc,ISNULL(sfbk, 0) bk,ISNULL(sfjk, 0) jk,bmqk =case when ISNULL(sfbk,0)= 1 then '报名' when ISNULL(sfbk,0)= 0 then '' end,bkbm.bkjse,bkbm.kslb as bklb,bkbm.ksfs,bksj.sj " +
                  "from dbo.bkbm bkbm  LEFT JOIN  dbo.bj bj  ON bkbm.bh = bj.BH LEFT JOIN bksj ON bkbm.bkz=bksj.bkz and bkbm.bkzc=bksj.bkzc and bkbm.bkjc1=bksj.bkjc1 and bkbm.bkjc2=bksj.bkjc2	where 1=1 " + strWhere + " order by bkbm.kcdm,bh,xh";
            DataTable dtReturn = new QJ_JWB().GetDTByCommand(strSQL);
            DataTable dabj = dtReturn.DefaultView.ToTable(true, new string[] { "tjxq", "kcmc", "jsmc", "bh", "bj", "kcdm", "kcxbmc", "zbmc", "bkjse", "sj" });//班级
            dabj.Columns.Add("bkry");
            dabj.Columns.Add("bkrs");
            dabj.Columns.Add("issd");//锁定字段
            dabj.Columns.Add("xsxx", Type.GetType("System.Object"));




            DataTable dtReturnSD = new QJ_JWB().GetDTByCommand("SELECT * FROM kskc WHERE   LB='" + strKSLB + "' AND  xq='" + strXQ + "'");

            for (int i = 0; i < dabj.Rows.Count; i++)
            {
                string strxm = "";

                string strtbh = dabj.Rows[i]["bh"].ToString();
                string strtkcdm = dabj.Rows[i]["kcdm"].ToString();
                string strtjsmc = dabj.Rows[i]["jsmc"].ToString();
                DataTable dtT = dtReturn.Where(" bh='" + strtbh + "' and  kcdm='" + strtkcdm + "' and  jsmc='" + strtjsmc + "'");
                dabj.Rows[i]["bkrs"] = dtT.Rows.Count;

                for (int m = 0; m < dtT.Rows.Count; m++)
                {
                    strxm = strxm + dtT.Rows[m]["xm"].ToString() + ",";
                }
                dabj.Rows[i]["bkry"] = strxm.TrimEnd(',');
                dabj.Rows[i]["xsxx"] = dtT;

                dabj.Rows[i]["issd"] = issd(strtbh, strtkcdm, dtReturnSD);//默认为未锁定

            }


            DataTable dakc = dtReturn.DefaultView.ToTable(true, new string[] { "tjxq", "kcxbmc", "kcmc", "kcdm", "jsmc" });//课程
            dakc.Columns.Add("score");
            dakc.Columns.Add("yps");
            dakc.Columns.Add("zt");

            var query = from t in dtReturn.AsEnumerable()
                        group t by new { t1 = t.Field<string>("tjxq"), t2 = t.Field<string>("kcmc"), t3 = t.Field<string>("jsmc") } into m
                        select new
                        {
                            tjxq = m.Key.t1,
                            kcmc = m.Key.t2,
                            jsmc = m.Key.t3,
                            score = m.Count()
                        };
            DataTable dtTemp = dtReturn.Where(" bkjse<>''");
            var query1 = from t in dtTemp.AsEnumerable()
                         group t by new { t1 = t.Field<string>("tjxq"), t2 = t.Field<string>("kcmc"), t3 = t.Field<string>("jsmc") } into m
                         select new
                         {

                             kcmc = m.Key.t2,
                             jsmc = m.Key.t3,
                             score = m.Count()
                         };

            for (int i = 0; i < dakc.Rows.Count; i++)
            {
                dakc.Rows[i]["score"] = "0";
                dakc.Rows[i]["yps"] = "0";
                dakc.Rows[i]["zt"] = "0";

                foreach (var item in query)
                {
                    if (item.kcmc == dakc.Rows[i]["kcmc"].ToString() && item.jsmc == dakc.Rows[i]["jsmc"].ToString())
                    {
                        dakc.Rows[i]["score"] = item.score;
                    }
                }
                foreach (var item in query1)
                {
                    if (item.kcmc == dakc.Rows[i]["kcmc"].ToString() && item.jsmc == dakc.Rows[i]["jsmc"].ToString())
                    {
                        dakc.Rows[i]["yps"] = item.score;
                    }
                }
                if (dakc.Rows[i]["score"].ToString() == dakc.Rows[i]["yps"].ToString())
                {
                    dakc.Rows[i]["zt"] = "1";

                }
            }

            msg.Result = dtReturn;
            msg.Result1 = dakc;
            msg.Result3 = dabj;

            JArray arrprobczt = new JArray();
            arrprobczt.Add(new JObject() { { "@_whfs", "查询时间" } });
            arrprobczt.Add(new JObject() { { "@_lb", strKSLB } });
            arrprobczt.Add(new JObject() { { "@_xq", strXQ } });
            DataTable dtReturnSJ = new QJ_JWB().GetDataTableByPR("pr_ksgl_kssj", arrprobczt);
            msg.Result2 = dtReturnSJ;




        }

        /// <summary>
        /// 获取待补考数据(班级)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">为0时获取全部数据,为1时获取已审核数据</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETDBKSJBJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strKSLB = context.Request("kslb", "补考").ToString();
            string strXQ = context.Request("xq").ToString();
            string strXiaoQ = context.Request("xiaoq").ToString();

            
            string strWhere = " AND bkbm.lb = '" + strKSLB + "' and bkbm.tjxq = '" + strXQ + "' ";
            string strXB = context.Request("xb").ToString();
            if (P1 == "1")
            {
                strWhere = strWhere + " and bkbm.sfbk = 1 and bkbm.sfjk = 1 ";
            }
            strWhere = strWhere + " and bj.xbdm in ('" + strXB.ToFormatLike() + "') ";
            string strSQL = "select bkbm.kcxbdm,bkbm.lb,bkbm.tjxq,bkbm.bh,bkbm.bj,bkbm.xh,bkbm.xm,bkbm.kcdm,bkbm.kcmc,bkbm.xn,bkbm.kclb,bkbm.xs,bkbm.xf,bkbm.kscj,bkbm.jsmc,bkbm.fzjs,bkbm.scbj,bkbm.sfbk,bkbm.sfjk," +
                   "bkbm.idn,bkbm.kclb lbdh, dbo.fn_xbdm_xbmc(bkbm.kcxbdm) kcxbmc, dbo.fn_cj_xh_xq_kcdm_maxcj(bkbm.xh, bkbm.ksfsm, bkbm.kcdm) maxcj,bkbm.pscj,bkbm.qzcj ,bkbm.qmcj, dbo.fn_xbdm_xbmc(bj.xbdm) zbmc,ISNULL(sfbk, 0) bk,ISNULL(sfjk, 0) jk,bmqk =case when ISNULL(sfbk,0)= 1 then '报名' when ISNULL(sfbk,0)= 0 then '' end,bkbm.bkjse,bkbm.kslb as bklb,bkbm.ksfs,bksj.sj " +
                  "from dbo.bkbm bkbm  LEFT JOIN  dbo.bj bj  ON bkbm.bh = bj.BH LEFT JOIN bksj ON bkbm.bkz=bksj.bkz and bkbm.bkzc=bksj.bkzc and bkbm.bkjc1=bksj.bkjc1 and bkbm.bkjc2=bksj.bkjc2	where 1=1 " + strWhere + " order by bkbm.kcdm,bh,xh";
            DataTable dtReturn = new QJ_JWB().GetDTByCommand(strSQL);
            DataTable dakc = dtReturn.DefaultView.ToTable(true, new string[] { "tjxq", "kcmc", "jsmc" });//课程

            DataTable dabj = dtReturn.DefaultView.ToTable(true, new string[] { "tjxq", "kcmc", "jsmc", "bh", "bj", "kcdm", "bh", "kcxbmc", "zbmc", "bkjse", "sj" });//班级


            msg.Result = dtReturn;
            msg.Result1 = dakc;

            JArray arrprobczt = new JArray();
            arrprobczt.Add(new JObject() { { "@_whfs", "查询时间" } });
            arrprobczt.Add(new JObject() { { "@_lb", strKSLB } });
            arrprobczt.Add(new JObject() { { "@_xq", strXQ } });
            DataTable dtReturnSJ = new QJ_JWB().GetDataTableByPR("pr_ksgl_kssj", arrprobczt);
            msg.Result2 = dtReturnSJ;
            msg.Result3 = dabj;

        }



        /// <summary>
        /// 锁定排考
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETSDPK(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string ids = context.Request("ids").ToString();
            string issd = context.Request("issd").ToString();

            DataTable dtReturnKS = new QJ_JWB().GetDTByCommand("SELECT * from  bkbm  where idn in ('" + ids.ToFormatLike() + "')");
            DataTable dakc = dtReturnKS.DefaultView.ToTable(true, new string[] { "tjxq", "lb", "kcdm", "bh", "bkz" });//课程
            for (int i = 0; i < dakc.Rows.Count; i++)
            {
                string lb = dakc.Rows[i]["lb"].ToString();
                string tjxq = dakc.Rows[i]["tjxq"].ToString();
                string kcdm = dakc.Rows[i]["kcdm"].ToString();
                string bh = dakc.Rows[i]["bh"].ToString();

                new QJ_JWB().ExsSclarSql("DELETE kskc WHERE LB='" + lb + "' AND  xq='" + tjxq + "' AND bh='" + bh + "' and kcdm='" + kcdm + "'");
                if (issd == "1")
                {
                    new QJ_JWB().ExsSclarSql("INSERT INTO [dbo].[kskc] ([lb], [xq], [bh], [kcdm], [scbj], [KSZ], [KSFS], [bz], [kch], [jsdm], [rs]) VALUES ('" + dakc.Rows[i]["lb"].ToString() + "', '" + dakc.Rows[i]["tjxq"].ToString() + "', '" + dakc.Rows[i]["bh"].ToString() + "', '" + dakc.Rows[i]["kcdm"].ToString() + "', '0', '" + dakc.Rows[i]["bkz"].ToString() + "', '统排', NULL, NULL, NULL, NULL);");
                }

            }
        }

        /// <summary>
        /// 获取空教室
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETKJSBYSJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = context.Request("xq").ToString();
            string strRS = context.Request("rs").ToString();
            string xiaoq = context.Request("xiaoq","本部").ToString().Trim();

            

            string SJ = P1;
            JArray arrprobczt = new JArray();
            arrprobczt.Add(new JObject() { { "@_whfs", "查询空教室" } });
            arrprobczt.Add(new JObject() { { "@_bmmc", xiaoq } });
            arrprobczt.Add(new JObject() { { "@_xq", strXQ } });
            arrprobczt.Add(new JObject() { { "@_cxz", P1.Split(',')[0] } });
            arrprobczt.Add(new JObject() { { "@_zc", P1.Split(',')[1] } });
            arrprobczt.Add(new JObject() { { "@_jc1", P1.Split(',')[2] } });
            arrprobczt.Add(new JObject() { { "@_jc2", P1.Split(',')[3] } });
            arrprobczt.Add(new JObject() { { "@_zws1", strRS } });
            arrprobczt.Add(new JObject() { { "@_zws2", 500 } });
            DataTable dtReturnJS = new QJ_JWB().GetDataTableByPR("pr_kbgl_cxkjse", arrprobczt);
            msg.Result = dtReturnJS;

        }


        /// <summary>
        /// 安排补考
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETBK(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string ids = context.Request("ids").ToString();
            string jse = context.Request("jse") ?? "";
            DataTable dtData = new QJ_JWB().GetDTByCommand("SELECT * from  bkbm  where idn in ('" + ids.ToFormatLike() + "')");
            DataTable dtKC = dtData.DefaultView.ToTable(true, new string[] { "tjxq", "lb", "kcdm" });//课程

            for (int i = 0; i < dtKC.Rows.Count; i++)
            {
                string strErr = "";
                string strkcdm = dtKC.Rows[i]["kcdm"].ToString();
                string strkslb = dtKC.Rows[i]["lb"].ToString();
                string strxq = dtKC.Rows[i]["tjxq"].ToString();
                strErr = jcks(P1, strkcdm, strkslb, strxq, dtData);
                if (strErr != "")
                {
                    msg.ErrorMsg = strErr;
                    return;
                }
            }
            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                string strErr = "";
                string strxh = dtData.Rows[i]["xh"].ToString();
                string strkslb = dtData.Rows[i]["lb"].ToString();
                string strxq = dtData.Rows[i]["tjxq"].ToString();
                strErr = jcksry(P1, strxh, strkslb, strxq, dtData);
                if (strErr != "")
                {
                    msg.ErrorMsg = strErr;
                    return;
                }
            }
            new QJ_JWB().ExsSclarSql(" update bkbm set bkz = '" + P1.Split(',')[0] + "', bkzc = '" + P1.Split(',')[1] + "', bkjc1 = '" + P1.Split(',')[2] + "', bkjc2 = '" + P1.Split(',')[3] + "', bkjse='" + jse + "' where idn in ('" + ids.ToFormatLike() + "')");



        }


        /// <summary>
        /// 撤销补考
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void CANCELBK(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string ids = context.Request("ids").ToString();
            string idssd = context.Request("idssd").ToString();
            new QJ_JWB().ExsSclarSql(" update bkbm set bkz = '0', bkzc = '0', bkjc1 = '0', bkjc2 = '0', bkjse='' where idn in ('" + ids.ToFormatLike() + "')");
            new QJ_JWB().ExsSclarSql(" update bkbm set  bkjse='' where idn in ('" + idssd.ToFormatLike() + "')"); //锁定得

        }


        /// <summary>
        /// 获取待安排监考得考场
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJKKCDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = context.Request("xq").ToString();
            string strKSLB = context.Request("kslb", "补考").ToString();
            string strXiaoQ = context.Request("xiaoq","").ToString();

            string strWhere = " AND bkbm.lb = '" + strKSLB + "'  and bkjse<>'' and bkbm.tjxq = '" + strXQ + "' ";
            string strXB = context.Request("xb").ToString();
            if (P2 == "1")
            {
                strWhere = strWhere + " and bkbm.sfbk = 1 and bkbm.sfjk = 1 ";
            }
            strWhere = strWhere + "and bj.bmmc='" + strXiaoQ + "' and bj.xbdm in ('" + strXB.ToFormatLike() + "') ";

            string strSQL = "select bkbm.kcxbdm,bkbm.lb,bkbm.tjxq,bkbm.bh,bkbm.bj,bkbm.xh,bkbm.xm,bkbm.kcdm,bkbm.kcmc,bkbm.xn,bkbm.kclb,bkbm.xs,bkbm.xf,bkbm.kscj,bkbm.jsmc,bkbm.fzjs,bkbm.scbj,bkbm.sfbk,bkbm.sfjk,bkbm.bkz,bkbm.bkzc,bkbm.bkjc1,bkbm.bkjc2," +
                   "bkbm.idn,bkbm.kclb lbdh, dbo.fn_xbdm_xbmc(bkbm.kcxbdm) kcxbmc, dbo.fn_cj_xh_xq_kcdm_maxcj(bkbm.xh, bkbm.ksfsm, bkbm.kcdm) maxcj,dbo.fn_xbdm_xbmc(bj.xbdm) zbmc,ISNULL(sfbk, 0) bk,ISNULL(sfjk, 0) jk,bmqk =case when ISNULL(sfbk,0)= 1 then '报名' when ISNULL(sfbk,0)= 0 then '' end,bkbm.bkjse,bkbm.bklb,bkbm.ksfs,bksj.sj " +
                  "from dbo.bkbm bkbm  LEFT JOIN  dbo.bj bj  ON bkbm.bh = bj.BH LEFT JOIN bksj ON bkbm.bkz=bksj.bkz and bkbm.bkzc=bksj.bkzc and bkbm.bkjc1=bksj.bkjc1 and bkbm.bkjc2=bksj.bkjc2	 where 1=1 " + strWhere + " order by bkbm.kcdm,bh,xh";
            DataTable dtbjkcxx = new QJ_JWB().GetDTByCommand(strSQL);



            //监考老师信息
            DataTable dtjkjs = new QJ_JWB().GetDTByCommand("SELECT * FROM bkjkjs WHERE tjxq='" + strXQ + "' AND lb='" + strKSLB + "'");

            //考场信息
            DataTable dtjkkc = dtbjkcxx.DefaultView.ToTable(true, new string[] { "bkz", "bkzc", "bkjc1", "bkjc2", "sj", "bkjse" });//课程


            var querysj = from t in dtjkkc.AsEnumerable()
                          group t by new { t1 = t.Field<int>("bkz").ToString(), t2 = t.Field<int>("bkzc").ToString(), t3 = t.Field<int>("bkjc1").ToString(), t4 = t.Field<int>("bkjc2").ToString(), t5 = t.Field<string>("sj") } into m
                          select new
                          {
                              sj = m.Key.t1 + "," + m.Key.t2 + "," + m.Key.t3 + "," + m.Key.t4,
                              sjxx = m.Key.t5,
                              sjxx1 = m.Key.t1 + "周,星期" + m.Key.t2 + "第" + m.Key.t3 + "到" + m.Key.t4 + "节",
                              score = m.Count()
                          };

            dtjkkc.Columns.Add("bjs");
            dtjkkc.Columns.Add("kcs");
            dtjkkc.Columns.Add("kssj");
            dtjkkc.Columns.Add("jkjs");
            dtjkkc.Columns.Add("jkjsdm");
            dtjkkc.Columns.Add("tempjkjs");
            dtjkkc.Columns.Add("tempjkjsdm");//添加临时字段，用于预览生成得监考方案
            for (int i = 0; i < dtjkkc.Rows.Count; i++)
            {
                string bksj = dtjkkc.Rows[i]["sj"].ToString();
                string bkjse = dtjkkc.Rows[i]["bkjse"].ToString();

                string bjkcs = getbjs(dtbjkcxx, bksj, bkjse, dtbjkcxx);
                dtjkkc.Rows[i]["bjs"] = bjkcs.Split('$')[0];
                dtjkkc.Rows[i]["kcs"] = bjkcs.Split('$')[1];

                dtjkkc.Rows[i]["kssj"] = "第" + dtjkkc.Rows[i]["bkz"].ToString() + "周,星期" + dtjkkc.Rows[i]["bkzc"].ToString() + "第" + dtjkkc.Rows[i]["bkjc1"].ToString() + "到" + dtjkkc.Rows[i]["bkjc2"].ToString() + "节";

                DataTable tmls = dtjkjs.Where("bkz='" + dtjkkc.Rows[i]["bkz"].ToString() + "' and bkzc='" + dtjkkc.Rows[i]["bkzc"].ToString() + "' and bkjc1='" + dtjkkc.Rows[i]["bkjc1"].ToString() + "' and bkjc2='" + dtjkkc.Rows[i]["bkjc2"].ToString() + "' and bkjse='" + bkjse + "'");
                string jkjs = "";
                string strjsdm = "";
                for (int m = 0; m < tmls.Rows.Count; m++)
                {
                    jkjs = jkjs + tmls.Rows[m]["jkjs"].ToString() + ",";
                    strjsdm = strjsdm + tmls.Rows[m]["jsdm"].ToString() + ",";
                }
                dtjkkc.Rows[i]["jkjs"] = jkjs.TrimEnd(',');
                dtjkkc.Rows[i]["jkjsdm"] = strjsdm.TrimEnd(',');
                dtjkkc.Rows[i]["tempjkjs"] = jkjs.TrimEnd(',');
                dtjkkc.Rows[i]["tempjkjsdm"] = strjsdm.TrimEnd(',');
            }

            msg.Result = dtjkkc;
            msg.Result1 = querysj;

        }



        /// <summary>
        /// 安排监考老师
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void APJKLS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = context.Request("xq").ToString();
            string strKSLB = context.Request("kslb", "补考").ToString();
            JArray dt = JsonConvert.DeserializeObject(P1) as JArray;
            foreach (JObject item in dt)
            {
                string bkjse = (string)item["bkjse"];
                string bkz = (string)item["bkz"];
                string bkzc = (string)item["bkzc"];
                string bkjc1 = (string)item["bkjc1"];
                string bkjc2 = (string)item["bkjc2"];
                string jkjsdm = (string)item["tempjkjsdm"].ToString().TrimEnd(',');
                string jkjs = (string)item["tempjkjs"].ToString().TrimEnd(',');

                new QJ_JWB().ExsSclarSql("delete bkjkjs where  tjxq='" + strXQ + "' AND lb='" + strKSLB + "' and bkz='" + bkz + "' and bkzc='" + bkzc + "' and bkjc1='" + bkjc1 + "' and bkjc2='" + bkjc2 + "' and bkjse='" + bkjse + "'");
                for (int i = 0; i < jkjsdm.Split(',').Count(); i++)
                {
                    new QJ_JWB().ExsSclarSql(" insert into bkjkjs(tjxq,lb,bkz,bkzc,bkjc1,bkjc2,bkjse,jkjs,xbdm,xbmc,jsdm) values('" + strXQ + "', '" + strKSLB + "', '" + bkz + "', '" + bkzc + "', '" + bkjc1 + "', '" + bkjc2 + "', '" + bkjse + "', '" + jkjs.Split(',')[i].ToString() + "', '" + UserInfo.BranchInfo.RoomCode + "', dbo.fn_xbdm_xbmc('" + UserInfo.BranchInfo.RoomCode + "'), '" + jkjsdm.Split(',')[i].ToString() + "')   ");
                }

            }

        }

        /// <summary>
        /// 取消监考老师
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void CANCELJK(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = context.Request("xq").ToString();
            string strKSLB = context.Request("kslb", "补考").ToString();
            JArray dt = JsonConvert.DeserializeObject(P1) as JArray;
            foreach (JObject item in dt)
            {
                string bkjse = (string)item["bkjse"];
                string bkz = (string)item["bkz"];
                string bkzc = (string)item["bkzc"];
                string bkjc1 = (string)item["bkjc1"];
                string bkjc2 = (string)item["bkjc2"];
                string jkjsdm = (string)item["jkjsdm"].ToString().TrimEnd(',');
                string jkjs = (string)item["jkjs"].ToString().TrimEnd(',');
                for (int i = 0; i < jkjsdm.Split(',').Count(); i++)
                {
                    string strSQL = "delete bkjkjs where jsdm='" + jkjsdm.Split(',')[i].ToString() + "' and  tjxq='" + strXQ + "' AND lb='" + strKSLB + "' and bkz='" + bkz + "' and bkzc='" + bkzc + "' and bkjc1='" + bkjc1 + "' and bkjc2='" + bkjc2 + "' and bkjse='" + bkjse + "';";
                    new QJ_JWB().ExsSclarSql(strSQL);
                }

            }

        }

        /// <summary>
        /// 获取空闲老师（补考）
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETKXLS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = context.Request("xq").ToString();
            string strKSLB = context.Request("kslb", "补考").ToString();
            string jsdm = context.Request("jsdm").ToString().Trim(',');
            if (P1 == "Y")
            {
                JH_Auth_UserCustomData model = new JH_Auth_UserCustomDataB().GetEntities(D => D.UserName == UserInfo.User.UserName && D.DataType == "监考").FirstOrDefault();
                if (model != null)
                {
                    jsdm = model.DataContent;
                }
            }

            DataTable dtReturnJS = new QJ_JWB().GetDTByCommand("SELECT jsdm.jsmc,jsdm.jsdm,COUNT(bkjkjs.idn) as jkcs from  jsdm LEFT  JOIN bkjkjs ON jsdm.JSDM=bkjkjs.jsdm and  lb='" + strKSLB + "' and tjxq='" + strXQ + "'  where  jsdm.jsdm in ('" + jsdm.ToFormatLike() + "') GROUP BY jsdm.jsmc,jsdm.jsdm");
            msg.Result = dtReturnJS;


            new JH_Auth_UserCustomDataB().Delete(D => D.UserName == UserInfo.User.UserName && D.DataType == "监考");
            JH_Auth_UserCustomData customData = new JH_Auth_UserCustomData();
            customData.ComId = UserInfo.User.ComId;
            customData.CRDate = DateTime.Now;
            customData.CRUser = UserInfo.User.UserName;
            customData.DataContent = jsdm;
            customData.DataType = "监考";
            customData.UserName = UserInfo.User.UserName;
            new JH_Auth_UserCustomDataB().Insert(customData);

        }

        /// <summary>
        /// 生成打印名单
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETKCMDPDF(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strKSLB = context.Request("kslb", "补考").ToString();
            string strXQ = context.Request("xq").ToString();
            string strjse = P1;
            string strSJ = P2;

            string strjtSJ = context.Request("sj").ToString(); ;


            var basePath = HttpContext.Current.Request.MapPath("/");
            string filePath = basePath + "/upload/dcmb/dymd.docx";
            Aspose.Words.Document doc = new Aspose.Words.Document(filePath);



            DocumentBuilder builder = new DocumentBuilder(doc);
            builder.MoveToBookmark("START");
            ////移动焦点到文档最后,获得原来的页数
            //builder.MoveToDocumentEnd();
            //int oldPageCount = doc.PageCount;
            ////循环加换行，直到页数发生变化为止
            //while (oldPageCount < doc.PageCount)
            //{
            //    builder.Writeln();
            //}
            foreach (string jse in strjse.Split(','))
            {
                string strWhere = " AND bkbm.lb = '" + strKSLB + "' and bkbm.tjxq = '" + strXQ + "' and  bkz = '" + strSJ.Split(',')[0] + "' and bkzc = '" + strSJ.Split(',')[1] + "' and bkjc1 = '" + strSJ.Split(',')[2] + "' and bkjc2 = '" + strSJ.Split(',')[3] + "' and bkjse in ('" + jse + "')";
                DataTable DTPD = new QJ_JWB().GetDTByCommand("SELECT  bj,xh,xm,kcmc,bkjse,'1' as zwh,kslb,'' as bz FROM Bkbm where 1=1 " + strWhere);
                DataTable dabj = DTPD.DefaultView.ToTable(true, new string[] { "bj" });//班级

                builder.Font.Size = 9;
                builder.Font.Name = "宋体";
                builder.Font.Bold = true;
                builder.InsertHtml("<p style='font-size:12px'>学期：" + strXQ + ",                       考场 ：" + jse + "\t                   考试时间:" + strjtSJ + "</p>");
                int zwh = 1;
                for (int i = 0; i < dabj.Rows.Count; i++)
                {
                    DataTable dttemp = DTPD.Where("bj='" + dabj.Rows[i]["bj"].ToString() + "'");
                    for (int m = 0; m < dttemp.Rows.Count; m++)
                    {
                        dttemp.Rows[m]["zwh"] = zwh;
                        dttemp.Rows[m]["kslb"] = dttemp.Rows[m]["kslb"].ToString().Replace("补考","");

                        zwh++;
                    }
                    new FILEManage().dcDatable(doc, builder, dttemp, "班级,学号,姓名,课程,考场,座位号,补考类别,备注");
                    //builder.Write("\n");
                    builder.Writeln();
                }

                builder.InsertBreak(BreakType.SectionBreakNewPage);
            }




            string Filepath = basePath + "\\Export\\";

            string strFileName = DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
            string strFileNamedoc = DateTime.Now.ToString("yyMMddHHmmss") + ".doc";

            doc.Save(Filepath + strFileName, Aspose.Words.Saving.DocSaveOptions.CreateSaveOptions(SaveFormat.Pdf));
            doc.Save(Filepath + strFileNamedoc, Aspose.Words.Saving.DocSaveOptions.CreateSaveOptions(SaveFormat.Doc));

            // doc.Save(Filepath + strFileName, Aspose.Words.Saving.DocSaveOptions.CreateSaveOptions(SaveFormat.Doc));
            msg.Result = "\\Export\\" + strFileName;

        }

        #endregion





    }
}