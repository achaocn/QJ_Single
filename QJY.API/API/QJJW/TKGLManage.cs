﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using System;
using System.Data;

namespace QJY.API
{
    public class TKGLManage
    {




        #region PC端


        /// <summary>
        /// 获取调课代课列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETTKDKLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strXQ = P1;
            string strXB = P2;
            string strZT = context.Request("shzt");
            string strWhere = "xq='" + strXQ + "' and CRUser='" + UserInfo.User.UserName + "' and pz='" + strZT + "'";
            DataTable dtTK = new QJ_JWB().GetDTByCommand(" select *,dbo.fn_xbdm_xbmc(dkxbdm) xbmc,dbo.fn_TransBJ(hbbh) as hbbj,'第'+cast(skz as varchar)+'周,'+'星期'+cast(zc as varchar)+',第'+cast(jc1 as varchar)+'-'+cast(jc2 as varchar)+'节' AS SKXX,'第'+cast(dkz as varchar)+'周,'+'星期'+cast(dkzc as varchar)+',第'+cast(dkjc1 as varchar)+'-'+cast(dkjc2 as varchar)+'节' AS dKXX from dksq where " + strWhere + " ORDER BY CRDate Desc");
            msg.Result = dtTK;

            DataTable dtDK = new QJ_JWB().GetDTByCommand(" select *,dbo.fn_xbdm_xbmc(dkxbdm) xbmc,dbo.fn_TransBJ(hbbh) as hbbj,'第'+cast(skz as varchar)+'周,'+'星期'+cast(zc as varchar)+',第'+cast(jc1 as varchar)+'-'+cast(jc2 as varchar)+'节' AS SKXX from dkgl where " + strWhere + " ORDER BY CRDate Desc");
            msg.Result1 = dtDK;
        }



        public string JSKY(string xq, string m_sksj, string m_dkjse)
        {
            //m_sksj = m_dkz + "," + m_dkzc + "," + m_dkjc1 + "," + m_dkjc2 + ",";
            string strReturn = "";

            JArray arrpro = new JArray();
            arrpro.Add(new JObject() { { "@_whfs", "检查时间教室借用是否有效" } });
            arrpro.Add(new JObject() { { "@_xq", xq } });
            arrpro.Add(new JObject() { { "@_jysj", m_sksj } });
            arrpro.Add(new JObject() { { "@_jse", m_dkjse } });
            DataTable dsjy = new QJ_JWB().GetDataTableByPR("pr_kbgl_jyjse_sq_check", arrpro);
            if (dsjy.Rows.Count > 0)
            {
                if (dsjy.Rows[0]["jy"].ToString() == "1")
                {
                    strReturn = "您借用的教室此刻已被其他人借用了，请重新查询空教室！";

                }
            }
            else
            {
                strReturn = "您借用的教室此刻已被其他人借用了，请重新查询空教室！";

            }
            return strReturn;
        }

        /// <summary>
        /// 获取年级专业
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETNJZY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strnj = context.Request("nj");
            JArray arrpro = new JArray();
            arrpro.Add(new JObject() { { "@_whfs", "查询各年级专业及方向信息" } });
            arrpro.Add(new JObject() { { "@_xbdm", "" } });
            arrpro.Add(new JObject() { { "@_sylb", strnj } });
            DataTable dt = new QJ_JWB().GetDataTableByPR("pr_zydm_sele", arrpro);
            msg.Result = dt;

        }

        /// <summary>
        /// 获取当前周次
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETTKINIT(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //判断有没有权限
            JArray arrpro = new JArray();
            arrpro.Add(new JObject() { { "@_whfs", "当前日期转换为学期周次星期" } });
            arrpro.Add(new JObject() { { "@_rq", "" } });
            DataTable dt = new QJ_JWB().GetDataTableByPR("pr_oa_date_to_zcxq", arrpro);
            string strxq = "";
            if (dt.Rows.Count > 0)
            {
                msg.Result = dt.Rows[0]["week"].ToString();
                strxq = dt.Rows[0]["xq"].ToString();
            }
            else
            {
                msg.Result = "1";

            }

            DataTable dtZCRQ = new QJ_JWB().GetDTByCommand(" select SKZ,MIN(CONVERT(varchar(12) , RQ, 111 ) ) SRQ ,MAX(CONVERT(varchar(12) , RQ, 111 ) ) ERQ from kb_pksj where xq='" + strxq + "' GROUP BY SKZ");
            msg.Result1 = dtZCRQ;
        }



        /// <summary>
        /// 按照上课周查询教师课表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSKKC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strXQ = P1;
            string skz = P2;
            DataTable dt = new QJ_JWB().GetDTByCommand(" SELECT distinct Kb.xq, Kb.kcdm,kbk.kcmc,Kb.zc,kb.jc1,kb.jc2,kb.skzc,Kb.jse, Kb.ds, Kb.zc1, Kb.zc2, Kb.dkzc, kbk.hbs, kbk.hb, dbo.fn_TransBJ(kbk.hb) hbbj, dbo.fn_bh_bmmc(bj.bh) bmmc, kbk.kcxbdm, kbk.jsmc, kbk.fzjs, kbk.kch, dbo.fn_kbgl_kch_hb_xkrs(kbk.xq, kbk.kch, kbk.hb) skrs  FROM dbo.kb Kb, dbo.kbk Kbk, dbo.bj Bj, dbo.kbk_jsmc kbkjs WHERE Kbk.xq = Kb.xq AND Kbk.bh = Kb.bh AND Kbk.kcdm = Kb.kcdm AND Bj.bh = Kbk.bh and kbk.xq = '" + strXQ + "' and kbkjs.XQ = '" + strXQ + "' and kbkjs.BH = kbk.bh and kbkjs.kch = kbk.kch and kbkjs.JSDM = '" + UserInfo.User.UserName + "' and dbo.fn_skzc_Trans_zc(kb.skzc, '" + skz + "') = 1");
            DataTable dtTemp = dt.Clone();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["jc1"].ToString() == "1" && dt.Rows[i]["jc2"].ToString() == "4")
                {
                    DataRow r1 = dtTemp.NewRow();
                    r1.ItemArray = dt.Rows[i].ItemArray;
                    r1["jc1"] = "1";
                    r1["jc2"] = "2";
                    dtTemp.Rows.Add(r1);

                    DataRow r2 = dtTemp.NewRow();
                    r2.ItemArray = dt.Rows[i].ItemArray;
                    r2["jc1"] = "3";
                    r2["jc2"] = "4";
                    dtTemp.Rows.Add(r2);

                }
                else
                {
                    dtTemp.ImportRow(dt.Rows[i]);
                }
            }
            msg.Result = dtTemp;
        }


        /// <summary>
        /// 获取上课周可用节次
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="user"></param>
        public void GETSKZJC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strXQ = P1;
            string tkz = P2;
            string m_kch = context.Request("kch");
            string m_hb = context.Request("hb");

            //判断有没有权限



            JArray arrpro = new JArray();
            arrpro.Add(new JObject() { { "@_whfs", "学年制指定周教师可调课时间" } });
            arrpro.Add(new JObject() { { "@_xq", strXQ } });
            arrpro.Add(new JObject() { { "@_jsdm", UserInfo.User.UserName } });
            arrpro.Add(new JObject() { { "@_jsmc", UserInfo.User.UserRealName } });
            arrpro.Add(new JObject() { { "@_cxz", tkz } });
            arrpro.Add(new JObject() { { "@_kch", m_kch } });
            arrpro.Add(new JObject() { { "@_hb", m_hb } });
            arrpro.Add(new JObject() { { "@_dkxx", " " } });
            DataTable dssj = new QJ_JWB().GetDataTableByPR("pr_kbgl_dk_cxksj", arrpro);

            dssj.Columns.Add("code");
            for (int i = 0; i < dssj.Rows.Count; i++)
            {
                dssj.Rows[i]["code"] = dssj.Rows[i]["zc"].ToString() + "-" + dssj.Rows[i]["jc"].ToString();
            }

            DataTable datemp = dssj.DefaultView.ToTable(true, new string[] { "zc" });//课程
            datemp.Columns.Add("optins", Type.GetType("System.Object"));

            for (int i = 0; i < datemp.Rows.Count; i++)
            {
                DataTable dtT = dssj.Where(" zc='" + datemp.Rows[i]["zc"].ToString() + "'");
                datemp.Rows[i]["optins"] = dtT;
            }
            msg.Result = datemp;
        }

        /// <summary>
        /// 获取可用教室
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="user"></param>
        public void GETSKJS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strXQ = P1;
            string skz = P2;

            //判断有没有权限


            string m_bmmc = context.Request("bmmc");
            string m_zc = context.Request("zc");
            string m_jc1 = context.Request("jc1");
            string m_jc2 = context.Request("jc2");
            string skrs = context.Request("skrs");

            JArray arrpro = new JArray();
            arrpro.Add(new JObject() { { "@_whfs", "查询空教室" } });
            arrpro.Add(new JObject() { { "@_bmmc", m_bmmc } });
            arrpro.Add(new JObject() { { "@_xq", strXQ } });
            arrpro.Add(new JObject() { { "@_cxz", skz } });
            arrpro.Add(new JObject() { { "@_zc", m_zc } });
            arrpro.Add(new JObject() { { "@_jc1", m_jc1 } });
            arrpro.Add(new JObject() { { "@_jc2", m_jc2 } });
            arrpro.Add(new JObject() { { "@_zws1", skrs } });
            arrpro.Add(new JObject() { { "@_zws2", 1000 } });
            DataTable dskjse_dk = new QJ_JWB().GetDataTableByPR("pr_kbgl_cxkjse", arrpro);
            msg.Result = dskjse_dk;
        }




        /// <summary>
        ///  申请调课
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="user"></param>
        public void SQTK(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strXQ = P1;
            //判断有没有权限
            string tklb = context.Request("tklb");//调课类别
            string m_dkyy = context.Request("tkyy");//调课原因
            string skz = P2;//调课周
            string m_zc = context.Request("zc") ?? "";//调课星期
            string m_jc1 = context.Request("jc1") ?? "";//调课节次一
            string m_jc2 = context.Request("jc2") ?? "";//调课节次二
            string m_dkjse = context.Request("skjse") ?? "";//调课教室
            string m_kcdm = context.Request("kcdm") ?? "";
            string m_hb = context.Request("hb") ?? "";
            string ytkxx = context.Request("ytkxx") ?? "";//原调课信息
            string m_sksj = skz + "," + m_zc + "," + m_jc1 + "," + m_jc2 + ",";
            string strJSKY = JSKY(strXQ, m_sksj, m_dkjse);
            if (strJSKY != "")
            {
                msg.ErrorMsg = strJSKY;
                return;
            }
            string[] arrytkxx = ytkxx.Split(',');
            string m_dkxx = skz + "," + m_zc + "," + m_jc1 + "," + m_jc2 + "," + m_dkjse + ","; //调课信息
            string strInsertSQL = "INSERT INTO [dksq] ([xq], [dkxbdm], [kcdm], [kcmc],[hbbh], [jsmc],[skz], [zc], [jc], [jc1], [jc2], [jse], [dkz], [dkzc], [dkjc1], [dkjc2], [dkjc], [dkjse], [dkyy], [pz], [pzr], [scbj], [CRUser], [DCode], [DName], [CRUserName], [CRDate], [intProcessStanceid], [ComID],tklb)" +
                                                 " VALUES ('" + strXQ + "', dbo.fn_kcdm_xbdm('" + m_kcdm + "'), '" + m_kcdm + "', dbo.fn_kcdm_kcmc('" + m_kcdm + "') ,  '" + m_hb + "', '" + UserInfo.User.UserRealName + "', '" + arrytkxx[0].ToString() + "', '" + arrytkxx[1].ToString() + "', '0', '" + arrytkxx[2].ToString() + "', '" + arrytkxx[3].ToString() + "', '" + arrytkxx[4].ToString() + "', '" + skz + "', '" + m_zc + "', '" + m_jc1 + "', '" + m_jc2 + "', '0', '" + m_dkjse + "', '" + m_dkyy + "', '0', '',  '0', '" + UserInfo.User.UserName + "', '" + UserInfo.BranchInfo.RoomCode + "', '" + UserInfo.BranchInfo.DeptName + "', '" + UserInfo.User.UserRealName + "', '" + DateTime.Now + "', '0', '0','" + tklb + "');";
            new QJ_JWB().ExsSql(strInsertSQL);
            msg.Result = "";
        }





        /// <summary>
        ///  调课代课审核列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="user"></param>
        public void GETSHTK(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strXQ = P1;
            string strXB = P2;
            string strZT = context.Request("shzt");
            string strWhere = "xq='" + strXQ + "'and dkxbdm in ('" + strXB.ToFormatLike() + "') and pz='" + strZT + "'";
            DataTable dt = new QJ_JWB().GetDTByCommand(" select *,dbo.fn_xbdm_xbmc(dkxbdm) xbmc,dbo.fn_TransBJ(hbbh) as hbbj,'第'+cast(skz as varchar)+'周,'+'星期'+cast(zc as varchar)+',第'+cast(jc1 as varchar)+'-'+cast(jc2 as varchar)+'节' AS SKXX,'第'+cast(dkz as varchar)+'周,'+'星期'+cast(dkzc as varchar)+',第'+cast(dkjc1 as varchar)+'-'+cast(dkjc2 as varchar)+'节' AS dKXX from dksq where " + strWhere + " ORDER BY CRDate Desc");
            msg.Result = dt;


            DataTable dt1 = new QJ_JWB().GetDTByCommand(" select *,dbo.fn_xbdm_xbmc(dkxbdm) xbmc,dbo.fn_TransBJ(hbbh) as hbbj,'第'+cast(skz as varchar)+'周,'+'星期'+cast(zc as varchar)+',第'+cast(jc1 as varchar)+'-'+cast(jc2 as varchar)+'节' AS SKXX from dkgl where " + strWhere + " ORDER BY CRDate Desc");
            msg.Result1 = dt1;
        }



        /// <summary>
        /// 调课审核
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void TKSH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strXB = P2;
            string strZT = context.Request("shzt");
            string strSHYJ = context.Request("shyj");
            string idns = context.Request("idns");
            new QJ_JWB().ExsSclarSql("update dksq set shyj='" + strSHYJ + "',shsj='" + DateTime.Now + "',pz='" + strZT + "',pzr='" + UserInfo.User.UserName + "'  where xq='" + strXQ + "' and dkxbdm in ('" + strXB.ToFormatLike() + "')  and idn in ('" + idns.ToFormatLike() + "')");
        }



        /// <summary>
        /// 删除调课记录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELTKJL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strXB = P2;
            string idns = context.Request("idns");
            new QJ_JWB().ExsSclarSql("delete dksq where xq='" + strXQ + "' and dkxbdm in ('" + strXB.ToFormatLike() + "')  and idn in ('" + idns.ToFormatLike() + "')");

        }






        /// <summary>
        ///  申请代课
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="user"></param>
        public void SQDK(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strXQ = P1;
            string strJS = P2;

            //判断有没有权限
            string tklb = context.Request("tklb");//代课类别
            string m_dkyy = context.Request("dkyy");//调课原因

            string m_kcdm = context.Request("kcdm") ?? "";
            string m_hb = context.Request("hb") ?? "";
            string ytkxx = context.Request("ytkxx") ?? "";//原调课信息

            string[] arrytkxx = ytkxx.Split(',');
            string strInsertSQL = "INSERT INTO [dkgl] ([xq], [dkxbdm], [kcdm], [kcmc],[hbbh], [jsmc],[skz], [zc], [jc], [jc1], [jc2], [jse],[dkjs],[dkname],[dkyy], [pz], [pzr], [scbj], [CRUser], [DCode], [DName], [CRUserName], [CRDate], [intProcessStanceid], [ComID],tklb)" +
                                                 " VALUES ('" + strXQ + "', dbo.fn_kcdm_xbdm('" + m_kcdm + "'), '" + m_kcdm + "', dbo.fn_kcdm_kcmc('" + m_kcdm + "') ,  '" + m_hb + "', '" + UserInfo.User.UserRealName + "', '" + arrytkxx[0].ToString() + "', '" + arrytkxx[1].ToString() + "', '0', '" + arrytkxx[2].ToString() + "', '" + arrytkxx[3].ToString() + "', '" + arrytkxx[4].ToString() + "', '" + strJS + "',  dbo.fn_jsdm_jsmc('" + strJS + "'),'" + m_dkyy + "', '0',  '','', '" + UserInfo.User.UserName + "', '" + UserInfo.BranchInfo.RoomCode + "', '" + UserInfo.BranchInfo.DeptName + "', '" + UserInfo.User.UserRealName + "', '" + DateTime.Now + "', '0', '0','" + tklb + "');";
            new QJ_JWB().ExsSql(strInsertSQL);
            msg.Result = "";
        }


        /// <summary>
        /// 删除代课记录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELDKJL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strXB = P2;
            string idns = context.Request("idns");
            new QJ_JWB().ExsSclarSql("delete dkgl where xq='" + strXQ + "' and dkxbdm in ('" + strXB.ToFormatLike() + "')  and idn in ('" + idns.ToFormatLike() + "')");

        }




        /// <summary>
        /// 调课代课审核
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DKSH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strXB = P2;
            string strZT = context.Request("shzt");
            string strSHYJ = context.Request("shyj");
            string idns = context.Request("idns");
            new QJ_JWB().ExsSclarSql("update dkgl set shyj='" + strSHYJ + "',shsj='" + DateTime.Now + "',pz='" + strZT + "',pzr='" + UserInfo.User.UserName + "'  where xq='" + strXQ + "' and dkxbdm in ('" + strXB.ToFormatLike() + "')  and idn in ('" + idns.ToFormatLike() + "')");
        }
        #endregion





    }
}