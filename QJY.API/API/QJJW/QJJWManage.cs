﻿using Newtonsoft.Json.Linq;
using QJY.Data;
using System;
using System.Data;
using System.Linq;
using QJY.Common;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace QJY.API
{
    public class QJJWManage
    {




        #region PC端



        public void TBUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtJG = new QJ_JWB().GetDTByCommand("SELECT XBDM AS JGDM,XBMC AS JGMC,IDN FROM jxjd.dbo.xbdm");
            for (int i = 0; i < dtJG.Rows.Count; i++)
            {
                int strJGID = int.Parse(dtJG.Rows[i]["IDN"].ToString());
                string strJGMC = dtJG.Rows[i]["JGMC"].ToString();
                string strJGDM = dtJG.Rows[i]["JGDM"].ToString();

                if (new JH_Auth_BranchB().GetEntities(D => D.RoomCode.ToString() == strJGDM).Count() == 0)
                {
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO [jh_auth_branch] ([ComId], [DeptCode], [DeptName], [DeptShort], [DeptDesc], [DeptRoot], [BranchLevel], [Remark1], [Remark2], [BranchLeader], [WXBMCode], [TXLQX], [RoleQX], [IsHasQX], [RoomCode], [CRUser], [CRDate]) VALUES ('10334', '" + strJGID + "', N'" + strJGMC + "', '2', N'" + strJGMC + "', '1', '0', N'1-" + strJGID + "', N'', N'', '', N'', N'', N'', N'" + strJGDM + "', N'administrator', '2020-07-26 22:24:01.000');");
                }
            }
            DataTable dtUser = new QJ_JWB().GetDTByCommand("SELECT JS.jsdm,JS.JSMC,JS.XBDM,XB.idn FROM jxjd.dbo.jsdm JS LEFT JOIN jxjd.dbo.xbdm XB ON JS.XBDM=XB.XBDM where XB.idn is not null");

            for (int i = 0; i < dtUser.Rows.Count; i++)
            {
                string jsdm = dtUser.Rows[i]["jsdm"].ToString();
                string strJSXM = dtUser.Rows[i]["JSMC"].ToString();
                int jgdm = int.Parse(dtUser.Rows[i]["idn"].ToString());

                JH_Auth_User user = new JH_Auth_UserB().GetEntity(D => D.UserName == jsdm);
                if (user != null)
                {
                    if (user.BranchCode != jgdm)
                    {
                        user.BranchCode = jgdm;
                        new JH_Auth_UserB().Update(user);
                    }

                }
                else
                {
                    user = new JH_Auth_User();
                    user.BranchCode = jgdm;
                    user.UserName = jsdm;
                    user.UserRealName = strJSXM;
                    user.Sex = "男";
                    user.UserPass = "E99A18C428CB38D5F260853678922E03";
                    user.IsUse = "Y";
                    user.remark = "1";
                    user.ComId = 10334;
                    user.UserOrder = 0;
                    user.CRUser = "administrator";
                    user.CRDate = DateTime.Now;

                    new JH_Auth_UserB().Insert(user);
                }
            }


        }



        public void TBZW(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //职务
            DataTable dtzw = new QJ_JWB().GetDTByCommand("SELECT * FROM jxjd.dbo.DJLB");
            for (int i = 0; i < dtzw.Rows.Count; i++)
            {
                int strJGID = int.Parse(dtzw.Rows[i]["IDN"].ToString());
                string strJGMC = dtzw.Rows[i]["DJMC"].ToString().Trim();
                string strJGDM = dtzw.Rows[i]["QXDJ"].ToString().Trim();

                if (new JH_Auth_RoleB().GetEntities(D => D.WXBQCode.ToString() == strJGDM).Count() == 0)
                {
                    new JH_Auth_RoleB().ExsSclarSql("INSERT INTO [QJBI_JW].[dbo].[jh_auth_role] ([ComId], [RoleName], [RoleDec], [PRoleCode], [DisplayOrder], [leve], [IsUse], [isSysRole], [WXBQCode], [RoleQX], [IsHasQX]) VALUES ('10334', N'" + strJGMC + "', N'', '0', '0', '0', N'Y', N'N', '" + strJGDM + "', N'', N'1');");
                }
            }
            DataTable dtUserRole = new QJ_JWB().GetDTByCommand("SELECT * FROM jxjd.dbo.YHDJ");

            for (int i = 0; i < dtUserRole.Rows.Count; i++)
            {
                string jsdm = dtUserRole.Rows[i]["YHDM"].ToString().Trim();
                string strCode = dtUserRole.Rows[i]["QXDJ"].ToString().Trim();
                JH_Auth_Role Role = new JH_Auth_RoleB().GetEntity(D => D.WXBQCode.ToString() == strCode);
                if (new JH_Auth_UserRoleB().GetEntities(D => D.UserName.ToString() == jsdm && D.RoleCode == Role.RoleCode).Count() == 0)
                {
                    JH_Auth_UserRole UserRole = new JH_Auth_UserRole() { ComId = 0, RoleCode = Role.RoleCode, UserName = jsdm };
                    new JH_Auth_UserRoleB().Insert(UserRole);
                }
            }


        }



        /// <summary>
        /// 导入课程数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DRKC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            DataTable dtkc = JsonConvert.DeserializeObject<DataTable>(P1);

            string strCon = new QJ_JWB().GetDBString();
            DBFactory db = new DBFactory(strCon);
            for (int i = 0; i < dtkc.Rows.Count; i++)
            {

                int kkxqs = 0;
                int.TryParse(dtkc.Rows[i]["开课学期数量"].ToString(), out kkxqs);
                string strCCXH = dtkc.Rows[i]["层次序号"].ToString();
                string strXBXH = dtkc.Rows[i]["系部序号"].ToString();
                string strZYXH = dtkc.Rows[i]["专业序号"].ToString();
                for (int m = 1; m < kkxqs + 1; m++)
                {
                    var dt = new Dictionary<string, object>();

                    string strKCBM = new JWHelp().GetXH(strXBXH, strZYXH, strCCXH);
                    string strKCName = dtkc.Rows[i]["课程名称"].ToString().Trim();
                    if (kkxqs > 1)
                    {
                        strKCName = strKCName + "0" + m.ToString();
                    }
                    dt.Add("kcdm", strKCBM);
                    dt.Add("kcmc", strKCName);
                    dt.Add("qjxbmc", dtkc.Rows[i]["系部"].ToString().Trim());
                    dt.Add("qjxbdm", dtkc.Rows[i]["系部序号"].ToString().Trim());
                    dt.Add("jysdm", dtkc.Rows[i]["教研室代码"].ToString().Trim());
                    dt.Add("kcmc_e", "");
                    dt.Add("TPKC", "0");
                    dt.Add("RXSYCS", "0");
                    dt.Add("kcxz", dtkc.Rows[i]["学制"].ToString().Trim());
                    dt.Add("kclb", dtkc.Rows[i]["课程类型"].ToString().Trim());
                    dt.Add("kcsx", dtkc.Rows[i]["课程属性"].ToString().Trim());


                    dt.Add("qjzydm", dtkc.Rows[i]["专业序号"].ToString().Trim());
                    dt.Add("qjzyname", dtkc.Rows[i]["专业名称"].ToString().Trim());
                    dt.Add("qjfx", dtkc.Rows[i]["方向"].ToString().Trim());
                    dt.Add("qjxqs", kkxqs);
                    dt.Add("CRUser", "000000061");
                    dt.Add("CRDate", DateTime.Now);
                    dt.Add("ComID", "0");
                    dt.Add("DCode", UserInfo.User.BranchCode);
                    dt.Add("DName", UserInfo.BranchInfo.DeptName);

                    dt.Add("intProcessStanceid", "0");
                    //新增
                    db.InserData(dt, "kcdm");
                }
            }
            string strSQL = "   UPDATE kcdm SET kcdm.kcxbdm = xbdm.xbdm from kcdm INNER JOIN xbdm ON kcdm.qjxbdm = xbdm.jxmskl";
            new QJ_JWB().ExsSclarSql(strSQL);

        }






        #endregion



        #region  基础方法



        /// <summary>
        /// 获取课程类别
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETCZQX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            string strQXLX = P1;
            DataTable DT = new JH_Auth_QYB().GetDTByCommand("select * from qj_czqx where qxtype=@qxtype", new { qxtype = strQXLX });
            msg.Result = DT;
        }


        /// <summary>
        /// 获取课程类别
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SAVECZQX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            DataTable dt = new DataTable();
            dt = JsonConvert.DeserializeObject<DataTable>(P1);
            List<Dictionary<string, object>> dc = new JH_Auth_QYB().Db.Utilities.DataTableToDictionaryList(dt);//5.0.23版本支持
            var t666 = new JH_Auth_QYB().Db.Updateable(dc).AS("qj_czqx").WhereColumns("id").ExecuteCommand();
        }


        /// <summary>
        /// 初始化教务系统数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJWINITDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //学期数据
            DataTable XQDATA = new JWHelp().GetXQ();
            msg.Result = XQDATA;

            //系部信息
            DataTable DTXB = new JWHelp().GetQXXB(UserInfo.User.UserName, "1,4,17,");
            msg.Result1 = DTXB;


            //年级
            DataTable DTNJ = new JWHelp().GetNJ();
            msg.Result2 = DTNJ;


            //校区信息
            DataTable DTXQ = new JWHelp().GetXiaoQ();
            msg.Result3 = DTXQ;


            //专业信息
            DataTable DTZY = new JWHelp().GetZY();
            msg.Result4 = DTZY;

        }


        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETQXXBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            DataTable DT = new JWHelp().GetQXXB(UserInfo.User.UserName, "1,4,17,");
            msg.Result = DT;

        }



        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJSDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            DataTable DT = new JWHelp().GetJSXX();
            DataTable daBU = DT.DefaultView.ToTable(true, new string[] { "xbdm", "xbmc" });//课程
            //DataTable dtBM = new DataTable();
            //dtBM.Columns.Add("jsmc");
            //dtBM.Columns.Add("jsdm");
            //dtBM.Columns.Add("children", Type.GetType("System.Object"));



            daBU.Columns[0].ColumnName = "jsdm";
            daBU.Columns[1].ColumnName = "jsmc";
            daBU.Columns.Add("children", Type.GetType("System.Object"));
            for (int i = 0; i < daBU.Rows.Count; i++)
            {
                daBU.Rows[i]["children"] = DT.Where("xbdm ='" + daBU.Rows[i]["jsdm"].ToString() + "'");
            }
            msg.Result = DT;
            msg.Result1 = daBU;

        }




        /// <summary>
        /// 获取班级TreeData
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETBJTREEDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            DataTable DT = new JWHelp().GetBJTree();
            msg.Result = DT;
        }




        /// <summary>
        /// 获取课程类别
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETKCLB(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            DataTable DT = new JWHelp().Getkclb();
            msg.Result = DT;
        }
        #endregion





    }
}