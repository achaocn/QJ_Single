﻿using Newtonsoft.Json.Linq;
using System.Data;
using QJY.Common;
using Newtonsoft.Json;
using System.Text;
using System.Collections;

namespace QJY.API
{
    public class JXJHManage
    {

        #region 基础方法
        public string Escape(string s)
        {
            StringBuilder sb = new StringBuilder();
            byte[] byteArr = System.Text.Encoding.Unicode.GetBytes(s);

            for (int i = 0; i < byteArr.Length; i += 2)
            {
                sb.Append("%u");
                sb.Append(byteArr[i + 1].ToString("X2"));//把字节转换为十六进制的字符串表现形式

                sb.Append(byteArr[i].ToString("X2"));
            }
            return sb.ToString();
        }


        public string ChackJS(string strJSName, DataTable dataJS, ref string strJSDM)
        {
            string strMsg = "";
            bool isHasJS = false;
            for (int i = 0; i < dataJS.Rows.Count; i++)
            {
                if (dataJS.Rows[i]["jsmc"].ToString().Trim() == strJSName.Trim(';').Trim())
                {
                    isHasJS = true;
                    strJSDM = dataJS.Rows[i]["jsdm"].ToString();
                }
            }
            if (!isHasJS)
            {
                strMsg = "系统无法找到该老师" + strJSName;
            }
            return strMsg;

        }

        public string ChackHB(string strBJName, DataTable dataHB, ref string bh)
        {
            string strMsg = "";
            foreach (string bj in strBJName.Split(','))
            {
                for (int i = 0; i < dataHB.Rows.Count; i++)
                {
                    if (dataHB.Rows[i]["bj"].ToString().Trim() == bj.Trim(';').Trim())
                    {
                        bh = bh + dataHB.Rows[i]["bh"].ToString().Trim() + ",";
                    }
                }
            }

            if (strBJName.Split(',').Length > bh.Trim(',').Split(',').Length)
            {
                strMsg = "系统无法找到全部班级-" + strBJName;
            }
            return strMsg;
        }
        public string ChackQSZ(string strKZZ, string strJSZ)
        {
            string strMsg = "";
            int ksz = 0;
            int jsz = 0;
            if (!int.TryParse(strKZZ, out ksz) || !int.TryParse(strJSZ, out jsz))
            {
                strMsg = "开始周,结束周数据格式有误";

            }
            return strMsg;
        }
        public string ChackHBDATA(string strHBS, DataTable dtData, string strKCDM)
        {
            string strMsg = "";
            foreach (string BJ in strHBS.Split(','))
            {
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    if (BJ == dtData.Rows[i]["班级"].ToString().Trim().Trim(';').Trim(',') && strKCDM == dtData.Rows[i]["课程代码"].ToString().Trim().Trim(';').Trim(','))
                    {
                        string strHBBJ = dtData.Rows[i]["合班班级"].ToString().Trim().Trim(';').Trim(',');
                        if (strHBBJ != strHBS)
                        {
                            strMsg = "合班班级数据有误!请检查数据!";
                        }
                    }


                }
            }
            return strMsg;
        }

        public string ChackXS(string strZXS, string strXS)
        {
            string strMsg = "";
            double zxs = 0;
            double xs = 0;
            if (double.TryParse(strZXS, out zxs) && double.TryParse(strXS, out xs))
            {
                if (zxs > xs)
                {
                    strMsg = "周学时不能大于总学时";
                }
            }
            else
            {
                strMsg = "周学时格式有误";

            }
            return strMsg;
        }

        public string ChackSJXHDATA(string XH, string XQ, string strBH, string strKCDM, DataTable dtData)
        {
            string strMsg = "";

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                if (XH == dtData.Rows[i]["idn"].ToString())
                {
                    if (dtData.Rows[i]["xq"].ToString() == XQ && dtData.Rows[i]["bh"].ToString() == strBH && dtData.Rows[i]["kcdm"].ToString() == strKCDM)
                    {
                        strMsg = "";
                    }
                    else
                    {
                        strMsg = "序号数据有误!";

                    }
                }


            }

            return strMsg;
        }
        #endregion


        #region PC端


        /// <summary>
        /// 获取年级专业
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETNJZY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strnj = context.Request("nj");
            string strxbdm = context.Request("xbdm");

            string strSQL = string.Format("		select zyxx.*,jhsrqk=case	when jhsrmp.jhs>0 then '已输入' when jhsrmp.jhs Is null then '' end,dbo.fn_zydm_fxdm_nj_xz_bjxx(zyxx.zydm,zyxx.fxdm,zyxx.nj,zyxx.xz) bjxx from  (select distinct bj.nj, zydm.zydm, zydm.zymc, bj.xz, zydm.xbdm, dbo.fn_xbdm_xbmc(zydm.xbdm) xbmc, bj.sylb, bj.fxdm, dbo.fn_zyfxdm_zyfx(bj.fxdm) zyfx from dbo.bj bj, dbo.zydm zydm where bj.zydm = zydm.zydm and bj.nj = '{0}' ) as zyxx left outer join(select nj, zydm, xz, COUNT(nj) as jhs from dbo.jxjh where nj = '{0}' group by nj, zydm, xz) as jhsrmp on zyxx.nj = jhsrmp.nj and zyxx.zydm = jhsrmp.zydm and zyxx.xz = jhsrmp.xz", strnj);

            if (!string.IsNullOrEmpty(strxbdm))
            {
                strSQL = string.Format("		select zyxx.*,jhsrqk=case	when jhsrmp.jhs>0 then '已输入' when jhsrmp.jhs Is null then '' end,dbo.fn_zydm_fxdm_nj_xz_bjxx(zyxx.zydm,zyxx.fxdm,zyxx.nj,zyxx.xz) bjxx from  (select distinct bj.nj, zydm.zydm, zydm.zymc, bj.xz, zydm.xbdm, dbo.fn_xbdm_xbmc(zydm.xbdm) xbmc, bj.sylb, bj.fxdm, dbo.fn_zyfxdm_zyfx(bj.fxdm) zyfx from dbo.bj bj, dbo.zydm zydm where bj.zydm = zydm.zydm and bj.nj = '{0}' AND  zydm.xbdm IN ('{1}') ) as zyxx left outer join(select nj, zydm, xz, COUNT(nj) as jhs from dbo.jxjh where nj = '{0}' group by nj, zydm, xz) as jhsrmp on zyxx.nj = jhsrmp.nj and zyxx.zydm = jhsrmp.zydm and zyxx.xz = jhsrmp.xz", strnj, strxbdm.ToString().ToFormatLike());

            }

            //JArray arrpro = new JArray();
            //arrpro.Add(new JObject() { { "@_whfs", "查询各年级专业及方向信息" } });
            //arrpro.Add(new JObject() { { "@_xbdm", "" } });
            //arrpro.Add(new JObject() { { "@_sylb", strnj } });
            //DataTable dt = new QJ_JWB().GetDataTableByPR("pr_zydm_sele", arrpro);
            DataTable dt = new QJ_JWB().GetDTByCommand(strSQL);


            msg.Result = dt;

        }
        /// <summary>
        /// 获取教学计划
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJXJH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strnj = context.Request("nj");
            string strzydm = context.Request("zydm");
            string strxz = context.Request("xz");
            string m_xs = "0,0,0,0,0,0";
            JArray arrpro = new JArray();
            arrpro.Add(new JObject() { { "@_whfs", "查询专业计划" } });
            arrpro.Add(new JObject() { { "@_nj", strnj } });
            arrpro.Add(new JObject() { { "@_zydm", strzydm } });
            arrpro.Add(new JObject() { { "@_xz", strxz } });
            arrpro.Add(new JObject() { { "@_xq", 0 } });
            arrpro.Add(new JObject() { { "@_kcdm", "" } });
            arrpro.Add(new JObject() { { "@_ksfsdm", 0 } });
            arrpro.Add(new JObject() { { "@_lbdh", "" } });
            arrpro.Add(new JObject() { { "@_xs", m_xs } });
            arrpro.Add(new JObject() { { "@_xf", 0 } });
            arrpro.Add(new JObject() { { "@_fxdm", "" } });
            arrpro.Add(new JObject() { { "@_ksfs", "" } });
            arrpro.Add(new JObject() { { "@_kcxbdm", "" } });
            arrpro.Add(new JObject() { { "@_zc1", 0 } });
            arrpro.Add(new JObject() { { "@_zc2", 0 } });
            arrpro.Add(new JObject() { { "@_idn", 0 } });
            DataTable dtJC = new QJ_JWB().GetDataTableByPR("pr_jxjh_wh", arrpro);
            msg.Result = dtJC;

        }






        /// <summary>
        /// 删除教学计划
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELJXJH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strnj = context.Request("nj");
            string zydm = context.Request("zydm");
            string ids = context.Request("ids");

            string strSQL = string.Format("delete from jxjh where nj = '{0}' and zydm = '{1}' and idn in ('{2}') ", strnj, zydm, ids.ToFormatLike());
            new QJ_JWB().ExsSclarSql(strSQL);
        }




        /// <summary>
        /// 检查导入任务数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void CHECKRWDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable checkdata = new DataTable();
            checkdata = JsonConvert.DeserializeObject<DataTable>(context.Request("P1"));
            string strBJ = "";
            for (int i = 0; i < checkdata.Rows.Count; i++)
            {
                if (!string.IsNullOrEmpty(checkdata.Rows[i]["班级"].ToString()))
                {
                    strBJ = strBJ + checkdata.Rows[i]["班级"].ToString() + ",";
                }
            }
            DataTable dtBJ = new QJ_JWB().GetDTByCommand("SELECT bj,bh FROM bj WHERE BJ IN ('" + strBJ.Trim(',').ToFormatLike() + "')");

            DataTable dtJS = new QJ_JWB().GetDTByCommand("select jsdm,jsmc from jsdm");
            for (int i = 0; i < checkdata.Rows.Count; i++)
            {

                string strJS = checkdata.Rows[i]["上课教师"].ToString().Trim().Trim(';');
                string strHBBJ = checkdata.Rows[i]["合班班级"].ToString().Trim().Trim(';').Trim(',');
                string strKCDM = checkdata.Rows[i]["课程代码"].ToString().Trim().Trim(';').Trim(',');

                string strQSZ = checkdata.Rows[i]["起始周"].ToString().Trim().Trim(';').Trim(',');
                string strJSZ = checkdata.Rows[i]["结束周"].ToString().Trim().Trim(';').Trim(',');
                string strZXS = checkdata.Rows[i]["周学时"].ToString().Trim().Trim(';').Trim(',');
                string strXS = checkdata.Rows[i]["学时"].ToString().Trim().Trim(';').Trim(',');

                if (!string.IsNullOrEmpty(strJS))
                {
                    if (string.IsNullOrEmpty(strHBBJ))
                    {
                        checkdata.Rows[i]["合班班级"] = checkdata.Rows[i]["班级"];

                        strHBBJ = checkdata.Rows[i]["班级"].ToString();
                    }
                    string strhbbh = "";
                    string jsdm = "";
                    string bh = "";

                    string strReturnMsg = ChackJS(strJS, dtJS, ref jsdm) + ChackHB(strHBBJ, dtBJ, ref strhbbh) + ChackQSZ(strQSZ, strJSZ) + ChackXS(strZXS, strXS) + ChackHBDATA(strHBBJ, checkdata, strKCDM);

                    checkdata.Rows[i]["hbbh"] = strhbbh;
                    checkdata.Rows[i]["jsdm"] = jsdm;
                    string tempbj = checkdata.Rows[i]["班级"].ToString();
                    ChackHB(tempbj, dtBJ, ref bh);
                    if (!strHBBJ.Contains(tempbj))
                    {
                        strReturnMsg = strReturnMsg + "合班班级必须包含当前班级";
                    }
                    checkdata.Rows[i]["bh"] = bh;
                    checkdata.Rows[i]["导入状态"] = strReturnMsg;
                }

            }
            msg.Result = checkdata;
        }



        /// <summary>
        /// 批量导入或更新任务数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DRRWDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable drdata = new DataTable();
            drdata = JsonConvert.DeserializeObject<DataTable>(context.Request("P1"));
            string strXQ = context.Request("xq");

            for (int i = 0; i < drdata.Rows.Count; i++)
            {
                ArrayList arrSQL = new ArrayList();

                string BJ = drdata.Rows[i]["班级"].ToString().Trim().Trim(';').Trim(',');
                string BH = drdata.Rows[i]["bh"].ToString().Trim().Trim(';').Trim(',');
                string KCDM = drdata.Rows[i]["课程代码"].ToString().Trim().Trim(';');
                string jsmc = drdata.Rows[i]["上课教师"].ToString().Trim().Trim(';');
                string jsdm = drdata.Rows[i]["jsdm"].ToString().Trim().Trim(';');


                string KCMC = drdata.Rows[i]["课程名称"].ToString().Trim().Trim(';');
                string KCLB = drdata.Rows[i]["类别"].ToString().Trim().Trim(';');
                string KHFS = drdata.Rows[i]["考核"].ToString().Trim().Trim(';');
                string xs = drdata.Rows[i]["学时"].ToString().Trim().Trim(';');
                string xf = drdata.Rows[i]["学分"].ToString().Trim().Trim(';');

                string sjxs = drdata.Rows[i]["实践学时"].ToString().Trim().Trim(';');
                string sjzxs = drdata.Rows[i]["上机周学时"].ToString().Trim().Trim(';');
                string llxs = "0";//理论学时

                if (!string.IsNullOrEmpty(jsdm))
                {
                    string zc1 = drdata.Rows[i]["起始周"].ToString().Trim().Trim(';');
                    string zc2 = drdata.Rows[i]["结束周"].ToString().Trim().Trim(';');
                    string hb = drdata.Rows[i]["hbbh"].ToString().Trim().Trim(';').Trim(',') + ",";
                    string zxs = drdata.Rows[i]["周学时"].ToString().Trim().Trim(';');

                    string hbs = hb.Replace('，', ',').Trim(',').Split(',').Length.ToString();

                    DataTable dtkbk = new QJ_JWB().GetDTByCommand("SELECT * FROM kbk WHERE  kcdm='" + KCDM + "' AND  bh='" + BH + "' AND xq='" + strXQ + "'");
                    if (dtkbk.Rows.Count>0)
                    {
                        string strSQL = "UPDATE kbk SET jsmc='" + jsmc + "',zxs='" + zxs + "',zc1='" + zc1 + "',zc2='" + zc2 + "',hbs='" + hbs + "',hb='" + hb + "' WHERE kcdm='" + KCDM + "' AND  bh='" + BH + "' AND xq='" + strXQ + "'";
                        arrSQL.Add(strSQL);
                    }
                    else
                    {
                        string strInsetSQL = "INSERT INTO [dbo].[kbk] ([xq], [bh], [bj], [kcdm], [kcmc], [lbdh], [zxs], [ksfs], [xss], [syxs], [sjxs], [xs], [xf], [jsmc], [fzjs], [zc1], [zc2], [hbs], [hb], [pk], [cjsr], [scbj], [kcxbdm], [XJBDTJ], [sfpj], [pj_kclxdm], [skzc], [skzs], [jc1],[jc2], [LLXS], [ZXXS], [JSDM], [FZDM], [XSQK], [KKRS], [ZYFXDM], [KCMKDM], [SHJXS], [QTXS], [jsmc_sfzj], [fzjs_sfzj], [KCH], [BZ], [CJSRZT], [SYCJBL], [kcxz], [kclb], [kcsx], [sjzxs], [khfs])  " +
                            "VALUES('" + strXQ + "', '" + BH + "', '" + BJ + "', '" + KCDM + "', '" + KCMC + "', '" + KCLB + "', '" + zxs + "', '" + KHFS + "', '0', '0', '" + sjzxs + "', '" + xs + "', '" + xf + "', '" + jsmc + "', '', '" + zc1 + "', '" + zc2 + "', '" + hbs + "', '" + hb + "', '', '0', '0', dbo.fn_kcdm_xbdm('" + KCDM + "'), '0', '1', 'A', '', '0', '0', '0', '" + llxs + "', '.00', '" + jsdm + "', '', '', '0', '', '', '" + sjxs + "', '.00', '0', '0', '" + KCDM + "', NULL, '', '.00', NULL, NULL, NULL, '" + sjzxs + "', NULL); ";
                        arrSQL.Add(strInsetSQL);
                    }


                    string strDELSQL = "DELETE kbk_jsmc   WHERE kcdm='" + KCDM + "' AND  BH='" + BH + "' AND xq='" + strXQ + "'";
                    string stADDSQL = "INSERT INTO [KBK_JSMC] ([XQ], [KCDM],  [BH], [JSDM], [JSMC], [ZJJS], [SFPJ], [CJSR], [PXH], [KCH], [HBS], [HB], [jsskzc]) VALUES " +
                        "('" + strXQ + "', '" + KCDM + "',  '" + BH + "', '" + jsdm + "', '" + jsmc + "', '1', '1', '0', '1', '" + KCDM + "', '" + hbs + "', '" + hb + "', NULL);";
                    arrSQL.Add(strDELSQL);
                    arrSQL.Add(stADDSQL);

                }
                new QJ_JWB().ExecuteSqlTran(arrSQL);
            }
            msg.Result = drdata;
        }
        #endregion





    }
}