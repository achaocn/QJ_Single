﻿using Newtonsoft.Json.Linq;
using QJY.Common;
using System.Data;

namespace QJY.API
{
    public class GZLManage
    {




        #region PC端

        /// <summary>
        /// 获取生成工作量的教学任务
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETDSCJXRW(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strType = P2;
            string strWhere = " NOT IN ";
            if (P2 == "1")
            {
                strWhere = " IN ";
                //获取已经生成上课记录的教学任务
            }
            string strSQL = "select bh,bj,kcdm,kcmc,jsmc,hb,dbo.fn_TransBJ(hb) hbbj,dbo.fn_xbdm_xbmc(kcxbdm) kcxbmc from kbk where xq='" + strXQ + "' and jsmc!='' and pk='已排' and lbdh!='S' and rtrim(kcdm)+rtrim(hb) " + strWhere + " (select distinct rtrim(kcdm)+rtrim(hb) from KB_KCJH_GZL where xq='" + strXQ + "')";
            DataTable dtReturn = new QJ_JWB().GetDTByCommand(strSQL);

            msg.Result = dtReturn;

        }


        /// <summary>
        /// 单个数据重新生成
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETSKJHGZL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string kcdms = context.Request("kcdm", "");
            string hbs = context.Request("hb", "");

            int count = 0;
            foreach (var item in kcdms.Split('$'))
            {
                JArray arrprobczt = new JArray();
                arrprobczt.Add(new JObject() { { "@_whfs", "生成教师课程授课计划次数" } });
                arrprobczt.Add(new JObject() { { "@_xq", strXQ } });
                arrprobczt.Add(new JObject() { { "@_kch", kcdms.Split('$')[count] } });
                arrprobczt.Add(new JObject() { { "@_hb", hbs.Split('$')[count] } });
                arrprobczt.Add(new JObject() { { "@_sj", "" } });

                DataTable dtReturn = new QJ_JWB().GetDataTableByPR("pr_kbgl_kcdm_skjcmx_new_gzl", arrprobczt);
                count++;
            }
            msg.Result = "";
        }

        /// <summary>
        /// 时间转换
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SJZHGZL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;


            JArray arrprobczt = new JArray();
            arrprobczt.Add(new JObject() { { "@_whfs", "批量转换上课日期" } });
            arrprobczt.Add(new JObject() { { "@_xq", strXQ } });
            arrprobczt.Add(new JObject() { { "@_kch", "" } });
            arrprobczt.Add(new JObject() { { "@_hb", "" } });
            arrprobczt.Add(new JObject() { { "@_sj", "" } });
            arrprobczt.Add(new JObject() { { "@_skz", 0 } });
            arrprobczt.Add(new JObject() { { "@_zc", 0 } });
            DataTable dtReturn = new QJ_JWB().GetDataTableByPR("pr_ysc_skjcmx_gzl", arrprobczt);

            msg.Result = "";
        }


        /// <summary>
        /// 获取已生成的工作量记录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETGZL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSQL = "select xq,kcdm,dbo.fn_kcdm_kcmc(kcdm) as kcmc,skjh,kcxbdm,dbo.fn_xbdm_xbmc(kcxbdm) as kcxbmc,hb,dbo.fn_TransBJ(hb) as hbbj,jsdm,dbo.fn_jsdm_jsmc(jsdm) as jsmc, skz, zc, jc1, jc2, sfsk, dkjsdm, dbo.fn_jsdm_jsmc(dkjsdm) as dkjsmc, sclb, dbo.fn_hb_tj_xsrs(hb) as skrs, kss, dbo.fn_date_cdate(skrq) as skrq, idn from KB_KCJH_GZL where xq = '" + context.Request("xq") + "' and kcxbdm = '" + context.Request("xbdm") + "'";
            if (context.Request("qrzt") == "0")
            {
                strSQL = strSQL + " AND (sfsk IS NULL OR sfsk='0') ";
            }
            if (context.Request("qrzt") == "1")
            {
                strSQL = strSQL + " AND sfsk='1'";

            }
            if (context.Request("skz") != "")
            {
                strSQL = strSQL + " and SKZ = '" + context.Request("skz") + "'";

            }
            DataTable dtReturn = new QJ_JWB().GetDTByCommand(strSQL);

            msg.Result = dtReturn;
        }


        /// <summary>
        /// 上课确认或者取消动作
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SKCZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strwhfs = "秘书上课信息确认";
            if (P1 == "1")
            {
                strwhfs = "秘书上课信息撤销";
            }

            string ids = context.Request("idn", "");
            foreach (var item in ids.Split(','))
            {
                int id = int.Parse(item);
                JArray arrprobczt = new JArray();
                arrprobczt.Add(new JObject() { { "@_whfs", strwhfs } });
                arrprobczt.Add(new JObject() { { "@_xq", "" } });
                arrprobczt.Add(new JObject() { { "@_kch", UserInfo.User.UserName } });
                arrprobczt.Add(new JObject() { { "@_hb", "" } });
                arrprobczt.Add(new JObject() { { "@_sj", "" } });
                arrprobczt.Add(new JObject() { { "@_skz", id } });
                arrprobczt.Add(new JObject() { { "@_zc", 0 } });
                DataTable dtReturn = new QJ_JWB().GetDataTableByPR("pr_ysc_skjcmx_gzl", arrprobczt);
            }
            msg.Result = "";
        }


        /// <summary>
        /// 设置代课老师信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETDKLS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSQL = "update KB_KCJH_GZL set dkjsdm = '" + P1.Trim() + "' where IDN IN( '" + P2.Trim().ToFormatLike(',') + "')";
            new QJ_JWB().ExsSclarSql(strSQL);
        }


        #endregion





    }
}