﻿using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;

namespace QJY.API
{
    public class JCGLManage
    {


        #region PC端
        /// <summary>
        /// 获取待选教材的教学计划
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETDXJC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strType = P2;
            string _whfs = "";
            string strjsdm = context.Request("jsdm").ToString();
            string strkcdm = context.Request("kcdm").ToString();
            DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
            DataTable dtReturn = new DataTable();
            if (strType == "1")
            {
                _whfs = "查询教师学期教材选用情况";
                dtReturn = dbccgc.GetDBClient().Ado.GetDataTable("select distinct '教学任务' as lx,kbk.xq,@jsdm  as jsdm,kbk.kcdm,kbk.kcmc,kbk.lbdh,kbk.xs,kbk.hb,jxjd.dbo.fn_bh_bmmc(jxjd.dbo.fn_hb_Trans_firstbh(kbk.hb)) AS bmmc,jxjd.dbo.fn_TransBj(kbk.hb) as skbj  from jxjd.dbo.kbk kbk  INNER JOIN jxjd.dbo.kbk_jsmc kbkjs ON kbkjs.bh = kbk.bh and kbkjs.kch = kbk.kch and kbkjs.xq = kbk.xq where kbk.xq =@xq  and kbkjs.jsdm =@jsdm ", new { xq = strXQ, jsdm = strjsdm });

            }
            else
            {
                if (strkcdm!="")
                {
                    _whfs = "查询课程学期教材选用情况(ALL)";
                    dtReturn = dbccgc.GetDBClient().Ado.GetDataTable("select distinct '教学任务' as lx,kbk.xq,@jsdm  as jsdm,kbk.kcdm,kbk.kcmc,kbk.lbdh,kbk.xs,kbk.hb,jxjd.dbo.fn_bh_bmmc(jxjd.dbo.fn_hb_Trans_firstbh(kbk.hb)) AS bmmc,jxjd.dbo.fn_TransBj(kbk.hb) as skbj  from jxjd.dbo.kbk kbk  INNER JOIN jxjd.dbo.kbk_jsmc kbkjs ON kbkjs.bh = kbk.bh and kbkjs.kch = kbk.kch and kbkjs.xq = kbk.xq where kbk.xq =@xq  and kbk.kcdm =@kcdm ", new { xq = strXQ, kcdm = strkcdm });

                }
            }
            dtReturn.Columns.Add("bjrs");

            dtReturn.Columns.Add("jsyssl");
            dtReturn.Columns.Add("shqk");
            dtReturn.Columns.Add("xyshqk");
            dtReturn.Columns.Add("jwcshqk");

            dtReturn.Columns.Add("jcdm");
            dtReturn.Columns.Add("jcmc");
            dtReturn.Columns.Add("jcmx", Type.GetType("System.Object"));

            for (int i = 0; i < dtReturn.Rows.Count; i++)
            {
                string strHB = dtReturn.Rows[i]["hb"].ToString().Trim(',');
                string strKCDM = dtReturn.Rows[i]["kcdm"].ToString().Trim(',');

                dtReturn.Rows[i]["bjrs"] = new JWHelp().GetBJRS(strHB);
                DataTable dtcbs = dbccgc.GetDBClient().Ado.GetDataTable("SELECT DISTINCT jc_jxjhjc.jcdm,jcmc,bzz,nf,cbs.cbs,jsyssl,JCK.dj,jc_jxjhjc.sh,jc_jxjhjc.xysh,jc_jxjhjc.jwcsh  FROM jc_jxjhjc INNER JOIN JCK ON jc_jxjhjc.jcdm = JCK.ISBN  LEFT JOIN CBS ON JCK.CBSDM = CBS.CBSDM  WHERE FFXQ = '" + strXQ + "' AND KCDM = '" + strKCDM + "' AND bh IN('" + strHB.ToFormatLike() + "')");

                if (dtcbs.Rows.Count > 0)
                {
                    dtReturn.Rows[i]["jsyssl"] = dtcbs.Rows[0]["jsyssl"].ToString();
                    dtReturn.Rows[i]["jcdm"] = dtcbs.GetDTColum("jcdm");
                    dtReturn.Rows[i]["jcmc"] = dtcbs.GetDTColum("jcmc");
                    dtReturn.Rows[i]["jcmx"] = dtcbs;

                    if (dtcbs.Rows[0]["sh"].ToString() == "1")
                    {
                        dtReturn.Rows[i]["shqk"] = "通过";
                    }
                    if (dtcbs.Rows[0]["xysh"].ToString() == "1")
                    {
                        dtReturn.Rows[i]["xyshqk"] = "通过";
                    }
                    if (dtcbs.Rows[0]["jwcsh"].ToString() == "1")
                    {
                        dtReturn.Rows[i]["jwcshqk"] = "通过";
                    }
                }

            }

            DataTable dtKYJC = dbccgc.GetDBClient().Ado.GetDataTable("select jcdm,jcmc,bzz,nf,bs,jck.cbsdm,cbs.cbs,dj,isbn,jck.idn,ISNULL(jck.sy,0) sy  from jck,cbs where jck.cbsdm=cbs.cbsdm and jck.sy=1	");
            msg.Result = dtReturn;
            msg.Result1 = dtKYJC;


        }

        /// <summary>
        /// 教师选教材
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETJC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strKCDATA = P2;
            JArray KC = JArray.Parse(strKCDATA);

            string strJCDM = context.Request("jcdm").ToString();
            for (int m = 0; m < strJCDM.Split(',').Length; m++)
            {
                string strjcdm = strJCDM.Split(',')[m].ToString() + ",0,";

                string strhblist = "";
                foreach (JObject item in KC)
                {
                    string[] hbs = ((string)item["hb"].ToString().Trim(',')).Split(',');
                    string[] bjrss = ((string)item["bjrs"].ToString().Trim(',')).Split(',');

                    for (int i = 0; i < hbs.Length; i++)
                    {
                        strhblist = strhblist + hbs[i] + "," + bjrss[i] + ",";

                    }
                    string strkcdm = (string)item["kcdm"].ToString();
                    if (strhblist != "")
                    {
                        List<SugarParameter> ListP = new List<SugarParameter>();
                        ListP.Add(new SugarParameter("@_whfs", "选用课程教材(含征订号)"));
                        ListP.Add(new SugarParameter("@_xq", strXQ));
                        ListP.Add(new SugarParameter("@_bhlist", strhblist));
                        ListP.Add(new SugarParameter("@_kcdm", strkcdm));
                        ListP.Add(new SugarParameter("@_jcdm", strjcdm));
                        ListP.Add(new SugarParameter("@_czy", UserInfo.User.UserName));
                        ListP.Add(new SugarParameter("@_jcsl", context.Request("jssl").ToString()));
                        DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
                        DataTable dtReturn = dbccgc.GetDBClient().Ado.UseStoredProcedure().GetDataTable("pr_jcgl_jcxy_new_jcnum", ListP);
                    }
                }
            }




        }

        /// <summary>
        /// 锁定教材,锁定后不可修改,
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SDJC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strIdns = P1;
            string strstatus = P2;
            DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
            if (P2 == "Y")
            {
                //锁定
                dbccgc.GetDBClient().Ado.ExecuteCommand("update jc_jxjhjc set jwcsh='1',sdtime=GETDATE() where idn in ('" + strIdns.ToFormatLike() + "')");
            }
            else
            {
                dbccgc.GetDBClient().Ado.ExecuteCommand("update jc_jxjhjc set jwcsh=null,sdtime=null,sh='',xysh='' where idn in ('" + strIdns.ToFormatLike() + "')");

            }

        }
        /// <summary>
        /// 教研室系部选教材
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void JSYYBSETJC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strKCDATA = P2;
            JArray KC = JArray.Parse(strKCDATA);

            string strJCDM = context.Request("jcdm").ToString();
            for (int i = 0; i < strJCDM.Split(',').Length; i++)
            {
                string strjcdm = strJCDM.Split(',')[i].ToString() + ",0,";
                string strhblist = "";
                foreach (JObject item in KC)
                {
                    string strBH = (string)item["bh"].ToString().Trim(',');
                    string bjrss = new JWHelp().GetBJRS(strBH);
                    strhblist = strBH + "," + bjrss + ",";
                    string strkcdm = (string)item["kcdm"].ToString();
                    if (strhblist != "")
                    {
                        //选过的就没法再改了....
                        List<SugarParameter> ListP = new List<SugarParameter>();
                        ListP.Add(new SugarParameter("@_whfs", "选用课程教材(含征订号)"));
                        ListP.Add(new SugarParameter("@_xq", strXQ));
                        ListP.Add(new SugarParameter("@_bhlist", strhblist));
                        ListP.Add(new SugarParameter("@_kcdm", strkcdm));
                        ListP.Add(new SugarParameter("@_jcdm", strjcdm));
                        ListP.Add(new SugarParameter("@_czy", UserInfo.User.UserName));
                        ListP.Add(new SugarParameter("@_jcsl", context.Request("jssl").ToString()));
                        DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
                        DataTable dtReturn = dbccgc.GetDBClient().Ado.UseStoredProcedure().GetDataTable("pr_jcgl_jcxy_new_jcnum", ListP);
                    }
                }
            }


        }

        /// <summary>
        /// 教研室系部取消教材
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELJYSJCXYXX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strKCDATA = P2;
            JArray KC = JArray.Parse(strKCDATA);
            DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);

            foreach (JObject item in KC)
            {
                string hbs = (string)item["bh"].ToString().Trim(',');
                string strkcdm = (string)item["kcdm"].ToString().Trim(',');

                dbccgc.GetDBClient().Ado.GetDataTable(" delete from jc_jxjhjc where ffxq='" + strXQ + "' and bh  in('" + hbs.ToFormatLike() + "') and kcdm = '" + strkcdm + "'");

            }
        }


        /// <summary>
        /// 删除选教材信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELJCXYXX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strKCDATA = P2;
            JArray KC = JArray.Parse(strKCDATA);
            DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);

            foreach (JObject item in KC)
            {
                string hbs = (string)item["hb"].ToString().Trim(',');
                string strkcdm = (string)item["kcdm"].ToString().Trim(',');

                dbccgc.GetDBClient().Ado.GetDataTable(" delete from jc_jxjhjc where ffxq='" + strXQ + "' and bh  in('" + hbs.ToFormatLike() + "') and kcdm = '" + strkcdm + "'");

            }



        }



        /// <summary>
        /// 获取教研室审核教材
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJYSSHDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            //List<SugarParameter> ListP = new List<SugarParameter>();
            //ListP.Add(new SugarParameter("@_whfs", "按教研室汇总教师选用教材"));
            //ListP.Add(new SugarParameter("@_xq", strXQ));
            //ListP.Add(new SugarParameter("@_bh", ""));
            //ListP.Add(new SugarParameter("@_xh", ""));
            //ListP.Add(new SugarParameter("@_dm", UserInfo.User.UserName));
            //DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
            //DataTable dtReturn = dbccgc.GetDBClient().Ado.UseStoredProcedure().GetDataTable("pr_jcgl_jcxy_cx", ListP);


            DataTable dtQXJYS = new QJ_JWB().GetDTByCommand(" select dm, jxjd.dbo.fn_jysdm_jysmc(dm) as jysmc from jxjd.dbo.yhqtqx where yhdm = '" + UserInfo.User.UserName + "' and lb = '教研室代码' and qxdj = 5  ");

            string strJYS = "";
            strJYS = dtQXJYS.GetDTColum("dm");

            DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
            string strSQL = "	select kbk.xq,kbk.kcdm,kcdm.kcmc,kbk.lbdh,kbk.xs,kbk.bh,bj.bj,bj.nj,bj.bjrs,jxjd.dbo.fn_kbgl_bh_kcdm_jsmclist(kbk.xq,kbk.bh,kbk.kch) jsmc," +
                   " kcdm.jysdm,kbk.kcxbdm,bj.xbdm,jxjd.dbo.fn_xbdm_xbmc(bj.xbdm) AS xbmc,jcxy.idn,jcxy.SH,jck.jcdm,jck.jcmc,jck.bzz,jck.dj,jck.nf,jcxy.jwcsh,jys.jysmc,jck.ISBN,jck.bs,jcxy.jsyssl,jcxy.idn as xyid from jxjd.dbo.kbk kbk INNER JOIN jxjd.dbo.kcdm kcdm ON kbk.kcdm = kcdm.kcdm INNER JOIN  jxjd.dbo.bj bj ON kbk.bh = bj.bh  LEFT JOIN jc_jxjhjc jcxy ON kbk.xq = jcxy.ffxq and kbk.bh = jcxy.bh and kbk.kcdm = jcxy.kcdm LEFT JOIN jck ON jcxy.jcdm = jck.jcdm LEFT JOIN  jxjd.dbo.jysdm jys ON kcdm.jysdm=jys.jysdm where  kbk.xq = '" + strXQ + "'  AND kcdm.jysdm in ('" + strJYS.ToFormatLike() + "')";
            DataTable dtReturn = dbccgc.GetDBClient().Ado.GetDataTable(strSQL);
            dtReturn.Columns.Add("jwcshqk");
            dtReturn.Columns.Add("jysshqk");

            for (int i = 0; i < dtReturn.Rows.Count; i++)
            {
                if (dtReturn.Rows[i]["jwcsh"].ToString() == "1")
                {
                    dtReturn.Rows[i]["jwcshqk"] = "通过";
                }
                if (dtReturn.Rows[i]["SH"].ToString() == "1")
                {
                    dtReturn.Rows[i]["jysshqk"] = "通过";
                }


            }


            DataTable dtKYJC = dbccgc.GetDBClient().Ado.GetDataTable("select jcdm,jcmc,bzz,nf,bs,jck.cbsdm,cbs.cbs,dj,isbn,jck.idn,ISNULL(jck.sy,0) sy  from jck,cbs where jck.cbsdm=cbs.cbsdm and jck.sy=1	");

            msg.Result = dtReturn;
            msg.Result1 = dtKYJC;


        }



        /// <summary>
        ///教研室审核教材
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SHJYSSHDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strISSH = context.Request("shzt");
            string strSHSM = "";
            string stridn = context.Request("idns"); ;
            DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
            DataTable dtReturn = dbccgc.GetDBClient().Ado.GetDataTable("update jc_jxjhjc set shrq = getdate(), sh = '" + strISSH + "', shrdm = '" + UserInfo.User.UserName + "', sm = '" + strSHSM + "' where idn in ('" + stridn.ToFormatLike() + "')");


        }



        /// <summary>
        /// 获取院系审核教材
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYXSHDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {



            string strXQ = P1;
            string strxbdm = P2;
            List<SugarParameter> ListP = new List<SugarParameter>();
            ListP.Add(new SugarParameter("@_whfs", "按院系汇总教师选用教材"));
            ListP.Add(new SugarParameter("@_xq", strXQ));
            ListP.Add(new SugarParameter("@_bh", strxbdm));
            ListP.Add(new SugarParameter("@_xh", ""));
            ListP.Add(new SugarParameter("@_dm", strxbdm));
            DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
            DataTable dtReturn = dbccgc.GetDBClient().Ado.UseStoredProcedure().GetDataTable("pr_jcgl_jcxy_cx", ListP);

            dtReturn.Columns.Add("jwcshqk");
            for (int i = 0; i < dtReturn.Rows.Count; i++)
            {
                if (dtReturn.Rows[i]["jwcsh"].ToString() == "1")
                {
                    dtReturn.Rows[i]["jwcshqk"] = "通过";

                }
                dtReturn.Rows[i]["shrq"] = dtReturn.Rows[i]["shrq"].ToString().Replace("1900-01-01", "");
                dtReturn.Rows[i]["xyshrq"] = dtReturn.Rows[i]["xyshrq"].ToString().Replace("1900-01-01", "");


            }


            msg.Result = dtReturn;


        }


        /// <summary>
        /// 院系审核教材
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SHXYSHDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strISSH = context.Request("shzt");
            string strSHSM = "";
            string stridn = context.Request("idns"); ;
            DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
            DataTable dtReturn = dbccgc.GetDBClient().Ado.GetDataTable("update jc_jxjhjc set xyshrq = getdate(), xysh = '" + strISSH + "', xyshrdm = '" + UserInfo.User.UserName + "', xysm = '" + strSHSM + "' where idn in ('" + stridn.ToFormatLike() + "')");
        }






        /// <summary>
        /// 获取教材汇总
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJCHZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strXQ = P1;
            string strxbdm = P2;

            if (strxbdm == "")
            {
                strxbdm = new JWHelp().GetALLXB();
            }
            string strnj = context.Request("nj");

            List<SugarParameter> ListP = new List<SugarParameter>();
            ListP.Add(new SugarParameter("@_whfs", "查询学期院系教材选用信息(选择年级)"));
            ListP.Add(new SugarParameter("@_xq", strXQ));
            ListP.Add(new SugarParameter("@_bh", ""));
            ListP.Add(new SugarParameter("@_xh", strnj));
            ListP.Add(new SugarParameter("@_dm", strxbdm));
            DBFactory dbccgc = new BI_DB_SourceB().GetDB(2);
            DataTable dtReturn = dbccgc.GetDBClient().Ado.UseStoredProcedure().GetDataTable("pr_jcgl_jcxy_cx", ListP);
            msg.Result = dtReturn;


        }

        #endregion





    }
}