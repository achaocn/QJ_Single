﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QJY.API
{
    class SFJSHelp
    {
        /// <summary>
        /// 获取移动页面目录
        /// </summary>
        /// <param name="strQYName"></param>
        /// <returns></returns>
        public static string GetMobPath(string strQYName)
        {
            string websit = "";
            if (strQYName.Contains("宝鸡"))
            {
                websit = "SFJS";
            }
            if (strQYName.Contains("石头河"))
            {
                websit = "SFJSSTH";
            }
            if (strQYName.Contains("交口"))
            {
                websit = "SFJSJK";
            }
            return websit;
        }
    }
}
