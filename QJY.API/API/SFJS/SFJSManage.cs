﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace QJY.API
{
    public class SFJSManage
    {


        #region 基础方法

        public void DelZData(string zid)
        {
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_xzuser WHERE ZID='" + zid + "'");
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_xzq WHERE ZID='" + zid + "'");
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_sfjs_jfuser WHERE ZID='" + zid + "'");
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_db WHERE ZID='" + zid + "'");
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_cz WHERE ZID='" + zid + "'");
            new JH_Auth_BranchB().ExsSclarSql("DELETE qj_db_xz from  qj_db_xz INNER JOIN qj_cz ON qj_cz.ID=qj_db_xz.xzid WHERE qj_cz.ZID ='" + zid + "'");

        }
        public DataTable ToDataTable(string json)
        {
            DataTable dataTable = new DataTable();  //实例化
            DataTable result;
            try
            {

                ArrayList arrayList = JsonConvert.DeserializeObject<ArrayList>(json);
                if (arrayList.Count > 0)
                {
                    foreach (Dictionary<string, object> dictionary in arrayList)
                    {
                        if (dictionary.Keys.Count == 0)
                        {
                            result = dataTable;
                            return result;
                        }
                        if (dataTable.Columns.Count == 0)
                        {
                            foreach (string current in dictionary.Keys)
                            {
                                dataTable.Columns.Add(current, dictionary[current].GetType());
                            }
                        }
                        DataRow dataRow = dataTable.NewRow();
                        foreach (string current in dictionary.Keys)
                        {
                            dataRow[current] = dictionary[current];
                        }

                        dataTable.Rows.Add(dataRow); //循环添加行到DataTable中
                    }
                }
            }
            catch
            {
            }
            result = dataTable;
            return result;
        }


        public string getZinfo(string strZid)
        {
            string strZinfo = "";
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(10334, int.Parse(strZid));
            strZinfo = branch.DeptName;
            if (branch.DeptRoot != 1)
            {
                JH_Auth_Branch Pbranch = new JH_Auth_BranchB().GetBMByDeptCode(10334, branch.DeptRoot);

                strZinfo = Pbranch.DeptName + "/" + strZinfo;
            }

            return strZinfo;
        }


        public DataTable getXZ(string strXZID)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_cz WHERE id='" + strXZID + "'");
            return dt;
        }
        /// <summary>
        /// 获取总站
        /// </summary>
        /// <param name="strzzname"></param>
        /// <returns></returns>
        public DataTable getZZ(string strzzname)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM jh_auth_branch WHERE deptname='" + strzzname + "'");
            return dt;
        }

        public DataTable getDinfo(string strZQ, string strDQ, string strDduanQ, string strZid)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_db WHERE (dbname='" + strDQ + "' OR dbname='" + strDQ + "斗') AND gzqname='" + strZQ + "'AND dqname='" + strDduanQ + "' AND  zid='" + strZid + "'");
            return dt;
        }
        public DataTable getCzinfo(string strXZQ, string strDQ, string strxzname, string strZid)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_cz WHERE ZID='" + strZid + "'AND xzname='" + strxzname + "' AND pname='" + strXZQ + "' AND dbname='" + strDQ + "'");
            return dt;
        }

        public DataTable getCzinfoV(string strXZQ, string strxzname, string strZid)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_cz WHERE ZID='" + strZid + "'AND xzname='" + strxzname + "' AND  pname like '%" + strXZQ + "%'");
            return dt;
        }

        public DataTable getDIDByName(string strZQ, string strDQ, string strDuanQ, string strZid)
        {

            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT id,dbname FROM qj_db WHERE dbname IN ('" + strDQ.ToFormatLike(',') + "') AND dqname='" + strDuanQ + "'  AND gzqname='" + strZQ + "' AND  zid='" + strZid + "'");
            return dt;
        }

        public DataTable getXZinfo(string strXZ, string strC, string strZid)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_xzq WHERE czname='" + strC + "' AND pname like '%" + strXZ + "%' AND  zid='" + strZid + "'");
            return dt;
        }

        public bool isExitDB(string strDQ, string strZQ, string strDUANQ, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_db WHERE dbname='" + strDQ + "' AND gzqname='" + strZQ + "' AND dqname='" + strDUANQ + "' AND  zid='" + ZID + "'").Rows.Count == 0;
        }

        /// <summary>
        /// 存在该灌溉用户
        /// </summary>
        /// <param name="XZID"></param>
        /// <param name="SFZBM"></param>
        /// <returns></returns>
        public bool ExitGGYH(string XZID, string SFZBM)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT ID FROM qj_sfjs_jfuser WHERE xzid = '" + XZID + "' AND sfzbm = '" + SFZBM + "'").Rows.Count > 0;
        }



        public DataTable isExitXS(string strXSName, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_xzq WHERE CZname='" + strXSName + "' AND type='县市' AND  zid='" + ZID + "'");
        }


        public DataTable isExitXZ(string strXSName, string strXZName, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_xzq WHERE CZname='" + strXZName + "' AND pname='" + strXSName + "' AND type='乡镇' AND  zid='" + ZID + "'");
        }


        public DataTable isExitC(string strXSName, string strXZName, string strCName, string ZID)
        {
            string strXX = strXSName + "/" + strXZName;
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_xzq WHERE CZname='" + strCName + "' AND pname='" + strXX + "' AND type='村' AND  zid='" + ZID + "'");
        }

        public DataTable isExitCZ(string strCZName, string strXZQName, string strDBName, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM vw_dbcz WHERE xzname='" + strCZName + "' AND dbname='" + strDBName + "' AND pname LIKE '%" + strXZQName + "' AND  zid='" + ZID + "'");
        }
        public DataTable isExitCZV1(string strCZName, string strXZQName, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM vw_dbcz WHERE xzname='" + strCZName + "'  AND pname LIKE '%" + strXZQName + "' AND  zid='" + ZID + "'");
        }

        public DataTable isExitXM(string strSFZHM, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_sfjs_jfuser WHERE  sfzbm='" + strSFZHM + "' AND  zid='" + ZID + "'");
        }

        public DataTable isExitXMV1(string strSFZHM, string ZNAME)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_sfjs_jfuser WHERE  sfzbm='" + strSFZHM + "' AND  zname LIKE '%" + ZNAME + "'");
        }


        /// <summary>
        /// 获取年度
        /// </summary>
        /// <param name="strSFZHM"></param>
        /// <param name="ZNAME"></param>
        /// <returns></returns>
        public DataTable GETNDDATA()
        {
            DataTable dtGGND = new JH_Auth_UserB().GetDTByCommand("select TypeNO from jh_auth_zidian where class='灌溉年度'  ORDER BY TypeNO DESC ");
            dtGGND.Columns.Add("gjdata", Type.GetType("System.Object"));
            for (int i = 0; i < dtGGND.Rows.Count; i++)
            {
                dtGGND.Rows[i]["gjdata"] = getnddata(dtGGND.Rows[i]["TypeNO"].ToString());
            }
            return dtGGND;
        }


        /// <summary>
        /// 获取总站或者管理站得用水量,灌溉亩数,灌溉金额,已支付金额得合计数量
        /// </summary>
        /// <param name="glzname"></param>
        /// <param name="sdate"></param>
        /// <param name="edate"></param>
        /// <param name="TYPE"></param>
        /// <param name="striszz"></param>
        /// <returns></returns>
        public string getdata(string glzname, string sdate, string edate, string TYPE, string striszz = "N")
        {
            DataTable dtReturn = new DataTable();
            string glzwhere = "";
            if (glzname != "")
            {
                if (striszz == "N")
                {
                    glzwhere = " AND  GLZNAME LIKE '%" + glzname + "%'";
                    //获取管理站数据

                }
                else
                {
                    glzwhere = " AND  ZGLZNAME='" + glzname + "'";
                    //获取总站数据
                }
            }
            if (TYPE == "1")
            {
                //总水量
                dtReturn = new JH_Auth_UserB().GetDTByCommand("SELECT ROUND( ISNULL(SUM(ZSL) , 0)/10000,3)   AS ZS FROM vwSBData WHERE 1=1 " + glzwhere + " AND  CRDate BETWEEN '" + sdate + " 00:00:00' AND '" + edate + " 23:59:59'");
            }
            if (TYPE == "2")
            {
                //灌溉亩数
                dtReturn = new JH_Auth_UserB().GetDTByCommand("SELECT  ROUND( ISNULL(SUM(TDMS) , 0)/10000,3)   AS ZS FROM vwSBData WHERE  1=1 " + glzwhere + " AND  CRDate BETWEEN '" + sdate + " 00:00:00' AND '" + edate + " 23:59:59'");
            }
            if (TYPE == "3")
            {
                //总金额
                dtReturn = new JH_Auth_UserB().GetDTByCommand("SELECT ROUND( ISNULL(SUM(ZJE) , 0)/10000,3)   AS ZS FROM vwSBData WHERE  1=1  " + glzwhere + " AND  CRDate BETWEEN '" + sdate + " 00:00:00' AND '" + edate + " 23:59:59'");
            }
            if (TYPE == "4")
            {
                //干口水量
                glzwhere = " AND  zzname like '%" + glzname + "%'";
                dtReturn = new JH_Auth_UserB().GetDTByCommand(" SELECT  ROUND( ISNULL(SUM(ysl) , 0)/10000,3)  as gkll  FROM qj_zzsb  WHERE  zt='已确认' AND 1=1 " + glzwhere + " AND  CRDate BETWEEN '" + sdate + " 00:00:00' AND '" + edate + " 23:59:59'");
            }
            if (TYPE == "5")
            {
                //已支付金额
                dtReturn = new JH_Auth_UserB().GetDTByCommand("SELECT ROUND( ISNULL(SUM(hjje) , 0),2)   AS ZS FROM vwSBData WHERE  1=1  AND zfzt='已支付'  " + glzwhere + " AND  CRDate BETWEEN '" + sdate + " 00:00:00' AND '" + edate + " 23:59:59'");
            }
            return Decimal.Parse(dtReturn.Rows[0][0].ToString()).ToString("0.00");
        }

        /// <summary>
        /// 获取行政区小组数据
        /// </summary>
        /// <param name="strZID"></param>
        /// <returns></returns>
        public DataTable getXZTree(string strZID)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT DISTINCT 'P'+convert(nvarchar(50),pid) as id, Pname as label,'0' as pid FROM qj_xzq WHERE ZID='" + strZID + "' AND TYPE='村' UNION ALL  SELECT   'P'+convert(nvarchar(50),ID) as id, czname as label, 'P'+convert(nvarchar(50),pid) as pid FROM qj_xzq WHERE ZID='" + strZID + "' AND TYPE='村' UNION ALL  SELECT convert(nvarchar(50),ID) AS id ,xzname as label,  'P'+convert(nvarchar(50),pid) as pid  FROM qj_cz  WHERE ZID='" + strZID + "'");
            return dt;
        }


        #endregion

        #region PC端

        /// <summary>
        /// 获取行政区树
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETXQZTREE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            P1 = P1 ?? "4142";
            String strDBID = P2;

            DataTable dtcz = new JH_Auth_QYB().GetDTByCommand("SELECT xzid FROM qj_db_xz  WHERE   did IN ('" + strDBID + "')");
            DataTable dtyh = getXZTree(P1);
            string strXZID = dtcz.GetDTColum("xzid");
            msg.Result = dtyh;
            msg.Result1 = strXZID;

        }
        public void GETUSERINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtyh = new DataTable();
            dtyh = new SFJSManage().GetUserInfo("", UserInfo);
            string strUserType = dtyh.Rows[0]["usertype"].ToString();
            string strxzids = dtyh.Rows[0]["czids"].ToString();
            string strzid = dtyh.Rows[0]["zid"].ToString();

            msg.Result = dtyh;
        }

        /// <summary>
        /// 微信管理端-初始化
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void INITDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtyh = new DataTable();
            dtyh = new SFJSManage().GetUserInfo("", UserInfo);

            string strUserType = dtyh.Rows[0]["usertype"].ToString();
            string strxzids = dtyh.Rows[0]["czids"].ToString();
            string strzid = dtyh.Rows[0]["zid"].ToString();

            msg.Result = dtyh;


            DataTable dt = new JH_Auth_QYB().GetDTByCommand("SELECT id,title,fbdate,imgurl,lls FROM qj_sys_tzgg WHERE ggtype='新闻资讯'  ORDER BY ID DESC");
            DataTable dt1 = new JH_Auth_QYB().GetDTByCommand("SELECT   id,title,fbdate,imgurl,lls FROM qj_sys_tzgg WHERE  ggtype='新闻资讯' AND islb='是' ORDER BY ID DESC");
            DataTable dt3 = new JH_Auth_QYB().GetDTByCommand("SELECT id,title,fbdate,imgurl,lls FROM qj_sys_tzgg WHERE  ggtype='系统公告' AND islb='是' ORDER BY ID DESC");
            msg.Result2 = dt1;
            msg.Result3 = dt;

            DataTable dtcz = new JH_Auth_QYB().GetDTByCommand("SELECT id,xzname,pname,did,dbname,(pname+'/'+xzname) AS xz FROM qj_cz  WHERE   id IN ('" + strxzids.ToFormatLike() + "')");
            dtcz.Columns.Add("dbdata", Type.GetType("System.Object"));
            for (int i = 0; i < dtcz.Rows.Count; i++)
            {
                string czid = dtcz.Rows[i]["id"].ToString();
                dtcz.Rows[i]["dbdata"] = new JH_Auth_QYB().GetDTByCommand("SELECT did,dbname FROM qj_db_xz  WHERE   xzid IN ('" + czid + "')");
            }
            msg.Result4 = dtcz;

            msg.Result6 = dt3;

            JH_Auth_Branch Branch = new JH_Auth_BranchB().GetEntities(d => d.DeptCode == 1).FirstOrDefault();
            if (Branch != null)
            {
                msg.Result5 = Branch.IsHasQX;
            }

            DataTable dtGGND = GETNDDATA();
            msg.Result1 = dtGGND;
        }

        public void TBGLZDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            new JH_Auth_BranchB().ExsSclarSql("UPDATE qj_sfjs_de SET DCode=zid,DName=zname; UPDATE qj_sfjs_jg SET DCode = zid, DName = zname;UPDATE qj_cz SET DCode = zid, DName = zname;");

        }



        /// <summary>
        /// 处理交口数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void TBGLZDATAV1(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strName = P1;
            string strZID = P2;


            #region 同步新增行政区，斗别，村数据
            //string strSQL = "SELECT DISTINCT  GLZNAME AS '管理站',PNAME AS '县市','' AS '乡镇',CNAME AS '村',  XZNAME AS '组',YHNAME AS '姓名',SFZBM AS '身份证号'," +
            //    "LXDH AS '手机号码',JH_DB.ZQ AS '干支渠',JH_DB.ZQ AS '段别', JH_DB.NAME AS '斗渠' FROM SFJS.dbo.JH_JFUser LEFT JOIN SFJS.dbo.JH_DB ON JH_JFUser.DID = JH_DB.ID LEFT JOIN SFJS.dbo.JH_XX ON JH_JFUser.CID = JH_XX.ID WHERE GLZNAME='" + strName + "'";

            //string strXZC = "SELECT DISTINCT  GLZNAME AS '管理站',VW.PNAME AS '县市','' AS '乡镇',XZNAME AS '村',  VW.CZNAME AS '组','' AS 姓名,'' AS 身份证号, '' AS 手机号码, JH_DB.ZQ AS '干支渠',JH_DB.ZQ AS '段别', JH_DB.NAME AS '斗渠','' as '粮油作物面积','' as '经济作物面积','' as '作物总面积'  FROM SFJS.dbo.vwSBData VW LEFT JOIN SFJS.dbo.JH_DB ON VW.DID = JH_DB.ID LEFT JOIN SFJS.dbo.JH_XX ON VW.CID = JH_XX.ID WHERE VW.CZNAME  NOT LIKE '%、%' AND VW.GLZNAME ='" + strName + "' ORDER BY vw.GLZNAME";

            //DataTable dt = new JH_Auth_BranchB().GetDTByCommand(strXZC);
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    string strTEMP = dt.Rows[i]["县市"].ToString();
            //    dt.Rows[i]["乡镇"] = strTEMP.Split('-')[1];
            //    dt.Rows[i]["县市"] = strTEMP.Split('-')[0];


            //    dt.Rows[i]["姓名"] = i.ToString();
            //    dt.Rows[i]["身份证号"] = i.ToString();
            //    dt.Rows[i]["手机号码"] = i.ToString();

            //}
            //msg.Result = dt;
            #endregion





            #region 同步用户信息



            //string strYHSQL = "SELECT DISTINCT  GLZNAME AS '管理站',PNAME AS '县市','' AS '乡镇',CNAME AS '村',  XZNAME AS '组',YHNAME AS '姓名',SFZBM AS '身份证号' FROM SFJS.dbo.JH_JFUser LEFT JOIN SFJS.dbo.JH_DB ON JH_JFUser.DID = JH_DB.ID LEFT JOIN SFJS.dbo.JH_XX ON JH_JFUser.CID = JH_XX.ID WHERE GLZNAME = '" + strName + "'";
            //DataTable dtYH = new JH_Auth_BranchB().GetDTByCommand(strYHSQL);


            //for (int i = 0; i < dtYH.Rows.Count; i++)
            //{
            //    string strSFZH = dtYH.Rows[i]["身份证号"].ToString();
            //    string strXM = dtYH.Rows[i]["姓名"].ToString();
            //    try
            //    {
            //        if (isExitXMV1(strSFZH, strName).Rows.Count == 0 && strSFZH.Length == 18)
            //        {
            //            string strXZName = dtYH.Rows[i]["县市"].ToString().Split('-')[1];
            //            string strCName = dtYH.Rows[i]["村"].ToString();
            //            string strCZName = dtYH.Rows[i]["组"].ToString();
            //            DataTable dtXZ = getCzinfoV(strXZName + "/" + strCName, strCZName, strZID);
            //            string xzid = dtXZ.Rows[0]["ID"].ToString();
            //            string xzNAME = dtXZ.Rows[0]["xzname"].ToString();
            //            string PNAME = dtXZ.Rows[0]["pname"].ToString();
            //            new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_sfjs_jfuser ( CRUser, DCode, DName, CRUserName, CRDate, intProcessStanceid, ComID, YHName, zid, zname, sfzbm, lxdh, xzid, xzname, dlpassword,lsmj, jjmj, zmj, pname) VALUES ( 'administrator', '" + strZID + "', '" + strName + "', 'administrator', '2020-07-28 11:38:11', '0', '10334', '" + strXM + "', '" + strZID + "', '" + strName + "', '" + strSFZH + "', '', '" + xzid + "', '" + xzNAME + "', 'E99A18C428CB38D5F260853678922E03', '0', '0', '0', '" + PNAME + "');");
            //        }
            //    }
            //    catch (Exception ex)
            //    {

            //    }
            //}
            #endregion




            #region 同步灌溉信息

            #endregion
        }


        /// <summary>
        /// 设置管理站是否上报和计价类别
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GLZSET(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strZID = P1;
            string TYPE = P2;//0:计价类别，1：上报资格
            string upcontent = context.Request("upcontent") ?? "";
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM JH_Auth_Branch WHERE DeptCode in ('" + strZID.ToFormatLike() + "') ");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string intid = dt.Rows[i]["DeptCode"].ToString();
                if (TYPE == "0")
                {
                    new JH_Auth_BranchB().ExsSclarSql("UPDATE JH_Auth_Branch SET DeptDesc='" + upcontent + "' WHERE DeptCode='" + intid + "'");

                }
                else
                {
                    new JH_Auth_BranchB().ExsSclarSql("UPDATE JH_Auth_Branch SET IsHasQX='" + upcontent + "' WHERE DeptCode='" + intid + "'");
                }
            }


        }


        //获取短信内容
        public void DRJCDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtalldata = JsonConvert.DeserializeObject<DataTable>(P1);
            DataView dataView = dtalldata.DefaultView;

            string strZID = P2;
            string strZName = getZinfo(P2);
            #region 处理县市
            DataTable dtxq = dataView.ToTable(true, "县市");
            for (int i = 0; i < dtxq.Rows.Count; i++)
            {
                string strXSName = dtxq.Rows[i][0].ToString();
                DataTable dtTempxs = isExitXS(strXSName, strZID);
                if (dtTempxs.Rows.Count == 0)
                {
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_xzq (CRUser, CRDate, intProcessStanceid, ComID, type, czname, pid, zid, pname, DCode, CRUserName, zname, level, DName) VALUES ('administrator', '2020-07-24 17:03:40', '0', '10334', '县市', '" + strXSName + "', '0', '" + strZID + "', '', '" + strZID + "', 'administrator', '" + strZName + "', '0', '" + strZName + "');");
                }

            }
            #endregion
            #region 处理乡镇
            DataTable dtxz = dataView.ToTable(true, "县市", "乡镇");
            for (int i = 0; i < dtxz.Rows.Count; i++)
            {
                string strXSName = dtxz.Rows[i][0].ToString();
                string strXZName = dtxz.Rows[i][1].ToString();
                DataTable dtTempxs = isExitXS(strXSName, strZID);
                DataTable dtTempxz = isExitXZ(strXSName, strXZName, strZID);
                if (dtTempxz.Rows.Count == 0)
                {
                    string pid = dtTempxs.Rows[0]["ID"].ToString();
                    string pname = dtTempxs.Rows[0]["czname"].ToString();
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_xzq (CRUser, CRDate, intProcessStanceid, ComID, type, czname, pid, zid, pname, DCode, CRUserName, zname, level, DName) VALUES ('administrator', '2020-07-24 17:03:40', '0', '10334', '乡镇', '" + strXZName + "', '" + pid + "', '" + strZID + "', '" + pname + "', '" + strZID + "', 'administrator', '" + strZName + "', '1', '" + strZName + "');");
                }
            }
            #endregion
            #region 处理村
            DataTable dtc = dataView.ToTable(true, "县市", "乡镇", "村");
            for (int i = 0; i < dtc.Rows.Count; i++)
            {
                string strXSName = dtc.Rows[i][0].ToString();
                string strXZName = dtc.Rows[i][1].ToString();
                string strCName = dtc.Rows[i][2].ToString();
                DataTable dtTempxz = isExitXZ(strXSName, strXZName, strZID);
                DataTable dtTempC = isExitC(strXSName, strXZName, strCName, strZID);
                if (dtTempC.Rows.Count == 0)
                {
                    string pid = dtTempxz.Rows[0]["ID"].ToString();
                    string pname = dtTempxz.Rows[0]["pname"].ToString() + "/" + dtTempxz.Rows[0]["czname"].ToString();
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_xzq (CRUser, CRDate, intProcessStanceid, ComID, type, czname, pid, zid, pname, DCode, CRUserName, zname, level, DName) VALUES ('administrator', '2020-07-24 17:03:40', '0', '10334', '村', '" + strCName + "', '" + pid + "', '" + strZID + "', '" + pname + "', '" + strZID + "', 'administrator', '" + strZName + "', '2', '" + strZName + "');");
                }
            }
            #endregion

            #region 处理斗渠
            DataTable dtdq = dataView.ToTable(true, "干支渠", "段别", "斗渠");
            for (int i = 0; i < dtdq.Rows.Count; i++)
            {
                string strGQName = dtdq.Rows[i][0].ToString();
                string strDName = dtdq.Rows[i][1].ToString();
                string strDQName = dtdq.Rows[i][2].ToString();

                foreach (var item in strDQName.Split(','))
                {
                    if (isExitDB(item, strGQName, strDName, strZID))
                    {
                        new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_db (CRUser, CRDate, intProcessStanceid, ComID, zid, dbname, gzqname, dqname, remark, DCode, DName, CRUserName, zname, did) VALUES ( 'administrator', '2020-07-24 16:55:09', '0', '10334', '" + strZID + "', '" + item + "', '" + strGQName + "', '" + strDName + "', NULL, '" + strZID + "', '" + strZName + "', 'administrator', '" + strZName + "', NULL);");
                    }
                }


            }
            #endregion







            //DataView dataView = dt.DefaultView;
            //DataTable dtxq = dataView.ToTable(true, "乡镇");
            //DataTable dtc = dataView.ToTable(true, "村");//注：其中ToTable（）的第一个参数为是否DISTINCT
        }


        public void JCDRDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtalldata = JsonConvert.DeserializeObject<DataTable>(P1);
            List<string> Listcname = new List<string>() { "县市", "乡镇", "村", "组", "干支渠", "斗渠", "段别", "姓名", "身份证号", "手机号码", "粮油作物面积", "经济作物面积" };
            string strMsg = "";
            foreach (string cname in Listcname)
            {
                if (!dtalldata.Columns.Contains(cname))
                {
                    strMsg = strMsg + cname + "列缺少,";
                }

            }
            msg.ErrorMsg = strMsg;


        }
        public void XZDRDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtalldata = JsonConvert.DeserializeObject<DataTable>(P1);
            for (int i = 0; i < dtalldata.Rows.Count; i++)
            {
                if (i > 0)
                {
                    string strGLZName = dtalldata.Rows[i][0].ToString();
                    string strTempName = dtalldata.Rows[i][1].ToString();
                    string[] temp = dtalldata.Rows[i][1].ToString().Split('村');
                    string strCName = temp[0];
                    if (temp.Length > 1)
                    {
                        string strZName = temp[1];
                        string strUserName = dtalldata.Rows[i][2].ToString();
                        string strUserRealName = new JH_Auth_UserB().GetUserRealName(0, strUserName);
                        if (strUserRealName != "")
                        {
                            new JH_Auth_BranchB().ExsSclarSql(" UPDATE qj_cz SET czgly='" + strUserName + "',czglyname='" + strUserRealName + "' WHERE zname='" + strGLZName + "' and pname LIKE '%" + strCName + "%' AND  xzname='" + strZName + "'");

                        }

                    }
                }



            }


        }

        public void DRUSERDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtalldata = JsonConvert.DeserializeObject<DataTable>(P1);
            DataView dataView = dtalldata.DefaultView;
            string strZID = P2;
            string strZName = getZinfo(P2);
            string strMsg = "出错信息:";
            #region 处理小组
            DataTable dtcz = dataView.ToTable(true, "乡镇", "村", "组", "干支渠", "斗渠", "段别");
            for (int i = 0; i < dtcz.Rows.Count; i++)
            {
                try
                {


                    string strXZName = dtcz.Rows[i][0].ToString();
                    string strCName = dtcz.Rows[i][1].ToString();
                    string strCZName = dtcz.Rows[i][2].ToString();
                    string strGQName = dtcz.Rows[i][3].ToString();
                    string strDName = dtcz.Rows[i][4].ToString();
                    string strDQName = dtcz.Rows[i][5].ToString();

                    if (isExitCZV1(strCZName, strXZName + "/" + strCName, strZID).Rows.Count == 0)
                    {
                        DataTable dtD = getDIDByName(strGQName, strDName, strDQName, strZID);
                        string strDBIDS = "";
                        string strDBNAMES = "";

                        for (int m = 0; m < dtD.Rows.Count; m++)
                        {
                            strDBIDS = strDBIDS + dtD.Rows[m]["id"].ToString() + ",";
                            strDBNAMES = strDBNAMES + strGQName + "/" + strDQName + "/" + dtD.Rows[m]["dbname"].ToString() + ",";

                        }
                        strDBIDS = strDBIDS.Trim(',');
                        strDBNAMES = strDBNAMES.Trim(',');

                        DataTable dttempxz = getXZinfo(strXZName, strCName, strZID);
                        string xzname = dttempxz.Rows[0]["pname"].ToString() + "/" + dttempxz.Rows[0]["czname"].ToString();
                        string xzid = dttempxz.Rows[0]["ID"].ToString();
                        new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_cz ( CRUser, DCode, DName, CRUserName, CRDate, intProcessStanceid, ComID, zid, zname, xzname, pid, pname, did, dbname) VALUES ('administrator', '" + strZID + "', '" + strZName + "', 'administrator', '2020-07-25 22:36:06', '0', '10334', '" + strZID + "', '" + strZName + "', '" + strCZName + "', '" + xzid + "', '" + xzname + "', '" + strDBIDS + "', '" + strDBNAMES + "');");

                        string strxzid = new JH_Auth_BranchB().GetDTByCommand("SELECT max(id) as id from qj_cz").Rows[0][0].ToString();
                        string strInsertSQL = "";
                        for (int m = 0; m < dtD.Rows.Count; m++)
                        {
                            string tempdname = strGQName + "/" + strDQName + "/" + dtD.Rows[m]["dbname"].ToString();
                            strInsertSQL = strInsertSQL + " INSERT INTO qj_db_xz (DCode,did, dbname,xzid,CRUser,CRDate) VALUES('" + strZID + "','" + dtD.Rows[m]["id"].ToString() + "', '" + tempdname + "', '" + strxzid + "', '" + UserInfo.User.UserName + "', getdate()); ";
                        }
                        new JH_Auth_BranchB().ExsSclarSql(strInsertSQL);

                    }
                }
                catch (Exception)
                {

                    strMsg = strMsg + "小组" + i.ToString();
                }
            }

            List<string> Listczs = new List<string>();//村组id，后面更新qj_cz表使用
            for (int i = 0; i < dtcz.Rows.Count; i++)
            {
                string strXZName = dtcz.Rows[i][0].ToString();
                string strCName = dtcz.Rows[i][1].ToString();
                string strCZName = dtcz.Rows[i][2].ToString();
                string strGQName = dtcz.Rows[i][3].ToString();
                string strDName = dtcz.Rows[i][4].ToString();
                string strDQName = dtcz.Rows[i][5].ToString();

                DataTable dtDB = getDinfo(strGQName, strDName, strDQName, strZID);
                DataTable dtXZ = getCzinfoV(strXZName + "/" + strCName, strCZName, strZID);
                Listczs.Add(dtXZ.Rows[0]["id"].ToString());
                if (isExitCZ(strCZName, strXZName + "/" + strCName, strGQName + "/" + strDQName + "/" + strDName, strZID).Rows.Count == 0)
                {
                    if (dtDB.Rows.Count > 0 && dtXZ.Rows.Count > 0)
                    {
                        string strInsertSQL = " INSERT INTO qj_db_xz (DCode, did, dbname,xzid,CRUser,CRDate) VALUES('" + strZID + "','" + dtDB.Rows[0]["id"].ToString() + "', '" + strGQName + "/" + strDQName + "/" + strDName + "', '" + dtXZ.Rows[0]["id"].ToString() + "', '" + UserInfo.User.UserName + "', getdate()); ";
                        new JH_Auth_BranchB().ExsSclarSql(strInsertSQL);
                    }
                }
            }


            foreach (string czid in Listczs)
            {
                DataTable dtdb = new JH_Auth_BranchB().GetDTByCommand("SELECT * from qj_db_xz where xzid='" + czid + "'");

                string strDBIDS = "";
                string strDBNAMES = "";

                for (int m = 0; m < dtdb.Rows.Count; m++)
                {
                    strDBIDS = strDBIDS + dtdb.Rows[m]["did"].ToString() + ",";
                    strDBNAMES = strDBNAMES + dtdb.Rows[m]["dbname"].ToString() + ",";

                }
                strDBIDS = strDBIDS.Trim(',');
                strDBNAMES = strDBNAMES.Trim(',');

                new JH_Auth_BranchB().ExsSclarSql("UPDATE qj_cz set did='" + strDBIDS + "',dbname='" + strDBNAMES + "' where id='" + czid + "'");



            }
            #endregion

            #region 处理缴费用户
            for (int i = 0; i < dtalldata.Rows.Count; i++)
            {
                try
                {
                    string strXSName = dtalldata.Rows[i]["县市"].ToString();
                    string strXZName = dtalldata.Rows[i]["乡镇"].ToString();
                    string strCName = dtalldata.Rows[i]["村"].ToString();
                    string strCZName = dtalldata.Rows[i]["组"].ToString();


                    string strXMe = dtalldata.Rows[i]["姓名"].ToString();
                    string strSFZH = dtalldata.Rows[i]["身份证号"].ToString();
                    string strSJHM = dtalldata.Rows[i]["手机号码"].ToString();

                    string strLYMJ = dtalldata.Rows[i]["粮油作物面积"].ToString();
                    string strJJMJ = dtalldata.Rows[i]["经济作物面积"].ToString();
                    string strZWMJ = "0";
                    if (isExitXM(strSFZH, strZID).Rows.Count == 0 && strSFZH.Length == 18)
                    {
                        DataTable dttempxz = getCzinfoV(strXSName + "/" + strXZName + "/" + strCName, strCZName, strZID);
                        string xzid = dttempxz.Rows[0]["ID"].ToString();
                        string xzNAME = dttempxz.Rows[0]["xzname"].ToString();
                        string PNAME = dttempxz.Rows[0]["pname"].ToString();
                        new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_sfjs_jfuser ( CRUser, DCode, DName, CRUserName, CRDate, intProcessStanceid, ComID, YHName, zid, zname, sfzbm, lxdh, xzid, xzname, dlpassword,lsmj, jjmj, zmj, pname) VALUES ( 'administrator', '" + strZID + "', '" + strZName + "', 'administrator', '2020-07-28 11:38:11', '0', '10334', '" + strXMe + "', '" + strZID + "', '" + strZName + "', '" + strSFZH + "', '" + strSJHM + "', '" + xzid + "', '" + xzNAME + "', 'E99A18C428CB38D5F260853678922E03', '" + strLYMJ + "', '" + strJJMJ + "', '" + strZWMJ + "', '" + PNAME + "');");
                    }
                }
                catch (Exception ex)
                {

                    strMsg = strMsg + "用户" + i + "信息：" + ex.Message.ToString();
                }
            }

            msg.Result1 = strMsg;
            #endregion
        }


        public void CLDRDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strZID = P1;

            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_cz ");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string intPID = dt.Rows[i]["pid"].ToString();
                string intid = dt.Rows[i]["ID"].ToString();
                DataTable dtxzq = new JH_Auth_BranchB().GetDTByCommand("SELECT  (PNAME +'/' +CZNAME) AS XZNAME  FROM qj_xzq WHERE  id ='" + intPID + "'");
                new JH_Auth_BranchB().ExsSclarSql("UPDATE qj_cz SET pname='" + dtxzq.Rows[0]["XZNAME"].ToString() + "' WHERE ID='" + intid + "'");
            }


        }

        public void DELGLZDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strZID = P1;

            DelZData(strZID);


        }

        /// <summary>
        /// 添加斗长
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void TJDZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strPID = P1;
            Yan_WF_PI PI = new Yan_WF_PIB().GetEntity(D => D.ID.ToString() == P1);
            string glz = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjCascader33498", "1");
            string glzid = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjCascader33498");
            string username = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput14468");
            string xm = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput25846");
            string phone = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput78117");




            if (new JH_Auth_UserB().GetEntities(d => d.UserName == username).Count() > 0)
            {
                msg.ErrorMsg = "存在相同用户名,请更换用户名!";
                new Yan_WF_PIB().Delete(d => d.ID == PI.ID);
                new Yan_WF_TIB().Delete(d => d.PIID == PI.ID);
                return;
            }
            JH_Auth_User USER = new JH_Auth_User();
            USER.UserName = username;
            USER.UserRealName = xm;
            USER.UserPass = CommonHelp.GetMD5("abc123");
            USER.BranchCode = int.Parse(glzid);
            USER.CRUser = UserInfo.User.UserName;
            USER.IsUse = "Y";
            USER.ComId = 10334;
            USER.mobphone = phone;
            USER.CRDate = DateTime.Now;
            new JH_Auth_UserB().Insert(USER);
            new JH_Auth_UserRoleB().Insert(new JH_Auth_UserRole() { ComId = UserInfo.User.ComId, UserName = username, RoleCode = 1271 });
        }


        public void TJDZV1(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strPID = P1;
            Yan_WF_PI PI = new Yan_WF_PIB().GetEntity(D => D.ID.ToString() == P1);
            string glz = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjCascader62557", "1");
            string glzid = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjCascader62557");
            string username = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput42808");
            string xm = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput17448");
            string phone = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput83845");
            string rolecode = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput58151");

            int rol = 0;
            int.TryParse(rolecode, out rol);
            if (new JH_Auth_UserB().GetEntities(d => d.UserName == username).Count() > 0)
            {
                msg.ErrorMsg = "存在相同用户名,请更换用户名!";
                new Yan_WF_PIB().Delete(d => d.ID == PI.ID);
                new Yan_WF_TIB().Delete(d => d.PIID == PI.ID);
                return;
            }
            JH_Auth_User USER = new JH_Auth_User();
            USER.UserName = username;
            USER.UserRealName = xm;
            USER.UserPass = CommonHelp.GetMD5("abc123");
            USER.BranchCode = int.Parse(glzid);
            USER.CRUser = UserInfo.User.UserName;
            USER.IsUse = "Y";
            USER.ComId = 10334;
            USER.mobphone = phone;
            USER.CRDate = DateTime.Now;
            new JH_Auth_UserB().Insert(USER);
            new JH_Auth_UserRoleB().Insert(new JH_Auth_UserRole() { ComId = UserInfo.User.ComId, UserName = username, RoleCode = rol });
        }



        /// <summary>
        /// 删除斗长
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELDZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string username = P1;
            new JH_Auth_UserB().Delete(D => D.UserName == username);
            new JH_Auth_UserRoleB().Delete(D => D.UserName == username);
        }



        public void GETGLZINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {


            DataTable dtalldata = new DataTable();
            dtalldata = new JH_Auth_BranchB().GetDTByCommand("SELECT deptcode as id,DEPTNAME AS label,deptroot as pid,remark2 as isfz  FROM jh_auth_branch WHERE Remark2='Y' AND DEPTCODE IN ('" + UserInfo.UserBMQXCode.ToFormatLike() + "') ");
            msg.Result = dtalldata;

        }



        /// <summary>
        /// 批量设置村组管理员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETCZGLY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strxzids = P1;
            string struser = P2;
            string strUserName = new JH_Auth_UserB().GetUserRealName(UserInfo.User.ComId.Value, struser);
            new JH_Auth_UserB().ExsSclarSql(" UPDATE qj_cz SET czgly='" + struser + "',czglyname='" + strUserName + "' where id in ('" + strxzids.ToFormatLike(',') + "')");
            foreach (string user in struser.Split(','))
            {
                new JH_Auth_UserRoleB().Delete(D => D.UserName == user && D.RoleCode == 1272);
                new JH_Auth_UserRoleB().Insert(new JH_Auth_UserRole() { ComId = 10334, RoleCode = 1272, UserName = user });
            }

        }

        /// <summary>
        /// 批量设置斗长
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETDBGLY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strxzids = P1;
            string struser = P2;
            string strUserName = new JH_Auth_UserB().GetUserRealName(UserInfo.User.ComId.Value, struser);
            new JH_Auth_UserB().ExsSclarSql(" UPDATE qj_db SET dgly='" + struser + "',remark='" + strUserName + "' where id in ('" + strxzids.ToFormatLike(',') + "')");
            foreach (string user in struser.Split(','))
            {
                new JH_Auth_UserRoleB().Delete(D => D.UserName == user && D.RoleCode == 1271);
                new JH_Auth_UserRoleB().Insert(new JH_Auth_UserRole() { ComId = 10334, RoleCode = 1271, UserName = user });
            }

        }


        /// <summary>
        /// 设置村组与斗别关系
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETCZDB(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string piid = P1;
            DataTable dtDRSQ = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM qj_cz where intprocessStanceid='" + piid + "'");
            if (dtDRSQ.Rows.Count > 0)
            {
                string xzid = dtDRSQ.Rows[0]["ID"].ToString();
                string Did = dtDRSQ.Rows[0]["did"].ToString();
                string Dname = dtDRSQ.Rows[0]["dbname"].ToString();
                string zid = dtDRSQ.Rows[0]["zid"].ToString();

                string strInsertSQL = "";
                int i = 0;

                foreach (var item in Did.SplitTOList(','))
                {
                    strInsertSQL = strInsertSQL + " INSERT INTO qj_db_xz (DCode,did, dbname,xzid,CRUser,CRDate) VALUES('" + zid + "','" + item + "', '" + Dname.SplitTOList(',')[i].ToString() + "', '" + xzid + "', '" + UserInfo.User.UserName + "', getdate()); ";
                    i++;
                }
                new JH_Auth_UserB().ExsSclarSql("delete qj_db_xz where xzid='" + xzid + "'");
                new JH_Auth_UserB().ExsSclarSql(strInsertSQL);
            }

        }

        /// <summary>
        /// 设置斗别村组对应关系
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETDBCZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string piid = P1;
            string czids = P2;
            DataTable dtDRSQ = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM qj_db where id='" + piid + "'");
            if (dtDRSQ.Rows.Count > 0)
            {
                string dbid = dtDRSQ.Rows[0]["ID"].ToString();
                string Dname = dtDRSQ.Rows[0]["dbname"].ToString();
                string zid = dtDRSQ.Rows[0]["zid"].ToString();

                string strInsertSQL = "";
                int i = 0;

                foreach (var item in czids.SplitTOList(','))
                {
                    strInsertSQL = strInsertSQL + " INSERT INTO qj_db_xz (DCode,did, dbname,xzid,CRUser,CRDate) VALUES('" + zid + "','" + dbid + "', '" + Dname + "', '" + item + "', '" + UserInfo.User.UserName + "', getdate()); ";
                    i++;
                }
                new JH_Auth_UserB().ExsSclarSql("delete qj_db_xz where did='" + dbid + "'");
                new JH_Auth_UserB().ExsSclarSql(strInsertSQL);
            }

        }

        /// <summary>
        /// 获取大屏数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DPDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            // 最新水情
            DataTable dtDRSQ = new JH_Auth_UserB().GetDTByCommand("SELECT sbtime,sbdata FROM qj_sqsb ORDER BY ID DESC");

            //全站
            DataTable dtQZTJQ = new JH_Auth_UserB().GetDTByCommand("SELECT SUM(sl1+sl2) AS ZSL,SUM(zje) AS ZJE,  SUM(cast(tdms as decimal(6,1))) AS TDMS FROM qj_sfjs_ysdata ");

            //各管理站
            DataTable dtGLZTJ = new JH_Auth_UserB().GetDTByCommand("SELECT ZNAME, SUM(sl1+sl2) AS ZSL,SUM(zje) AS ZJE,  SUM(cast(tdms as decimal(6,1))) AS TDMS FROM qj_sfjs_ysdata GROUP BY ZNAME");

            // 粮食经济作物对比
            DataTable dtLSJJTJ = new JH_Auth_UserB().GetDTByCommand("SELECT TYPE AS SLTYPE, SUM(sl1+sl2) AS ZSL FROM qj_sfjs_ysdata GROUP BY TYPE");



            string strxzids = P1;
            string struser = P2;
            string strUserName = new JH_Auth_UserB().GetUserRealName(UserInfo.User.ComId.Value, struser);
            // new JH_Auth_UserB().ExsSclarSql(" UPDATE qj_cz SET czgly='" + struser + "',czglyname='" + strUserName + "' where id in ('" + strxzids.ToFormatLike(',') + "')");

        }



        /// <summary>
        /// 灌溉数据审核
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETGGJL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strids = P1;
            DataTable dt = new JH_Auth_UserB().GetDTByCommand(" select * FROM qj_sfjs_ysdata  where id in ('" + strids.ToFormatLike(',') + "')");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string strZT = dt.Rows[i]["jlstatus"].ToString() == "已审核" ? "待审核" : "已审核";
                string ID = dt.Rows[i]["id"].ToString();
                string sfzhm = dt.Rows[i]["sfzhm"].ToString();

                new JH_Auth_UserB().ExsSclarSql(" UPDATE qj_sfjs_ysdata SET jlstatus='" + strZT + "' where id  ='" + ID + "'");

                //DataTable dtUser=new 
                DataTable userListjfyh = new JH_Auth_UserB().GetDTByCommand("SELECT  * FROM qj_sfjs_jfuser WHERE sfzbm='" + sfzhm + "' ");
                if (strZT == "已审核" && userListjfyh.Rows.Count > 0 && !string.IsNullOrEmpty(userListjfyh.Rows[0]["wxopenid"].ToString()))
                {
                    string strOpenid = userListjfyh.Rows[0]["wxopenid"].ToString();
                    string strTitle = "尊敬的" + dt.Rows[i]["xm"].ToString() + "用户，您本期用水情况如下：";
                    string strRemark = "温馨提示：请您尽快交清本次水费。如有异议，请联系用水地村组管水员或管理站。";
                    string strYHDZ = dt.Rows[i]["cname"].ToString() + "/" + dt.Rows[i]["czname"].ToString();
                    string strYSL = Math.Round((decimal.Parse(dt.Rows[i]["sl1"].ToString()) + decimal.Parse(dt.Rows[i]["sl2"].ToString()))).ToString() + "方";
                    string strSFJE = Math.Round(decimal.Parse(dt.Rows[i]["hjje"].ToString()), 2, MidpointRounding.AwayFromZero) + "元";
                    string strYHH = userListjfyh.Rows[0]["ID"].ToString();
                    new WXGZHHelp().SENDWXMSG(strOpenid, UserInfo.QYinfo.WebSite + "/MOBWeb/" + SFJSHelp.GetMobPath(UserInfo.QYinfo.QYName) + "/indexgzh.html?dataid=" + ID, strTitle, strRemark, strYHDZ, strYSL, strSFJE, strYHH);
                }


            }


        }


        public void DELGGJL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strids = P1;
            new JH_Auth_UserB().ExsSclarSql(" delete qj_sfjs_ysdata  where id in ('" + strids.ToFormatLike(',') + "')");


        }

        public void SETSFJS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dt = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE id='" + P1 + "' AND jlstatus='已审核' ORDER BY CRDate DESC");
            msg.Result = dt;

        }



        /// <summary>
        /// 灌溉数据支付
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETGGJLZF(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strids = P1;
            new JH_Auth_UserB().ExsSclarSql(" UPDATE qj_sfjs_ysdata SET zfzt='已支付',zffs='手动支付',zfczr='" + UserInfo.User.UserName + "',zfsj=GETDATE() where id in ('" + strids.ToFormatLike(',') + "')");


        }

        #endregion

        #region andorid端,需要传UP参数


        /// <summary>
        /// 获取新闻详情
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETAPPSYDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dt = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM qj_sys_tzgg WHERE id='" + P1 + "'");
            JH_Auth_QY QY = new JH_Auth_QYB().GetALLEntities().FirstOrDefault();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string strXW = dt.Rows[i]["xwcontent"].ToString();
                dt.Rows[i]["xwcontent"] = strXW.Replace("/upload", QY.WebSite + "/upload");
            }
            msg.Result = dt;
        }

        /// <summary>
        /// 获取新闻列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETXWLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtxw = new JH_Auth_QYB().GetDTByCommand("SELECT id,title,fbdate,imgurl,lls FROM qj_sys_tzgg  ORDER BY ID DESC");
            msg.Result = dtxw;



            #region 生成测试数据
            //DBFactory db = new BI_DB_SourceB().GetDB(0);
            //DataTable dtSB = new JH_Auth_QYB().GetDTByCommand(" SELECT * FROM jh_sbdata");
            //DataTable dtYH = new JH_Auth_QYB().GetDTByCommand("SELECT qj_sfjs_jfuser.*,qj_cz.did,qj_cz.dbname,qj_cz.pname,qj_cz.pid FROM qj_sfjs_jfuser INNER  JOIN qj_cz  ON qj_sfjs_jfuser.xzid=qj_cz.ID WHERE qj_sfjs_jfuser.ZID='4113' AND xzid IN ('124','125','126','127')");

            //for (int i = 0; i < dtSB.Rows.Count; i++)
            //{

            //    try
            //    {
            //        Random ran = new Random();
            //        int RandKey = ran.Next(1, 190);

            //        var dt = new Dictionary<string, object>();
            //        dt.Add("DE", dtSB.Rows[i]["DE"].ToString());
            //        dt.Add("YSSC", dtSB.Rows[i]["YSSC"].ToString());
            //        dt.Add("CZNAME", dtYH.Rows[RandKey]["xzname"].ToString());

            //        dt.Add("TDMS", dtSB.Rows[i]["TDMS"].ToString());
            //        dt.Add("SLL", dtSB.Rows[i]["SLL"].ToString());
            //        dt.Add("ZJE", dtSB.Rows[i]["ZJE"].ToString());
            //        dt.Add("ZID", dtYH.Rows[RandKey]["zid"].ToString());
            //        dt.Add("XZID", dtYH.Rows[RandKey]["xzid"].ToString());
            //        dt.Add("XM", dtYH.Rows[RandKey]["YHName"].ToString());
            //        dt.Add("SL2", dtSB.Rows[i]["SL2"].ToString());
            //        dt.Add("SL1", dtSB.Rows[i]["SL1"].ToString());
            //        dt.Add("CRUser", "20200801001");
            //        dt.Add("CRDate", dtSB.Rows[i]["CRDate"].ToString());
            //        dt.Add("TYPE", dtSB.Rows[i]["TYPE"].ToString());

            //        dt.Add("DB", dtYH.Rows[RandKey]["dbname"].ToString());
            //        dt.Add("DW2", dtSB.Rows[i]["DW2"].ToString());
            //        dt.Add("DID", dtYH.Rows[RandKey]["did"].ToString());
            //        dt.Add("DW1", dtSB.Rows[i]["DW1"].ToString());
            //        dt.Add("CID", dtYH.Rows[RandKey]["pid"].ToString());
            //        dt.Add("cname", dtYH.Rows[RandKey]["pname"].ToString());


            //        dt.Add("sfzhm", dtYH.Rows[RandKey]["sfzbm"].ToString());
            //        dt.Add("zname", dtYH.Rows[RandKey]["zname"].ToString());



            //        dt.Add("intProcessStanceid", "0");
            //        dt.Add("ComID", "10334");
            //        dt.Add("GYSJ", dtSB.Rows[i]["GYSJ"].ToString());
            //        dt.Add("GYZJE", dtSB.Rows[i]["GYZJE"].ToString());
            //        db.InserData(dt, "qj_sfjs_ysdata");
            //    }
            //    catch (Exception)
            //    {
            //        continue;
            //    }



            //}
            #endregion
        }




        /// <summary>
        /// 修改密码接口
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>   
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void XGMM(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP);
            if (dtUser.Rows.Count > 0)
            {
                new JH_Auth_QYB().ExsSclarSql(" UPDATE jh_auth_user set userpass='" + CommonHelp.GetMD5(P1) + "' WHERE username ='" + dtUser.Rows[0]["username"].ToString() + "'");
            }

        }


        /// <summary>
        /// 获取管理站信息接口(包含价格,定额数据)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETZINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP, UserInfo);
            DataTable dtdeinfo = new DataTable();
            DataTable dtjg = new DataTable();
            DataTable dtjq = new DataTable();

            string strZid = P1;
            if (dtUser.Rows.Count > 0)
            {
                dtdeinfo = new JH_Auth_QYB().GetDTByCommand("SELECT  dymc as type,dw,zid,type as zwlb  FROM qj_sfjs_de where zid='" + strZid + "' ");
                dtjg = new JH_Auth_QYB().GetDTByCommand("SELECT TYPE,DW,JG,GYJG,JQMC FROM qj_sfjs_jg  where zid ='" + strZid + "'");
                dtjq = new JH_Auth_QYB().GetDTByCommand("SELECT DISTINCT JQMC  FROM qj_sfjs_jg  where zid ='" + strZid + "'  ORDER BY jqmc DESC");

                dtjq.Columns.Add("JGS", Type.GetType("System.Object"));
                for (int i = 0; i < dtjq.Rows.Count; i++)
                {
                    string strJQMC = dtjq.Rows[i]["JQMC"].ToString();
                    dtjq.Rows[i]["JGS"] = dtjg.Where(" JQMC = '" + strJQMC + "'");
                }
            }
            msg.Result = dtdeinfo;
            msg.Result1 = dtjg;
            msg.Result2 = dtjq;
            msg.Result3 = getZinfo(strZid);
            msg.Result4 = UserInfo.QYinfo.EncodingAESKey;


        }



        /// <summary>
        /// 根据小组,获取对应村组得灌溉用户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETGGYH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            string index = context.Request("INDEX") ?? "1";
            string isqj = context.Request("isqj") ?? "0";

            DataTable dtUser = GetUserInfo(UP, UserInfo);

            int datacount = 0;
            DataTable dtyhinfo = new DataTable();
            string strxzid = dtUser.Rows[0]["czids"].ToString();
            string strWhere = " AND  qj_sfjs_jfuser.zid IN ('" + UserInfo.UserBMQXCode.ToFormatLike() + "')";
            if (!string.IsNullOrEmpty(strxzid) || dtUser.Rows[0]["usertype"].ToString().Contains("斗长") || dtUser.Rows[0]["usertype"].ToString().Contains("协会人员"))
            {
                strWhere = "AND XZID  IN ('" + strxzid.ToFormatLike() + "') ";
            }
            if (isqj == "1")
            {
                //如果搜全局，去除搜索条件
                strWhere = "";
            }
            if (!string.IsNullOrEmpty(P2))
            {
                strWhere = strWhere + "AND ( YHName LIKE '%" + P2 + "%' OR sfzbm   LIKE  '%" + P2 + "%' OR lxdh   LIKE  '%" + P2 + "%')";
            }
            if (dtUser.Rows.Count > 0)
            {
                if (index == "0")
                {
                    dtyhinfo = new JH_Auth_QYB().Db.SqlQueryable<Object>(" SELECT qj_sfjs_jfuser.xzid,YHName,qj_sfjs_jfuser.zid,qj_sfjs_jfuser.zname,sfzbm,lxdh,qj_sfjs_jfuser.xzname,qj_sfjs_jfuser.ID,qj_cz.dbname,qj_cz.pname FROM qj_sfjs_jfuser INNER JOIN qj_cz ON qj_sfjs_jfuser.xzid=qj_cz.ID  WHERE  1=1 " + strWhere).OrderBy(" ID DESC").ToDataTable();
                    datacount = dtyhinfo.Rows.Count;

                }
                else
                {
                    dtyhinfo = new JH_Auth_QYB().Db.SqlQueryable<Object>(" SELECT qj_sfjs_jfuser.xzid,YHName,qj_sfjs_jfuser.zid,qj_sfjs_jfuser.zname,sfzbm,lxdh,qj_sfjs_jfuser.xzname,qj_sfjs_jfuser.ID,qj_cz.dbname,qj_cz.pname FROM qj_sfjs_jfuser INNER JOIN qj_cz ON qj_sfjs_jfuser.xzid=qj_cz.ID  WHERE  1=1 " + strWhere).OrderBy(" ID DESC").ToDataTablePage(int.Parse(index), 20, ref datacount);

                }
            }

            for (int i = 0; i < dtyhinfo.Rows.Count; i++)
            {
                dtyhinfo.Rows[i]["xzname"] = dtyhinfo.Rows[i]["pname"].ToString() + "/" + dtyhinfo.Rows[i]["xzname"].ToString();
            }

            if (!string.IsNullOrEmpty(P2) && dtyhinfo.Rows.Count > 0)
            {
                //如果传的身份证，就只返回用户所在小组
                strxzid = dtyhinfo.Rows[0]["xzid"].ToString();
            }

            DataTable dtcz = new JH_Auth_QYB().GetDTByCommand("SELECT id,xzname,pname,did,dbname,(pname+'/'+xzname) AS xz FROM qj_cz  WHERE   id IN ('" + strxzid.ToFormatLike() + "')");
            dtcz.Columns.Add("dbdata", Type.GetType("System.Object"));
            for (int i = 0; i < dtcz.Rows.Count; i++)
            {
                string czid = dtcz.Rows[i]["id"].ToString();
                dtcz.Rows[i]["dbdata"] = new JH_Auth_QYB().GetDTByCommand("SELECT did,dbname FROM qj_db_xz  WHERE   xzid IN ('" + czid + "')");
            }
            msg.Result = dtyhinfo;
            msg.Result1 = dtcz;
            msg.Result2 = datacount.ToString();


        }

        public void SBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DBFactory db = new BI_DB_SourceB().GetDB(0);
            CommonHelp.WriteLOG(context.ToString());

            DataTable dtUser = GetUserInfo(UP);

            string strXZID = context.Request("XZID");
            string strXMSFZ = context.Request("XM");
            string strZID = context.Request("ZID");

            if (string.IsNullOrEmpty(context.Request("XM")))
            {
                msg.ErrorMsg = "姓名不能为空";
                return;
            }


            if (!ExitGGYH(strXZID, strXMSFZ.Split('-')[1]))
            {
                msg.ErrorMsg = "该小组不存在该用户";
                return;
            }
            //if (Branch.Remark2 == "N")
            //{
            //    msg.ErrorMsg = "当前数据上报功能处于关闭状态!";
            //    return;
            //}




            var dt = new Dictionary<string, object>();
            dt.Add("XM", strXMSFZ.Split('-')[0]);
            dt.Add("sfzhm", strXMSFZ.Split('-')[1]);



            dt.Add("TDMS", context.Request("TDMS"));

            dt.Add("YSSC", context.Request("YSSC"));
            dt.Add("SLL", context.Request("SLL"));
            dt.Add("DE", context.Request("DE"));
            dt.Add("CRUser", context.Request("CRUser"));

            dt.Add("CRUserName", new JH_Auth_UserB().GetUserRealName(0, context.Request("CRUser")));
            dt.Add("DB", context.Request("DB"));
            dt.Add("ZID", context.Request("ZID"));
            dt.Add("ZName", dtUser.Rows[0]["zname"].ToString());
            dt.Add("DW2", context.Request("DW2"));
            dt.Add("DID", context.Request("DID"));
            dt.Add("DW1", context.Request("DW1"));
            dt.Add("SL2", context.Request("SL2"));
            dt.Add("SL1", context.Request("SL1"));

            dt.Add("jqname", context.Request("JQMC"));


            dt.Add("yssj", DateTime.Now.ToString());

            dt.Add("SL3", "0");
            dt.Add("SL4", "0");
            dt.Add("XZID", strXZID);
            dt.Add("CRDate", context.Request("YSSJ")); //妥协办法，将创建时间存用水时间,不然改动太大
            dt.Add("intProcessStanceid", "0");
            dt.Add("ComID", "10334");
            dt.Add("DCode", context.Request("ZID"));
            dt.Add("DName", dtUser.Rows[0]["zname"].ToString());

            string strGGZW = context.Request("TYPE");
            dt.Add("ggzw", strGGZW);
            DataTable dtXZQ = new JH_Auth_UserB().GetDTByCommand("SELECT pid,pname FROM  qj_cz WHERE ID= '" + strXZID + "'");
            dt.Add("cid", dtXZQ.Rows[0]["pid"].ToString());
            dt.Add("cname", dtXZQ.Rows[0]["pname"].ToString());
            dt.Add("jlstatus", "待审核");
            dt.Add("zfzt", "未支付");


            if (context.Request("CZNAME").Split('/').Length > 3)
            {
                dt.Add("CZNAME", context.Request("CZNAME").Split('/')[3]);

            }
            DataTable dtLYJG = new JH_Auth_UserB().GetDTByCommand("SELECT GYJG,jg.Type  FROM qj_sfjs_jg JG INNER  JOIN qj_sfjs_de DE ON jg.Type= DE.type and jg.zid =DE.zid WHERE jg.ZID = '" + strZID + "' AND DE.dymc = '" + strGGZW + "' ORDER BY JG.DW");
            if (dtLYJG.Rows.Count > 0)
            {
                dt.Add("TYPE", dtLYJG.Rows[0]["Type"].ToString());


                dt.Add("ZJE", Math.Round(decimal.Parse(context.Request("ZJE")), 2, MidpointRounding.AwayFromZero));
                dt.Add("GYSJ", dtLYJG.Rows[0]["GYJG"].ToString());

                decimal GYZJE = decimal.Parse(context.Request("SL1")) * decimal.Parse(dtLYJG.Rows[0]["GYJG"].ToString()) + decimal.Parse(context.Request("SL2")) * decimal.Parse(dtLYJG.Rows[0]["GYJG"].ToString());
                dt.Add("GYZJE", dtLYJG.Rows[0]["GYJG"].ToString());

            }
            db.InserData(dt, "qj_sfjs_ysdata");
            msg.Result = "上报成功";


        }


        public void SBDATAV1(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            JObject sbdata = (JObject)JsonConvert.DeserializeObject(P1);

            string strXZID = (string)sbdata["cz"];//小组ID
            DBFactory db = new BI_DB_SourceB().GetDB(0);
            CommonHelp.WriteLOG(context.ToString());

            DataTable dtUser = GetUserInfo(UP, UserInfo);

            string strXMSFZ = (string)sbdata["xm"];
            string strZID = (string)sbdata["zid"];
            string strZNAME = (string)sbdata["zname"];

            if (string.IsNullOrEmpty(strXMSFZ))
            {
                msg.ErrorMsg = "姓名不能为空";
                return;
            }
            //if (!ExitGGYH(strXZID, strXMSFZ.Split('-')[1]))
            //{
            //    msg.ErrorMsg = "该小组不存在该用户";
            //    return;
            //}
            if (UserInfo.QYinfo.FinanceLXR == "N")
            {
                msg.ErrorMsg = "当前数据上报功能处于关闭状态!";
                return;
            }




            var dt = new Dictionary<string, object>();
            dt.Add("XM", strXMSFZ.Split('-')[0]);
            dt.Add("sfzhm", strXMSFZ.Split('-')[1]);

            dt.Add("TDMS", (string)sbdata["ggmj"]);

            dt.Add("YSSC", (string)sbdata["yssc"]);
            dt.Add("SLL", (string)sbdata["dkll"]);
            dt.Add("DE", (string)sbdata["de"]);
            dt.Add("CRUser", dtUser.Rows[0]["username"].ToString());

            dt.Add("CRUserName", dtUser.Rows[0]["userrealname"].ToString());
            dt.Add("DB", (string)sbdata["dbname"]);
            dt.Add("ZID", strZID);
            dt.Add("ZName", strZNAME);
            dt.Add("DID", (string)sbdata["db"]);
            dt.Add("DW1", (string)sbdata["jg1"]);
            dt.Add("DW2", (string)sbdata["jg2"]);
            dt.Add("SL1", (string)sbdata["sl1"]);
            dt.Add("SL2", (string)sbdata["sl2"]);


            dt.Add("jqname", (string)sbdata["jq"]);


            dt.Add("yssj", DateTime.Now.ToString());

            dt.Add("SL3", "0");
            dt.Add("SL4", "0");
            dt.Add("XZID", strXZID);
            dt.Add("CRDate", (string)sbdata["ysdate"]); //妥协办法，将创建时间存用水时间,不然报表改动太大
            dt.Add("intProcessStanceid", "0");
            dt.Add("ComID", "10334");
            dt.Add("DCode", strZID);
            dt.Add("DName", strZNAME);

            string strGGZW = (string)sbdata["delb"];
            dt.Add("ggzw", strGGZW);
            DataTable dtXZQ = new JH_Auth_UserB().GetDTByCommand("SELECT pid,pname FROM  qj_cz WHERE ID= '" + strXZID + "'");
            dt.Add("cid", dtXZQ.Rows[0]["pid"].ToString());
            dt.Add("cname", dtXZQ.Rows[0]["pname"].ToString());
            dt.Add("jlstatus", "待审核");
            dt.Add("zfzt", "未支付");

            dt.Add("xcjg", (string)sbdata["xgdj"]);
            dt.Add("xczje", (string)sbdata["xgje"]);
            dt.Add("hjje", (string)sbdata["hjje"]);


            if (sbdata["czname"].ToString().Split('/').Length > 3)
            {
                dt.Add("CZNAME", sbdata["czname"].ToString().Split('/')[3]);

            }
            DataTable dtLYJG = new JH_Auth_UserB().GetDTByCommand("SELECT GYJG,jg.Type  FROM qj_sfjs_jg JG INNER  JOIN qj_sfjs_de DE ON jg.Type= DE.type and jg.zid =DE.zid WHERE jg.ZID = '" + strZID + "' AND DE.dymc = '" + strGGZW + "' ORDER BY JG.DW");
            if (dtLYJG.Rows.Count > 0)
            {
                dt.Add("TYPE", dtLYJG.Rows[0]["Type"].ToString());


                dt.Add("ZJE", Math.Round(decimal.Parse((string)sbdata["zje"]), 2, MidpointRounding.AwayFromZero));
                dt.Add("GYSJ", dtLYJG.Rows[0]["GYJG"].ToString());

                decimal GYZJE = decimal.Parse((string)sbdata["sl1"]) * decimal.Parse(dtLYJG.Rows[0]["GYJG"].ToString()) + decimal.Parse((string)sbdata["sl2"]) * decimal.Parse(dtLYJG.Rows[0]["GYJG"].ToString());
                dt.Add("GYZJE", dtLYJG.Rows[0]["GYJG"].ToString());

            }
            db.InserData(dt, "qj_sfjs_ysdata");
            msg.Result = "上报成功";


            //发送消息
            //string strOpenid = userListjfyh.Rows[0]["wxopenid"].ToString();
            //string strTitle = "尊敬的" + dt.Rows[i]["xm"].ToString() + "用户，您本期用水情况如下：";
            //string strRemark = "温馨提示：请您尽快交清本次水费。如有异议，请联系用水地村组管水员或管理站。";
            //string strYHDZ = dt.Rows[i]["cname"].ToString() + "/" + dt.Rows[i]["czname"].ToString();
            //string strYSL = (decimal.Parse(dt.Rows[i]["sl1"].ToString()) + decimal.Parse(dt.Rows[i]["sl2"].ToString())).ToString() + "方";
            //string strSFJE = dt.Rows[i]["hjje"].ToString() + "元";
            //string strYHH = userListjfyh.Rows[0]["ID"].ToString();
            //new WXGZHHelp().SENDWXMSG(strOpenid, UserInfo.QYinfo.WebSite + "/MOBWeb/" + SFJSHelp.GetMobPath(UserInfo.QYinfo.QYName) + "/indexgzh.html?dataid=" + ID, strTitle, strRemark, strYHDZ, strYSL, strSFJE, strYHH);
            //发送消息

        }


        /// <summary>
        /// 获取当前账号上报历史数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string UP = context.Request("UP");
            string XM = context.Request("XM") ?? "";

            DataTable dtUser = GetUserInfo(UP, UserInfo);
            string strUser = dtUser.Rows[0]["username"].ToString();
            string czids = dtUser.Rows[0]["czids"].ToString();

            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            string strWhere = "";
            if (!string.IsNullOrEmpty(SDATE))
            {
                strWhere = " AND  CRDate BETWEEN '" + SDATE + " 00:00:00' AND  '" + EDATE + " 23:59:59'";
            }
            if (dtUser.Rows.Count > 0)
            {
                if (XM == "")
                {
                    msg.Result = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM vwSBData WHERE CRUser='" + strUser + "'" + strWhere + " ORDER BY CRDate  ");
                    msg.Result1 = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM vwSBData WHERE XZID IN ('" + czids.ToFormatLike() + "')" + strWhere + " ORDER BY CRDate  ");


                }
                else
                {
                    msg.Result = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM vwSBData WHERE CRUser='" + strUser + "'" + strWhere + " AND XM LIKE '%" + XM + "%' ORDER BY CRDate  ");
                    msg.Result1 = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM vwSBData WHERE XZID IN ('" + czids.ToFormatLike() + "')" + strWhere + " AND XM LIKE '%" + XM + "%' ORDER BY CRDate  ");

                }

            }

        }
        /// <summary>
        /// ADDYH
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDYH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP, UserInfo);

            string strSFZBM = context.Request("SFZBM");
            if (string.IsNullOrEmpty(strSFZBM))
            {
                msg.ErrorMsg = "身份证号码不能为空";
                return;
            }
            if (strSFZBM.Length != 18)
            {
                msg.ErrorMsg = "必须为18位身份证号码";
                return;
            }
            if (string.IsNullOrEmpty(context.Request("YHName")))
            {
                msg.ErrorMsg = "姓名不能为空";
                return;
            }


            string ID = context.Request("ID") ?? "0";
            DBFactory db = new BI_DB_SourceB().GetDB(0);
            var dt = new Dictionary<string, object>();
            dt.Add("YHName", context.Request("YHName"));
            dt.Add("sfzbm", context.Request("SFZBM"));
            dt.Add("lxdh", context.Request("LXDH"));
            dt.Add("dlpassword", CommonHelp.GetMD5("abc123"));
            dt.Add("xzid", context.Request("XZID"));
            dt.Add("xzname", context.Request("XZName").Split('/')[3].ToString());

            DataTable dtXZ = getXZ(context.Request("XZID"));
            if (dtXZ.Rows.Count > 0)
            {
                dt.Add("pname", dtXZ.Rows[0]["pname"].ToString());
                //dt.Add("dbname", dtXZ.Rows[0]["dbname"].ToString());
            }
            if (ID != "0")
            {
                dt.Add("id", ID);
                db.UpdateData(dt, "qj_sfjs_jfuser");
            }
            else
            {
                dt.Add("zid", dtUser.Rows[0]["zid"].ToString());
                dt.Add("zname", dtUser.Rows[0]["zname"].ToString());
                dt.Add("CRUser", dtUser.Rows[0]["username"].ToString());
                dt.Add("DCode", dtUser.Rows[0]["zid"].ToString());
                dt.Add("DName", dtUser.Rows[0]["zname"].ToString());
                dt.Add("CRUserName", dtUser.Rows[0]["userrealname"].ToString());
                dt.Add("CRDate", DateTime.Now.ToString());
                dt.Add("intProcessStanceid", "0");
                dt.Add("lsmj", "0");
                dt.Add("jjmj", "0");
                dt.Add("zmj", "0");
                dt.Add("isyx", "Y");
                dt.Add("ComID", "10334");
                db.InserData(dt, "qj_sfjs_jfuser");
            }
        }




        public void DELYH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP, UserInfo);
            if (dtUser.Rows.Count > 0)
            {
                string strxzid = P1;
                new JH_Auth_UserB().ExsSclarSql("DELETE qj_sfjs_jfuser WHERE ID='" + P1 + "'");
                msg.Result = "删除用户成功";
            }


        }


        /// <summary>
        /// 统计数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBTJLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string UP = context.Request("UP");
            string XM = context.Request("XM") ?? "";
            string strUser = UP.Split(',')[0];

            string SDATE = context.Request("SDATE") ?? "";
            string EDATE = context.Request("EDATE") ?? "";

            string strWhere = " AND  CRDate BETWEEN '" + SDATE + " 00:00:00' AND  '" + EDATE + " 23:59:59'";

            //获取价格
            DataTable DT = new JH_Auth_UserB().GetDTByCommand("  SELECT CZNAME, SUM(cast(TDMS as decimal(10, 1))) AS TDMS, SUM(cast(YSSC as decimal(10, 0))) AS YSSC,  SUM(ZSL) AS SLL, SUM(cast(ZJE as decimal(10, 2))) AS ZJE FROM vwSBData WHERE CRUser = '" + strUser + "' " + strWhere + " GROUP BY  CZNAME ");
            msg.Result = DT;

        }


        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="strUserAndPass"></param>
        /// <returns></returns>
        public DataTable GetUserInfoOLD(string strUserAndPass)
        {
            string username = strUserAndPass.Split(',')[0];
            string password = strUserAndPass.Split(',')[1];
            DataTable userList = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM qj_xzuser where czusername='" + username + "' and password='" + password + "'");

            DataTable userListjfyh = new JH_Auth_UserB().GetDTByCommand("SELECT  * FROM qj_sfjs_jfuser WHERE sfzbm='" + username + "' AND dlpassword='" + password + "'");

            password = CommonHelp.GetMD5(password);
            DataTable userListgly = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM jh_auth_user where username='" + username + "' and UserPass='" + password + "'");



            DataTable dtyh = new DataTable();
            dtyh.Columns.Add("usertype");
            dtyh.Columns.Add("username");
            dtyh.Columns.Add("password");
            dtyh.Columns.Add("userrealname");
            dtyh.Columns.Add("userbranch");
            dtyh.Columns.Add("zid");
            dtyh.Columns.Add("zname");
            dtyh.Columns.Add("lxfs");
            dtyh.Columns.Add("czids");
            for (int i = 0; i < userList.Rows.Count; i++)
            {
                DataRow newRow;
                newRow = dtyh.NewRow();
                newRow["usertype"] = "村组管理员";
                newRow["username"] = userList.Rows[0]["czusername"].ToString();
                newRow["password"] = userList.Rows[0]["password"].ToString();
                newRow["userrealname"] = userList.Rows[0]["userrealname"].ToString();
                newRow["userbranch"] = userList.Rows[0]["zname"].ToString();
                newRow["zid"] = userList.Rows[0]["zid"].ToString();
                newRow["zname"] = userList.Rows[0]["zname"].ToString();
                newRow["lxfs"] = userList.Rows[0]["lxfs"].ToString();
                newRow["czids"] = userList.Rows[0]["czids"].ToString();

                dtyh.Rows.Add(newRow);
            }
            for (int i = 0; i < userListgly.Rows.Count; i++)
            {
                String strUserName = userListgly.Rows[0]["username"].ToString();
                string strRoleName = new JH_Auth_UserRoleB().GetRoleNameByUserName(strUserName, 10334);


                DataRow newRow;
                newRow = dtyh.NewRow();
                newRow["username"] = userListgly.Rows[0]["username"].ToString();
                newRow["password"] = userListgly.Rows[0]["userpass"].ToString();
                newRow["userrealname"] = userListgly.Rows[0]["userrealname"].ToString();
                newRow["lxfs"] = userListgly.Rows[0]["mobphone"].ToString();
                newRow["userbranch"] = userListgly.Rows[0]["branchCode"].ToString();
                if (strRoleName.Contains("斗长"))
                {
                    newRow["usertype"] = "斗长";//系统用户

                    DataTable DData = new JH_Auth_UserB().GetDTByCommand("select STUFF((select ',' + cast(ID as varchar) from qj_db  where dgly='" + strUserName + "' FOR XML PATH('')),1,1,'')  AS DBIDS;");

                    DataTable GLZata = new JH_Auth_UserB().GetDTByCommand("select TOP 1  zid,zname  from qj_db  where dgly='" + strUserName + "' ");
                    newRow["zid"] = GLZata.Rows[0]["zid"].ToString();
                    newRow["zname"] = GLZata.Rows[0]["zname"].ToString();
                    if (DData.Rows.Count > 0)
                    {
                        string strDIDS = DData.Rows[0]["DBIDS"].ToString();
                        DataTable XZata = new JH_Auth_UserB().GetDTByCommand("select STUFF((select ',' + cast(ID as varchar) from qj_cz  where did IN ('" + strDIDS.ToFormatLike() + "') FOR XML PATH('')),1,1,'')  AS XZIDS;");
                        if (XZata.Rows.Count > 0)
                        {
                            newRow["czids"] = XZata.Rows[0]["XZIDS"].ToString();
                        }
                    }
                }
                else
                {
                    newRow["usertype"] = "管理局人员";//系统用户
                    newRow["zid"] = "";
                    newRow["zname"] = "";
                    newRow["czids"] = "";

                }
                dtyh.Rows.Add(newRow);


            }


            for (int i = 0; i < userListjfyh.Rows.Count; i++)
            {
                DataRow newRow;
                newRow = dtyh.NewRow();
                newRow["usertype"] = "用水户";//系统用户
                newRow["username"] = userListjfyh.Rows[0]["sfzbm"].ToString();
                newRow["password"] = userListjfyh.Rows[0]["dlpassword"].ToString();
                newRow["userrealname"] = userListjfyh.Rows[0]["YHName"].ToString();
                newRow["lxfs"] = userListjfyh.Rows[0]["lxdh"].ToString();
                newRow["userbranch"] = userListjfyh.Rows[0]["zid"].ToString();
                newRow["zid"] = userListjfyh.Rows[0]["zid"].ToString();
                newRow["zname"] = userListjfyh.Rows[0]["zname"].ToString();
                newRow["czids"] = userListjfyh.Rows[0]["xzid"].ToString();
                dtyh.Rows.Add(newRow);
            }
            return dtyh;
        }





        public void SenWXMSG()
        {

            //string strOpenid = userListjfyh.Rows[0]["wxopenid"].ToString();
            //string strTitle = "尊敬的" + dt.Rows[i]["xm"].ToString() + "用户，您本期用水情况如下：";
            //string strRemark = "温馨提示：请您尽快交清本次水费。如有异议，请联系用水地村组管水员或管理站。";
            //string strYHDZ = dt.Rows[i]["cname"].ToString() + "/" + dt.Rows[i]["czname"].ToString();
            //string strYSL = (decimal.Parse(dt.Rows[i]["sl1"].ToString()) + decimal.Parse(dt.Rows[i]["sl2"].ToString())).ToString() + "方";
            //string strSFJE = dt.Rows[i]["hjje"].ToString() + "元";
            //string strYHH = userListjfyh.Rows[0]["ID"].ToString();
            //new WXGZHHelp().SENDWXMSG(strOpenid, UserInfo.QYinfo.WebSite + "/MOBWeb/" + SFJSHelp.GetMobPath(UserInfo.QYinfo.QYName) + "/indexgzh.html?dataid=" + ID, strTitle, strRemark, strYHDZ, strYSL, strSFJE, strYHH);
        }


        /// <summary>
        /// 获取上报数据//弃用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string SFZHM = P1.Trim();
            string TYPE = context.Request("TYPE") ?? "0";
            string UP = context.Request("UP") ?? "";
            DataTable dt = new DataTable();
            if (TYPE == "0")
            {
                DateTime datest = DateTime.Now.AddMonths(-5);
                dt = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + UP.Split(',')[0] + "' AND CRDate>'" + datest.ToString() + "' ORDER BY CRDate DESC");

            }
            else
            {
                DateTime datest = DateTime.Now.AddMonths(-12);
                dt = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + UP.Split(',')[0] + "' AND CRDate>'" + datest.ToString() + "' ORDER BY CRDate DESC");

            }
            if (dt.Rows.Count > 0)
            {
                msg.Result = dt;
            }
            else
            {
                msg.ErrorMsg = "没找到灌溉信息";

            }


        }

        public void GETSBDATAV1(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string SFZHM = P1.Trim();
            string TYPE = context.Request("TYPE") ?? "0";
            string UP = context.Request("UP") ?? "";
            DataTable dt = new DataTable();
            DataTable dtdzf = new DataTable();

            if (TYPE == "0")
            {
                dt = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + SFZHM + "' AND jlstatus='已审核' and  zfzt='未支付' ORDER BY CRDate DESC");

            }
            else if (TYPE == "1")
            {
                dt = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + SFZHM + "' AND zfzt='已支付' ORDER BY CRDate DESC");

            }
            else
            {
                dt = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + SFZHM + "' AND jlstatus='已审核'  ORDER BY CRDate DESC");
            }
            if (dt.Rows.Count > 0)
            {
                msg.Result = dt;
                msg.Result1 = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + SFZHM + "' AND jlstatus='已审核' and  zfzt='未支付' ORDER BY CRDate DESC"); ;

            }
            else
            {
                msg.ErrorMsg = "没找到灌溉信息";

            }
        }



        /// <summary>
        /// 获取用水户灌溉信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBDATAV2(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string SFZHM = P1.Trim();
            string TYPE = context.Request("TYPE") ?? "0";
            string UP = context.Request("UP") ?? "";
            string dataid = context.Request("dataid") ?? "0";
            DataTable dtDZF = new DataTable();
            DataTable dtYzf = new DataTable();
            if (dataid == "0")
            {
                dtDZF = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + SFZHM + "' AND jlstatus='已审核' and  zfzt='未支付'  ORDER BY CRDate DESC");
            }
            else
            {
                dtDZF = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE ID='" + dataid + "' AND sfzhm='" + SFZHM + "'   ORDER BY CRDate DESC");
            }

            dtYzf = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + SFZHM + "' AND jlstatus='已审核' and  zfzt='已支付' ORDER BY CRDate DESC");
            DataTable dtTJ = new JH_Auth_UserB().GetDTByCommand("SELECT SUM(Convert(decimal(18,2),ZSL)) ZSL,SUM(Convert(decimal(18,2),TDMS)) TDMS ,SUM(Convert(decimal(18,2),hjje)) hjje  FROM vwSBData where sfzhm='" + SFZHM + "'");
            DataTable dtTJdzf = new JH_Auth_UserB().GetDTByCommand("SELECT SUM(Convert(decimal(18,2),hjje)) hjje  FROM vwSBData where  jlstatus='已审核' and  zfzt='未支付' and sfzhm='" + SFZHM + "'");
            msg.Result = dtDZF;
            msg.Result1 = dtYzf;
            msg.Result2 = dtTJ;
            msg.Result3 = dtTJdzf;

        }

        /// <summary>
        /// 获取用水户灌溉信息V3
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBDATAV3(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string SFZHM = P1.Trim();
            string TYPE = context.Request("TYPE") ?? "0";
            string UP = context.Request("UP") ?? "";
            string ND = P2;

            DataTable dtYzf = new DataTable();
            dtYzf = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + SFZHM + "' AND jlstatus='已审核' and  CONVERT(nvarchar(6), CRDate, 112)  LIKE '" + P2 + "%' ORDER BY CRDate DESC");
            DataTable dtTJ = new JH_Auth_UserB().GetDTByCommand("SELECT SUM(Convert(decimal(18,2),ZSL)) ZSL,SUM(Convert(decimal(18,2),TDMS)) TDMS ,SUM(Convert(decimal(18,2),hjje)) hjje  FROM vwSBData where jlstatus='已审核' and  CONVERT(nvarchar(6), CRDate, 112)  LIKE '" + P2 + "%'  and   sfzhm='" + SFZHM + "'");
            DataTable dtTJdzf = new JH_Auth_UserB().GetDTByCommand("SELECT SUM(Convert(decimal(18,2),hjje)) hjje  FROM vwSBData where  jlstatus='已审核' and  zfzt='未支付' and sfzhm='" + SFZHM + "'");
            msg.Result = dtYzf;
            msg.Result2 = dtTJ;
            msg.Result3 = dtTJdzf;

        }



        /// <summary>
        /// 复制数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void COPYDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtglz = new JH_Auth_UserB().GetDTByCommand("SELECT * from jh_auth_branch where Remark2='Y' AND DeptCode in (" + P1 + ") ");
            string TYPE = context.Request("TYPE") ?? "JG";
            if (TYPE == "JG")
            {
                for (int m = 0; m < dtglz.Rows.Count; m++)
                {
                    //复制价格
                    DataTable dtJG = new JH_Auth_UserB().GetDTByCommand("SELECT * from qj_sfjs_jg where ID IN (" + P2 + ")");
                    for (int i = 0; i < dtJG.Rows.Count; i++)
                    {
                        new JH_Auth_UserB().ExsSclarSql("INSERT INTO [qj_sfjs_jg] ( [CRUser], [CRDate], [intProcessStanceid], [ComID], [Type], [DW], [jg], [gyjg], [zid], [qjjg], [DCode], [DName], [CRUserName], [zname], [jqmc]) VALUES " +
                            "( N'administrator', '" + DateTime.Now.ToString() + "', '0', '10334', N'" + dtJG.Rows[i]["Type"].ToString() + "', N'" + dtJG.Rows[i]["DW"].ToString() + "', '" + dtJG.Rows[i]["jg"].ToString() + "', '" + dtJG.Rows[i]["gyjg"].ToString() + "', '" + dtglz.Rows[m]["DeptCode"].ToString() + "', NULL, N'" + dtglz.Rows[m]["DeptCode"].ToString() + "', N'" + dtglz.Rows[m]["DeptName"].ToString() + "', N'administrator', N'" + dtglz.Rows[m]["DeptName"].ToString() + "', '" + dtJG.Rows[i]["jqmc"].ToString() + "');");
                    }
                }

            }
            else
            {
                for (int m = 0; m < dtglz.Rows.Count; m++)
                {
                    DataTable dtDE = new JH_Auth_UserB().GetDTByCommand("SELECT * from qj_sfjs_de where ID IN (" + P2 + ")");
                    for (int i = 0; i < dtDE.Rows.Count; i++)
                    {
                        new JH_Auth_UserB().ExsSclarSql("INSERT INTO [qj_sfjs_de] ( [CRUser], [CRDate], [intProcessStanceid], [ComID], [type], [dw], [zid], [remark], [DCode], [DName], [CRUserName], [zname], [dymc])  VALUES (" +
                                                                                 " N'administrator', '" + DateTime.Now.ToString() + "', '0', '10334', N'" + dtDE.Rows[i]["type"].ToString() + "', N'" + dtDE.Rows[i]["dw"].ToString() + "', '" + dtglz.Rows[m]["DeptCode"].ToString() + "', NULL, N'" + dtglz.Rows[m]["DeptCode"].ToString() + "', N'" + dtglz.Rows[m]["DeptName"].ToString() + "', N'administrator', N'" + dtglz.Rows[m]["DeptName"].ToString() + "', '" + dtDE.Rows[i]["dymc"].ToString() + "');");
                    }
                }


            }


        }



        /// <summary>
        /// 复制数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETSJSM(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY QY = new JH_Auth_QYB().GetALLEntities().FirstOrDefault();
            if (!QY.wxqrcode.Contains(QY.WebSite + "/upload"))
            {
                QY.wxqrcode = QY.wxqrcode.Replace("/upload", QY.WebSite + "/upload");
            }
            msg.Result = QY;


        }
        public void SETSJSMBC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            JH_Auth_QY QY = new JH_Auth_QYB().GetALLEntities().FirstOrDefault();

            if (P1 != "")
            {
                QY.wxqrcode = P1;
                QY.EncodingAESKey = context.Request("pz");
                QY.FinanceLXR = context.Request("iskq");//是否开启上报功能
                new JH_Auth_QYB().Update(QY);
                msg.Result = QY;
                CacheHelp.Remove(UserInfo.User.UserName);
            }



        }
        public void GETAPPV(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY QY = new JH_Auth_QYB().GetALLEntities().FirstOrDefault();
            msg.Result = "1.1";
            msg.Result1 = QY.WebSite + "/MobWeb/" + SFJSHelp.GetMobPath(QY.QYName) + "/app-release.apk";
            msg.Result2 = "系统更新内容";
            msg.Result3 = QY.QYName.Substring(0, 6);
            msg.Result4 = QY.QYName.Substring(6);
            msg.Result5 = QY.Position;

            // 陕西省石头河水库灌溉中心

        }

        public void SFLOGIN(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY QY = new JH_Auth_QYB().GetALLEntities().FirstOrDefault();
            string websit = SFJSHelp.GetMobPath(QY.QYName);

            DataTable dtyh = new DataTable();
            dtyh = new SFJSManage().GetUserInfo(context.Request("username") + "," + context.Request("password"));
            if (dtyh.Rows.Count == 0)
            {
                msg.ErrorMsg = "用户名或密码不正确";
            }
            else
            {
                msg.Result = dtyh;

                string strUserType = dtyh.Rows[0]["usertype"].ToString();
                string strxzids = dtyh.Rows[0]["czids"].ToString();
                string strzid = dtyh.Rows[0]["zid"].ToString();



                DataTable dt = new JH_Auth_QYB().GetDTByCommand("SELECT id,title,fbdate,imgurl,lls FROM qj_sys_tzgg WHERE ggtype='新闻资讯'  ORDER BY ID DESC");
                DataTable dt1 = new JH_Auth_QYB().GetDTByCommand("SELECT   id,title,fbdate,imgurl,lls FROM qj_sys_tzgg WHERE  ggtype='新闻资讯' AND islb='是' ORDER BY ID DESC");
                DataTable dt3 = new JH_Auth_QYB().GetDTByCommand("SELECT id,title,fbdate,imgurl,lls FROM qj_sys_tzgg WHERE  ggtype='系统公告'  AND islb='是' ORDER BY ID DESC");

                DataTable dtyys = new DataTable(); //应用
                dtyys.Columns.Add("yytype");//应用类型
                dtyys.Columns.Add("yyname");//应用名称
                dtyys.Columns.Add("yyico");//应用log
                dtyys.Columns.Add("url");//应用Url.如果应用类型是1，代表需要打开URL，如果是0，则是对应android页面代码
                string strUP = context.Request("username") + "," + context.Request("password");
                if (strUserType == "小组管理员")
                {
                    dtyys.Rows.Add(new object[] { "1", "计量计费", QY.WebSite + "/MobWeb/" + websit + "/index/nav-001.png", QY.WebSite + "/MobWeb/" + websit + "/UIM/REF.html?v=4&p=0&up=" + strUP });
                    //dtyys.Rows.Add(new object[] { "0", "用户管理", QY.WebSite + "/MobWeb/"+ websit + "/index/nav-005.png", "B" });
                    dtyys.Rows.Add(new object[] { "1", "上报历史", QY.WebSite + "/MobWeb/" + websit + "/index/nav-006.png", QY.WebSite + "/MobWeb/" + websit + "/UIM/REF.html?v=4&p=1&up=" + strUP });
                    dtyys.Rows.Add(new object[] { "1", "用户管理", QY.WebSite + "/MobWeb/" + websit + "/index/nav-005.png", QY.WebSite + "/MobWeb/" + websit + "/UIM/REF.html?v=4&p=2&up=" + strUP });

                    //dtyys.Rows.Add(new object[] { "0", "上报历史", QY.WebSite + "/MobWeb/SFJSSTH/index/nav-006.png", "C" });
                    dtyys.Rows.Add(new object[] { "1", "水价说明", QY.WebSite + "/MobWeb/" + websit + "/index/nav-008.png", QY.WebSite + "/MobWeb/" + websit + "/UIM/REF.html?v=4&p=3&up=" + strUP });


                }
                else if (strUserType == "斗长" || strUserType == "协会人员")
                {

                    dtyys.Rows.Add(new object[] { "1", "计量计费", QY.WebSite + "/MobWeb/" + websit + "/index/nav-001.png", QY.WebSite + "/MobWeb/" + websit + "/UIM/REF.html?v=4&p=0&up=" + strUP });
                    //dtyys.Rows.Add(new object[] { "0", "用户管理", QY.WebSite + "/MobWeb/"+ websit + "/index/nav-005.png", "B" });
                    dtyys.Rows.Add(new object[] { "1", "上报历史", QY.WebSite + "/MobWeb/" + websit + "/index/nav-006.png", QY.WebSite + "/MobWeb/" + websit + "/UIM/REF.html?v=4&p=1&up=" + strUP });
                    dtyys.Rows.Add(new object[] { "1", "用户管理", QY.WebSite + "/MobWeb/" + websit + "/index/nav-005.png", QY.WebSite + "/MobWeb/" + websit + "/UIM/REF.html?v=4&p=2&up=" + strUP });

                    //dtyys.Rows.Add(new object[] { "0", "上报历史", QY.WebSite + "/MobWeb/SFJSSTH/index/nav-006.png", "C" });
                    dtyys.Rows.Add(new object[] { "1", "水价说明", QY.WebSite + "/MobWeb/" + websit + "/index/nav-008.png", QY.WebSite + "/MobWeb/" + websit + "/UIM/REF.html?v=4&p=3&up=" + strUP });
                }
                else
                {

                    dtyys.Rows.Add(new object[] { "1", "水价说明", QY.WebSite + "/MobWeb/" + websit + "/index/nav-008.png", QY.WebSite + "/MobWeb/" + websit + "/UIM/REF.html?p=3&up=" + strUP });
                    dtyys.Rows.Add(new object[] { "1", "管理站统计", QY.WebSite + "/MobWeb/SFJSSTH/index/nav-008.png", QY.WebSite + "/ViewV5/AppPage/DATABI/YBPVIEW.html?ID=59&vtype=1" });

                }
                msg.Result1 = dtyys;
                msg.Result2 = dt1;
                msg.Result3 = dt;


                #region 斗别-村组
                //DataTable dtdb = new JH_Auth_QYB().GetDTByCommand("SELECT distinct did,dbname FROM vw_dbcz  WHERE   id IN ('" + strxzids.ToFormatLike() + "')");

                //DataTable dtcz = new JH_Auth_QYB().GetDTByCommand("SELECT id,xzname,pname,did,dbname,(pname+'/'+xzname) AS xz FROM vw_dbcz  WHERE   id IN ('" + strxzids.ToFormatLike() + "')");
                //dtdb.Columns.Add("xzdata", Type.GetType("System.Object"));
                //for (int i = 0; i < dtdb.Rows.Count; i++)
                //{
                //    dtdb.Rows[i]["xzdata"] = dtcz.Where("did='" + dtdb.Rows[i]["did"].ToString() + "'");
                //}
                //msg.Result4 = dtdb;

                #endregion


                DataTable dtcz = new JH_Auth_QYB().GetDTByCommand("SELECT id,xzname,pname,did,dbname,(pname+'/'+xzname) AS xz FROM qj_cz  WHERE   id IN ('" + strxzids.ToFormatLike() + "')");
                dtcz.Columns.Add("dbdata", Type.GetType("System.Object"));
                for (int i = 0; i < dtcz.Rows.Count; i++)
                {
                    string czid = dtcz.Rows[i]["id"].ToString();
                    dtcz.Rows[i]["dbdata"] = new JH_Auth_QYB().GetDTByCommand("SELECT did,dbname FROM qj_db_xz  WHERE   xzid IN ('" + czid + "')");
                }
                msg.Result4 = dtcz;

                msg.Result6 = dt3;

                JH_Auth_Branch Branch = new JH_Auth_BranchB().GetEntities(d => d.DeptCode == 1).FirstOrDefault();
                if (Branch != null)
                {
                    msg.Result5 = Branch.IsHasQX;
                }

            }

        }


        /// <summary>
        /// 获取用户信息,
        /// </summary>
        /// <param name="strUserAndPass"></param>
        /// <returns></returns>
        public DataTable GetUserInfo(string strUserAndPass, JH_Auth_UserB.UserInfo UserInfo = null)
        {

            string username = "";
            string password = "";
            string passwordmd5 = "";
            if (!string.IsNullOrEmpty(strUserAndPass))
            {
                strUserAndPass = strUserAndPass.Replace("%2C", ",");//处理Cook得数据
                username = strUserAndPass.Split(',')[0];
                password = strUserAndPass.Split(',')[1];
                passwordmd5 = CommonHelp.GetMD5(password);
            }
            else
            {
                if (UserInfo != null)
                {
                    username = UserInfo.User.UserName;
                    password = UserInfo.User.UserPass;
                }
            }

            DataTable userListjfyh = new JH_Auth_UserB().GetDTByCommand("SELECT  * FROM qj_sfjs_jfuser WHERE sfzbm='" + username + "' AND (dlpassword='" + passwordmd5 + "' or  dlpassword='" + password + "' )");
            DataTable userListgly = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM jh_auth_user where username='" + username + "' and (UserPass='" + passwordmd5 + "' or  UserPass='" + password + "' )");



            DataTable dtyh = new DataTable();
            dtyh.Columns.Add("usertype");
            dtyh.Columns.Add("username");
            dtyh.Columns.Add("password");
            dtyh.Columns.Add("userrealname");
            dtyh.Columns.Add("userbranch");
            dtyh.Columns.Add("zid");
            dtyh.Columns.Add("zname");
            dtyh.Columns.Add("lxfs");
            dtyh.Columns.Add("czids");
            dtyh.Columns.Add("dbids");
            dtyh.Columns.Add("wxopenid");
            dtyh.Columns.Add("dizhi");


            for (int i = 0; i < userListgly.Rows.Count; i++)
            {
                String strUserName = userListgly.Rows[0]["username"].ToString();
                string strRoleName = new JH_Auth_UserRoleB().GetRoleNameByUserName(strUserName, 10334);


                DataRow newRow;
                newRow = dtyh.NewRow();
                newRow["username"] = userListgly.Rows[0]["username"].ToString();
                newRow["password"] = userListgly.Rows[0]["userpass"].ToString();
                newRow["userrealname"] = userListgly.Rows[0]["userrealname"].ToString();
                newRow["lxfs"] = userListgly.Rows[0]["mobphone"].ToString();
                newRow["userbranch"] = userListgly.Rows[0]["branchCode"].ToString();
                if (strRoleName.Contains("斗长") || strRoleName.Contains("协会人员"))
                {
                    newRow["usertype"] = "斗长";//系统用户

                    DataTable DData = new JH_Auth_UserB().GetDTByCommand("select STUFF((select ',' + cast(ID as varchar) from qj_db  where dgly='" + strUserName + "' FOR XML PATH('')),1,1,'')  AS DBIDS;");

                    DataTable GLZata = new JH_Auth_UserB().GetDTByCommand("select TOP 1  zid,zname  from qj_db  where dgly='" + strUserName + "' ");
                    if (GLZata.Rows.Count > 0)
                    {
                        newRow["zid"] = GLZata.Rows[0]["zid"].ToString();
                        newRow["zname"] = GLZata.Rows[0]["zname"].ToString();
                        if (DData.Rows.Count > 0)
                        {
                            string strDIDS = DData.Rows[0]["DBIDS"].ToString();
                            newRow["dbids"] = strDIDS;
                            DataTable XZata = new JH_Auth_UserB().GetDTByCommand("select STUFF((select ',' + cast(ID as varchar) from vw_dbcz  where did IN ('" + strDIDS.ToFormatLike() + "') FOR XML PATH('')),1,1,'')  AS XZIDS;");
                            if (XZata.Rows.Count > 0)
                            {
                                newRow["czids"] = XZata.Rows[0]["XZIDS"].ToString();
                            }
                        }
                    }

                }
                else if (strRoleName.Contains("小组管理员"))
                {
                    newRow["usertype"] = "小组管理员";//系统用户

                    DataTable CZata = new JH_Auth_UserB().GetDTByCommand("select STUFF((select ',' + cast(ID as varchar) from qj_cz  where ','+czgly+','  like '%," + strUserName + ",%' FOR XML PATH('')),1,1,'')  AS CZIDS;");
                    if (CZata.Rows.Count > 0)
                    {
                        DataTable GLZata = new JH_Auth_UserB().GetDTByCommand("select TOP 1  zid,zname  from qj_cz  where ','+czgly+','  like '%," + strUserName + ",%' ");
                        if (GLZata.Rows.Count > 0)
                        {
                            newRow["zid"] = GLZata.Rows[0]["zid"].ToString();
                            newRow["zname"] = GLZata.Rows[0]["zname"].ToString();
                            newRow["czids"] = CZata.Rows[0]["CZIDS"].ToString();

                        }

                    }
                }
                else if (strRoleName.Contains("站管理员"))
                {
                    newRow["usertype"] = "站管理员";//系统用户

                    string strdcode = userListgly.Rows[0]["branchCode"].ToString();
                    DataTable CZata = new JH_Auth_UserB().GetDTByCommand("select STUFF((select ',' + cast(ID as varchar) from qj_cz  where zid='" + strdcode + "' FOR XML PATH('')),1,1,'')  AS CZIDS;");
                    if (CZata.Rows.Count > 0)
                    {

                        newRow["zid"] = strdcode;
                        newRow["zname"] = strdcode;
                        newRow["czids"] = CZata.Rows[0]["CZIDS"].ToString();

                    }
                }
                else
                {
                    newRow["usertype"] = strRoleName;//系统用户
                    newRow["zid"] = userListgly.Rows[0]["branchCode"].ToString();
                    newRow["zname"] = new JH_Auth_BranchB().GetBMByDeptCode(10334, int.Parse(userListgly.Rows[0]["branchCode"].ToString())).DeptName;
                    newRow["czids"] = "";

                }
                dtyh.Rows.Add(newRow);


            }


            for (int i = 0; i < userListjfyh.Rows.Count; i++)
            {
                DataRow newRow;
                newRow = dtyh.NewRow();
                newRow["usertype"] = "用水户";//系统用户
                newRow["username"] = userListjfyh.Rows[0]["sfzbm"].ToString();
                newRow["password"] = userListjfyh.Rows[0]["dlpassword"].ToString();
                newRow["userrealname"] = userListjfyh.Rows[0]["YHName"].ToString();
                newRow["lxfs"] = userListjfyh.Rows[0]["lxdh"].ToString();
                newRow["userbranch"] = userListjfyh.Rows[0]["zid"].ToString();
                newRow["zid"] = userListjfyh.Rows[0]["zid"].ToString();
                newRow["zname"] = userListjfyh.Rows[0]["zname"].ToString();
                newRow["czids"] = userListjfyh.Rows[0]["xzid"].ToString();
                newRow["wxopenid"] = userListjfyh.Rows[0]["wxopenid"].ToString();

                newRow["dizhi"] = userListjfyh.Rows[0]["pname"].ToString() + "/" + userListjfyh.Rows[0]["xzname"].ToString();

                dtyh.Rows.Add(newRow);
            }
            return dtyh;
        }



        public void GETSBITEM(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dt = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE id in ('" + P1.Trim(',').ToFormatLike() + "') AND jlstatus='已审核' ORDER BY CRDate DESC");
            msg.Result = dt;

        }


        public void WXZF(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string code = context.Request("code") ?? "";
            string orderid = "";
            string username = "";
            decimal ddje = 0;
            CommonHelp.WriteLOG("开始缴费" + code + "," + P1);

            DataTable dtOrderReturn = new JH_Auth_UserB().GetDTByCommand("select * from qj_sfjs_ysdata where id='" + P1 + "'");
            CommonHelp.WriteLOG("生成订单信息1" + JsonConvert.SerializeObject(dtOrderReturn));

            if (dtOrderReturn.Rows.Count > 0)
            {
                orderid = dtOrderReturn.Rows[0]["id"].ToString();
                ddje = decimal.Parse(dtOrderReturn.Rows[0]["zje"].ToString());
                username = dtOrderReturn.Rows[0]["sfzhm"].ToString();

            }


            CommonHelp.WriteLOG("订单号" + orderid + "金额" + ddje);

            WeiXinPay.WXPayJsapi(ref msg, username, orderid, code, "", ddje);
        }






        //获取今日水情以及灌概年度总数据,最近得灌溉记录,农作物统计
        public void GETJRSQ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {


            DataTable dtBM = new JH_Auth_UserB().GetDTByCommand("SELECT TOP 1  * FROM [dbo].[qj_sqsb] ORDER BY sbtime DESC");
            msg.Result = dtBM.Rows[0]["sbdata"].ToString().Replace("灌溉", "");



            DataTable dtggjl = new JH_Auth_UserB().GetDTByCommand("SELECT TOP 5  GLZNAME, XM,ZJE,DB,ZSL  FROM vwSBData  ORDER BY CRDate DESC");
            msg.Result1 = dtggjl;



            //本日
            string br = DateTime.Now.ToString("yyyyMMdd");
            DataTable dtbr = new JH_Auth_UserB().GetDTByCommand(" SELECT  ISNULL(SUM(ZSL),0) AS ZSL,ISNULL(SUM(TDMS),0) AS TDMS,ISNULL(SUM(ZJE),0) AS ZJE FROM vwSBData WHERE  LEFT(CONVERT(varchar(100),CRDATE,112),8)  LIKE '%" + br + "%'");
            msg.Result2 = dtbr;

            //本月
            string by = DateTime.Now.ToString("yyyyMM");
            DataTable dtby = new JH_Auth_UserB().GetDTByCommand(" SELECT  ISNULL(SUM(ZSL),0) AS ZSL,ISNULL(SUM(TDMS),0) AS TDMS,ISNULL(SUM(ZJE),0) AS ZJE FROM vwSBData WHERE  LEFT(CONVERT(varchar(100),CRDATE,112),6)  LIKE '%" + by + "%'");
            msg.Result3 = dtby;

            string ND = DateTime.Now.Year.ToString();
            DataTable dtgj = GetGJ(ND);
            msg.Result4 = dtgj;

        }


        /// <summary>
        /// 获取本灌季统计数据
        /// </summary>
        /// <param name="strND"></param>
        /// <returns></returns>
        private DataTable GetGJ(string strND)
        {
            DataTable dtgj = getnddata(strND);
            int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
            int co = 0;
            for (int i = 0; i < dtgj.Rows.Count; i++)
            {
                if (int.Parse(dtgj.Rows[i]["KSSJ"].ToString().Replace("-", "")) < now && now < int.Parse(dtgj.Rows[i]["JSSJ"].ToString().Replace("-", "")))
                {
                    co = i;
                }
            }

            DataTable dtReturn = new JH_Auth_UserB().GetDTByCommand("SELECT  ISNULL(SUM(ZSL) , 0)   AS ZSL, ISNULL(SUM(TDMS) , 0)   AS TDMS, ISNULL(SUM(ZJE) , 0)   AS ZJE FROM vwSBData WHERE  CRDate BETWEEN '" + dtgj.Rows[co]["KSSJ"].ToString() + " 00:00:00' AND '" + dtgj.Rows[co]["JSSJ"].ToString() + " 23:59:59'");

            return dtReturn;
        }


        /// <summary>
        /// 获取当年灌季
        /// </summary>
        /// <returns></returns>
        private string GetGJDate(string strDate = "")
        {
            int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
            if (strDate != "")
            {
                now = int.Parse(strDate.Replace("-", ""));
            }
            int nowyear = int.Parse(now.ToString().Substring(0, 4));
            List<string> ListYear = new List<string>() { nowyear.ToString(), (nowyear - 1).ToString(), (nowyear + 1).ToString() };
            string strGJRQ = "";
            foreach (string year in ListYear)
            {
                DataTable dtgj = getnddata(year);
                if (dtgj.Rows.Count > 0)
                {
                    int co = 0;
                    for (int i = 0; i < dtgj.Rows.Count; i++)
                    {
                        if (int.Parse(dtgj.Rows[i]["KSSJ"].ToString().Replace("-", "")) < now && now < int.Parse(dtgj.Rows[i]["JSSJ"].ToString().Replace("-", "")))
                        {
                            co = i;
                        }
                    }
                    strGJRQ = dtgj.Rows[co]["KSSJ"].ToString() + "," + dtgj.Rows[co]["JSSJ"].ToString() + "," + dtgj.Rows[co]["LB"].ToString() + "," + year;
                }
            }

            return strGJRQ;
        }



        /// <summary>
        /// 灌溉年度统计,年度同比( 石头河)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GGNDTJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string ND = P1;

            DataTable dttj = getnddata(ND);
            DataTable dtBM = new JH_Auth_UserB().GetDTByCommand("select * from jh_auth_branch where Remark2='Y'");

            DataTable dtzlb = new JH_Auth_UserB().GetDTByCommand(" SELECT TYPE AS name, SUM(ZSL) AS value FROM vwSBData  WHERE   CRDate BETWEEN '" + dttj.Rows[0]["KSSJ"].ToString() + " 00:00:00' AND '" + dttj.Rows[2]["JSSJ"].ToString() + " 23:59:59' GROUP BY TYPE");
            msg.Result5 = dtzlb;

            DataTable DT = new DataTable();
            DT.Columns.Add("管理站");
            DT.Columns.Add("冬灌");
            DT.Columns.Add("春灌");
            DT.Columns.Add("夏灌");
            for (int i = 0; i < dtBM.Rows.Count; i++)
            {
                DataRow dr = DT.NewRow();
                dr["管理站"] = dtBM.Rows[i]["DeptName"].ToString();
                dr["冬灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[0]["JSSJ"].ToString(), "1");
                dr["春灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[1]["KSSJ"].ToString(), dttj.Rows[1]["JSSJ"].ToString(), "1");
                dr["夏灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[2]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "1");

                DT.Rows.Add(dr);
            }

            //DataView dv = new DataView(DT);
            //dv.Sort = "id desc";
            //DT = dv.ToTable();
            msg.Result = DT;


            DataTable DT1 = new DataTable();
            DT1.Columns.Add("管理站");
            DT1.Columns.Add("冬灌");
            DT1.Columns.Add("春灌");
            DT1.Columns.Add("夏灌");
            for (int i = 0; i < dtBM.Rows.Count; i++)
            {
                DataRow dr = DT1.NewRow();
                dr["管理站"] = dtBM.Rows[i]["DeptName"].ToString();
                dr["冬灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[0]["JSSJ"].ToString(), "2");
                dr["春灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[1]["KSSJ"].ToString(), dttj.Rows[1]["JSSJ"].ToString(), "2");
                dr["夏灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[2]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "2");

                DT1.Rows.Add(dr);
            }
            msg.Result1 = DT1;


            DataTable DT2 = new DataTable();
            DT2.Columns.Add("管理站");
            DT2.Columns.Add("冬灌");
            DT2.Columns.Add("春灌");
            DT2.Columns.Add("夏灌");
            for (int i = 0; i < dtBM.Rows.Count; i++)
            {
                DataRow dr = DT2.NewRow();
                dr["管理站"] = dtBM.Rows[i]["DeptName"].ToString();
                dr["冬灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[0]["JSSJ"].ToString(), "3");
                dr["春灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[1]["KSSJ"].ToString(), dttj.Rows[1]["JSSJ"].ToString(), "3");
                dr["夏灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[2]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "3");

                DT2.Rows.Add(dr);
            }
            msg.Result2 = DT2;

            //全局用水量，灌溉亩数，金额
            msg.Result3 = getdata("", dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "1") + "," + getdata("", dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "2") + "," + getdata("", dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "3");




            DataTable dtGGND = new JH_Auth_UserB().GetDTByCommand("select * from jh_auth_zidian where class='灌溉年度'  ORDER BY TypeNO ");

            DataTable DT3 = new DataTable();
            DT3.Columns.Add("年度");
            DT3.Columns.Add("用水量");
            DT3.Columns.Add("灌溉亩数");
            DT3.Columns.Add("用水金额");
            for (int i = 0; i < dtGGND.Rows.Count; i++)
            {
                DataRow dr = DT3.NewRow();
                string strND = dtGGND.Rows[i]["TypeNO"].ToString();
                DataTable dttemp = getnddata(strND);

                dr["年度"] = strND;
                dr["用水量"] = getdata("", dttemp.Rows[0]["KSSJ"].ToString(), dttemp.Rows[2]["JSSJ"].ToString(), "1");
                dr["灌溉亩数"] = getdata("", dttemp.Rows[0]["KSSJ"].ToString(), dttemp.Rows[2]["JSSJ"].ToString(), "2");
                dr["用水金额"] = getdata("", dttemp.Rows[0]["KSSJ"].ToString(), dttemp.Rows[2]["JSSJ"].ToString(), "3");

                DT3.Rows.Add(dr);
            }
            msg.Result4 = DT3;


            DataTable dtYD = new DataTable();
            dtYD.Columns.Add("YF");
            for (int i = 1; i < 13; i++)
            {
                DataRow dr = dtYD.NewRow();
                dr["YF"] = i.ToString().PadLeft(2, '0');
                dtYD.Rows.Add(dr);
            }

            for (int i = 0; i < dtBM.Rows.Count; i++)
            {
                string strZName = dtBM.Rows[i]["DeptName"].ToString();
                dtYD.Columns.Add(strZName);
                DataTable dttemp = GetYDTJ(ND, strZName);
                for (int m = 0; m < dtYD.Rows.Count; m++)
                {
                    dtYD.Rows[m][strZName] = dttemp.Rows[m][1].ToString();

                }
            }




            msg.Result6 = dtYD;

        }


        /// <summary>
        /// 获取月度统计
        /// </summary>
        /// <param name="ND"></param>
        /// <returns></returns>
        private DataTable GetYDTJ(string ND, string glzname)
        {
            DataTable DT = new JH_Auth_UserB().GetDTByCommand("  SELECT  LEFT(CONVERT(varchar(100),CRDATE,112),6)   AS YF, SUM(ZSL) AS ZSL FROM vwSBData WHERE GLZNAME like '%" + glzname + "%' AND  CRDATE LIKE '%" + ND + "%'  GROUP BY  LEFT(CONVERT(varchar(100), CRDATE, 112), 6)");
            for (int i = 1; i < 13; i++)
            {
                string temp = ND + i.ToString().PadLeft(2, '0');
                if (DT.Where("YF='" + temp + "'").Rows.Count == 0)
                {
                    DataRow dr = DT.NewRow();
                    dr["YF"] = temp;
                    dr["ZSL"] = "0";

                    DT.Rows.InsertAt(dr, i - 1);
                }

            }
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                DT.Rows[i]["YF"] = DT.Rows[i]["YF"].ToString().Substring(4);
            }
            return DT;
        }

        public DataTable getnddata(string ND)
        {
            DataTable DT = new DataTable();
            DataTable dtOrderReturn = new JH_Auth_UserB().GetDTByCommand("select * from jh_auth_zidian where class='灌溉年度' AND  TypeNO='" + ND + "'");
            if (dtOrderReturn.Rows.Count > 0)
            {
                string strGJData = dtOrderReturn.Rows[0]["TypeName"].ToString();
                JArray querylist = JsonConvert.DeserializeObject(strGJData) as JArray;

                int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));

                DT.Columns.Add("LB");
                DT.Columns.Add("KSSJ");
                DT.Columns.Add("JSSJ");
                DT.Columns.Add("ISNOW");
                foreach (var item in querylist)
                {
                    DataRow dr = DT.NewRow();
                    dr["LB"] = (string)item["col14411"];
                    dr["KSSJ"] = (string)item["col31433"];
                    dr["JSSJ"] = (string)item["col59542"];
                    dr["ISNOW"] = "N";
                    if (int.Parse(dr["KSSJ"].ToString().Replace("-", "")) < now && now < int.Parse(dr["JSSJ"].ToString().Replace("-", "")))
                    {
                        dr["ISNOW"] = "Y";
                    }
                    DT.Rows.Add(dr);
                }
            }

            return DT;
        }


        #endregion

        #region 宝鸡峡
        /// <summary>
        /// 灌溉年度统计,年度同比( 石头河)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GGNDTJBJX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string ND = P1;


            Msg_Result msg1 = CacheHelp.Get(ND + "TJDATA") as Msg_Result;
            if (msg1 == null)
            {

                DataTable dttj = getnddata(ND);
                DataTable dtBM = new JH_Auth_UserB().GetDTByCommand("select * from jh_auth_branch where DeptRoot='1' and deptname like '%总站%'");

                DataTable dtzlb = new JH_Auth_UserB().GetDTByCommand(" SELECT TYPE AS name, SUM(ZSL) AS value FROM vwSBData  WHERE   CRDate BETWEEN '" + dttj.Rows[0]["KSSJ"].ToString() + " 00:00:00' AND '" + dttj.Rows[2]["JSSJ"].ToString() + " 23:59:59' GROUP BY TYPE");
                msg.Result5 = dtzlb;

                DataTable DT = new DataTable();
                DT.Columns.Add("管理站");
                DT.Columns.Add("冬灌");
                DT.Columns.Add("春灌");
                DT.Columns.Add("夏灌");
                for (int i = 0; i < dtBM.Rows.Count; i++)
                {
                    DataRow dr = DT.NewRow();
                    dr["管理站"] = dtBM.Rows[i]["DeptName"].ToString();
                    dr["冬灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[0]["JSSJ"].ToString(), "1", "Y");
                    dr["春灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[1]["KSSJ"].ToString(), dttj.Rows[1]["JSSJ"].ToString(), "1", "Y");
                    dr["夏灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[2]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "1", "Y");

                    DT.Rows.Add(dr);
                }

                //DataView dv = new DataView(DT);
                //dv.Sort = "id desc";
                //DT = dv.ToTable();
                msg.Result = DT;


                DataTable DT1 = new DataTable();
                DT1.Columns.Add("管理站");
                DT1.Columns.Add("冬灌");
                DT1.Columns.Add("春灌");
                DT1.Columns.Add("夏灌");
                for (int i = 0; i < dtBM.Rows.Count; i++)
                {
                    DataRow dr = DT1.NewRow();
                    dr["管理站"] = dtBM.Rows[i]["DeptName"].ToString();
                    dr["冬灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[0]["JSSJ"].ToString(), "2", "Y");
                    dr["春灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[1]["KSSJ"].ToString(), dttj.Rows[1]["JSSJ"].ToString(), "2", "Y");
                    dr["夏灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[2]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "2", "Y");

                    DT1.Rows.Add(dr);
                }
                msg.Result1 = DT1;


                DataTable DT2 = new DataTable();
                DT2.Columns.Add("管理站");
                DT2.Columns.Add("冬灌");
                DT2.Columns.Add("春灌");
                DT2.Columns.Add("夏灌");
                for (int i = 0; i < dtBM.Rows.Count; i++)
                {
                    DataRow dr = DT2.NewRow();
                    dr["管理站"] = dtBM.Rows[i]["DeptName"].ToString();
                    dr["冬灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[0]["JSSJ"].ToString(), "3", "Y");
                    dr["春灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[1]["KSSJ"].ToString(), dttj.Rows[1]["JSSJ"].ToString(), "3", "Y");
                    dr["夏灌"] = getdata(dtBM.Rows[i]["DeptName"].ToString(), dttj.Rows[2]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "3", "Y");
                    DT2.Rows.Add(dr);
                }
                msg.Result2 = DT2;

                //全局用水量，灌溉亩数，金额
                msg.Result3 = getdata("", dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "1", "Y") + "," + getdata("", dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "2", "Y") + "," + getdata("", dttj.Rows[0]["KSSJ"].ToString(), dttj.Rows[2]["JSSJ"].ToString(), "3", "Y");




                DataTable dtGGND = new JH_Auth_UserB().GetDTByCommand("select * from jh_auth_zidian where class='灌溉年度'  ORDER BY TypeNO ");

                DataTable DT3 = new DataTable();
                DT3.Columns.Add("年度");
                DT3.Columns.Add("用水量");
                DT3.Columns.Add("灌溉亩数");
                DT3.Columns.Add("用水金额");
                for (int i = 0; i < dtGGND.Rows.Count; i++)
                {
                    DataRow dr = DT3.NewRow();
                    string strND = dtGGND.Rows[i]["TypeNO"].ToString();
                    DataTable dttemp = getnddata(strND);

                    dr["年度"] = strND;
                    dr["用水量"] = getdata("", dttemp.Rows[0]["KSSJ"].ToString(), dttemp.Rows[2]["JSSJ"].ToString(), "1", "Y");
                    dr["灌溉亩数"] = getdata("", dttemp.Rows[0]["KSSJ"].ToString(), dttemp.Rows[2]["JSSJ"].ToString(), "2", "Y");
                    dr["用水金额"] = getdata("", dttemp.Rows[0]["KSSJ"].ToString(), dttemp.Rows[2]["JSSJ"].ToString(), "3", "Y");

                    DT3.Rows.Add(dr);
                }
                msg.Result4 = DT3;


                DataTable dtYD = new DataTable();
                dtYD.Columns.Add("YF");
                for (int i = 1; i < 13; i++)
                {
                    DataRow dr = dtYD.NewRow();
                    dr["YF"] = i.ToString().PadLeft(2, '0');
                    dtYD.Rows.Add(dr);
                }

                for (int i = 0; i < dtBM.Rows.Count; i++)
                {
                    string strZName = dtBM.Rows[i]["DeptName"].ToString();
                    dtYD.Columns.Add(strZName);
                    DataTable dttemp = GetYDTJ(ND, strZName);
                    for (int m = 0; m < dtYD.Rows.Count; m++)
                    {
                        dtYD.Rows[m][strZName] = dttemp.Rows[m][1].ToString();

                    }
                }
                msg.Result6 = dtYD;
                CacheHelp.Set(ND + "TJDATA", msg);
            }
            else
            {
                msg.Result = msg1.Result;
                msg.Result1 = msg1.Result1;
                msg.Result2 = msg1.Result2;
                msg.Result3 = msg1.Result3;
                msg.Result4 = msg1.Result4;
                msg.Result5 = msg1.Result5;
                msg.Result6 = msg1.Result6;


            }

        }
        #endregion

        #region 微信端公用
        public void WXXGMM(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (P1 != P2)
            {
                msg.ErrorMsg = "2次输入密码必须一致!";
                return;
            }
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP, UserInfo);
            if (dtUser.Rows.Count > 0)
            {
                if (dtUser.Rows[0]["usertype"].ToString() == "用水户")
                {
                    new JH_Auth_QYB().ExsSclarSql(" UPDATE qj_sfjs_jfuser set dlpassword='" + CommonHelp.GetMD5(P1) + "' WHERE sfzbm ='" + dtUser.Rows[0]["username"].ToString() + "'");
                }
                else
                {
                    new JH_Auth_UserB().UpdatePassWord(UserInfo.User.ComId.Value, UserInfo.User.UserName, CommonHelp.GetMD5(P1));

                }
            }

        }

        /// <summary>
        /// 绑定用户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void BDYH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string openid = context.Request("openid");
            string phone = context.Request("phone");

            string strUsername = context.Request("username");
            DataTable dtyh = new DataTable();
            dtyh = new SFJSManage().GetUserInfo(context.Request("username") + "," + context.Request("password"));
            if (dtyh.Rows.Count == 0)
            {
                msg.ErrorMsg = "用户名不存在或密码不正确";
                return;

            }
            else
            {
                if (!dtyh.Rows[0]["usertype"].ToString().Contains("用水户"))
                {
                    msg.ErrorMsg = "用户名或密码不正确";
                    return;

                }
                string strSQL = " UPDATE qj_sfjs_jfuser set wxopenid='" + openid + "', lxdh='" + phone + "' WHERE  sfzbm ='" + strUsername + "'";
                new JH_Auth_QYB().ExsSclarSql(strSQL);

                try
                {
                    JH_Auth_QY qy = new JH_Auth_QYB().GetALLEntities().FirstOrDefault();
                    DataTable dt = new JH_Auth_UserB().GetDTByCommand(" select top 5 *  FROM qj_sfjs_ysdata  where  jlstatus='已审核' and  zfzt='未支付' and sfzhm='" + strUsername + "'  ");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        string ID = dt.Rows[i]["id"].ToString();
                        string sfzhm = dt.Rows[i]["sfzhm"].ToString();
                        //DataTable dtUser=new 
                        DataTable userListjfyh = new JH_Auth_UserB().GetDTByCommand("SELECT  * FROM qj_sfjs_jfuser WHERE sfzbm='" + strUsername + "' ");
                        if (userListjfyh.Rows.Count > 0 && !string.IsNullOrEmpty(userListjfyh.Rows[0]["wxopenid"].ToString()))
                        {

                            string strOpenid = openid;
                            string strTitle = "尊敬的" + dt.Rows[i]["xm"].ToString() + "用户，您本期用水情况如下：";

                            string strRemark = "温馨提示：请您尽快交清本次水费。如有异议，请联系用水地村组管水员或管理站。";
                            string strYHDZ = dt.Rows[i]["cname"].ToString() + "/" + dt.Rows[i]["czname"].ToString();

                            string strYSL = Math.Round((decimal.Parse(dt.Rows[i]["sl1"].ToString()) + decimal.Parse(dt.Rows[i]["sl2"].ToString()))).ToString() + "方";

                            string strSFJE = Math.Round(decimal.Parse(dt.Rows[i]["hjje"].ToString()), 2, MidpointRounding.AwayFromZero) + "元";

                            string strYHH = userListjfyh.Rows[0]["ID"].ToString();

                            new WXGZHHelp().SENDWXMSG(strOpenid, qy.WebSite + "/MOBWeb/" + SFJSHelp.GetMobPath(qy.QYName) + "/indexgzh.html?dataid=" + ID, strTitle, strRemark, strYHDZ, strYSL, strSFJE, strYHH);
                        }


                    }
                }
                catch (Exception ex)
                {

                    CommonHelp.WriteLOG(ex.Message.ToString());
                }



                msg.Result = dtyh;
            }

        }


        /// <summary>
        /// 初始化微信公众号页面使用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETWXUSERINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            CommonHelp.WriteLOG("测试0");
            string code = context.Request("code");
            CommonHelp.WriteLOG("测试1" + code);
            string strOpenid = new WXGZHHelp().GetOpenIDByCode(code);
            CommonHelp.WriteLOG("测试2" + strOpenid);

            if (strOpenid == "CODEISUSERD")
            {
                msg.ErrorMsg = "CODEISUSERD";
                return;
            }
            DataTable dt = new JH_Auth_QYB().GetDTByCommand("select * from qj_sfjs_jfuser where wxopenid='" + strOpenid + "'");

            CommonHelp.WriteLOG("测试3");

            msg.Result = dt.Rows.Count > 0 ? "Y" : "N";
            msg.Result1 = strOpenid;
            if (dt.Rows.Count > 0)
            {
                DataTable dtUser = GetUserInfo(dt.Rows[0]["sfzbm"].ToString() + "," + dt.Rows[0]["dlpassword"].ToString());
                msg.Result2 = dtUser;

            }



        }


        /// <summary>
        /// 取消微信绑定
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void QXBD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strUsername = context.Request("username");
            new JH_Auth_QYB().ExsSclarSql(" UPDATE qj_sfjs_jfuser set wxopenid=null WHERE sfzbm ='" + strUsername + "'");

        }
        #endregion

        #region 交口专用


        /// <summary>
        /// 交口建行对账接口
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void JKDZJK(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strDate = DateTime.Now.AddDays(-1).ToString("yyyy-HH-dd");

            DataTable dtDFKOrder = new JH_Auth_QYB().GetDTByCommand("select * from qj_order where ddzt='等待付款' and CRDate between '" + strDate + " 00:00:00' AND '" + strDate + " 23:59:59' ");
            for (int i = 0; i < dtDFKOrder.Rows.Count; i++)
            {
                string strOrder = dtDFKOrder.Rows[i]["BusinessNo"].ToString();
                string strUrl = "https://ibsbjstar.ccb.com.cn/app/ccbMain?MERCHANTID=105000093994978&BRANCHID=610000000&POSID=055021362&ORDERDATE=&BEGORDERTIME=00:00:00&ENDORDERTIME=23:59:59&ORDERID=" + strOrder + "&QUPWD=ggglj123456&TXCODE=410408&TYPE=0&KIND=1&STATUS=1&SEL_TYPE=3&PAGE=1&OPERATOR=&CHANNEL=&MAC=8a1097d37ce1fcf7784e6ef999e3aadd";
                string strR = CommonHelp.HttpGet(strUrl);
                new JH_Auth_QYB().ExsSclarSql("update qj_order set dddzmx='" + strR + "',dddztime='" + DateTime.Now.ToString() + "' where BusinessNo='" + strOrder + "'");
            }



        }


        /// <summary>
        /// 获取上报初说数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void INITSBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string UP = context.Request("UP");
            DataTable dtUser = GetUserInfo(UP, UserInfo);


            string strType = P2;
            if (strType == "1")
            {
                DataTable dtzz = new JH_Auth_QYB().GetDTByCommand("SELECT DeptName, deptCode  FROM jh_auth_branch WHERE deptname like '%总站%'  and deptcode in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "')");
                msg.Result = dtzz;
            }
            if (strType == "2")
            {
                //DataTable dtglz = new JH_Auth_QYB().GetDTByCommand("SELECT DeptName, deptCode  FROM jh_auth_branch WHERE remark2='Y' and deptcode in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "')");

                DataTable dtglz = new JH_Auth_QYB().GetDTByCommand("SELECT  DISTINCT  zid, zname ,gzqname FROM qj_db WHERE zid in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "')");
                dtglz.Columns.Add("DeptName");
                dtglz.Columns.Add("deptCode");

                for (int i = 0; i < dtglz.Rows.Count; i++)
                {
                    dtglz.Rows[i]["deptCode"] = dtglz.Rows[i]["zid"].ToString() + "-" + i.ToString();
                    dtglz.Rows[i]["DeptName"] = dtglz.Rows[i]["zname"].ToString().Split('/')[1] + "-" + dtglz.Rows[i]["gzqname"].ToString();
                }
                msg.Result = dtglz;
            }
            if (strType == "3")
            {
                string strdbid = dtUser.Rows[0]["dbids"].ToString();
                DataTable dtglz = new JH_Auth_QYB().GetDTByCommand("SELECT id as deptCode,'' as DeptName,did,DBNAme,pname,xzname FROM vw_dbcz where did in ('" + strdbid.ToFormatLike() + "')");

                for (int i = 0; i < dtglz.Rows.Count; i++)
                {
                    dtglz.Rows[i]["pname"] = dtglz.Rows[i]["pname"].ToString().Split('/')[2].ToString();
                }

                DataTable dtr = new JH_Auth_QYB().GetDTByCommand("SELECT DISTINCT did as deptCode,DBNAme as DeptName FROM vw_dbcz where did in ('" + strdbid.ToFormatLike() + "')");
                dtr.Columns.Add("xzdata", Type.GetType("System.Object"));
                for (int i = 0; i < dtr.Rows.Count; i++)
                {
                    string strdid = dtr.Rows[i]["deptCode"].ToString();
                    dtr.Rows[i]["xzdata"] = dtglz.Where(" did='" + strdid + "'");
                }
                msg.Result = dtr;


                ///获取段长人员
                DataTable dtdz = new JH_Auth_QYB().GetDTByCommand("SELECT jh_auth_user.UserName,(jh_auth_branch.DeptName+'/'+ jh_auth_user.UserRealName) AS UserRealName FROM jh_auth_userrole INNER JOIN jh_auth_user ON jh_auth_userrole.UserName=jh_auth_user.UserName INNER JOIN jh_auth_branch ON jh_auth_branch.DeptCode=jh_auth_user.BranchCode WHERE RoleCode='1277' and  jh_auth_branch.DeptCode in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "') ");
                msg.Result1 = dtdz;
            }





        }







        /// <summary>
        /// 获取水质及雨晴信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSZYQ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strYZ = "http://xxfb.mwr.cn/hydroSearch/greatRiver";
            string strSZURL = "http://106.37.208.243:8068/GJZ/Ajax/Publish.ashx?AreaID=&RiverID=&MNName=%25E6%25B2%2599%25E7%258E%258B%25E6%25B8%25A1&PageIndex=-1&PageSize=60&action=getRealDatas";
            try
            {
                string s1 = CommonHelp.HttpGet(strYZ);
                //水情
                JObject sqdata = (JObject)JsonConvert.DeserializeObject(s1);
                //  JObject sqdataR = (JObject)JsonConvert.DeserializeObject((string)sqdata["result"]);
                JArray gldata = (JArray)sqdata["result"]["data"];
                foreach (JObject pro in gldata)
                {
                    string glfield = (string)pro["stnm"].ToString().Trim();
                    string glfield1 = (string)pro["rvnm"].ToString().Trim();
                    string tm = (string)pro["tm"].ToString().Trim();//时间
                    string zl = (string)pro["zl"].ToString().Trim();//流量
                    string wrz = (string)pro["wrz"].ToString().Trim();//警戒
                    string ql = (string)pro["ql"].ToString().Trim();//水位
                    if (glfield == "咸阳" || glfield == "临潼" || glfield == "景村" || glfield == "张家山")
                    {
                        string strSQSQL = "INSERT INTO [qj_shuiqin] ([CRUser], [DCode], [DName], [CRUserName], [CRDate], [intProcessStanceid], [ComID], [liuyu], [xzq], [hemin], [zhanmin], [sqsj], [shuiwei], [liul], [jjsw]) VALUES ('administrator', '1', '灌溉中心', '超级管理员', '" + DateTime.Now.ToString() + "', '0','1', '黄河', '陕西省', '" + glfield1 + "', '" + glfield + "', '" + tm + "', '" + zl + "', '" + ql + "', '" + wrz + "');";
                        DataTable dttmp = new JH_Auth_BranchB().GetDTByCommand("select * from  qj_shuiqin where sqsj='" + tm + "' and  zhanmin='" + glfield + "'");
                        if (dttmp.Rows.Count == 0)
                        {
                            new JH_Auth_BranchB().ExsSclarSql(strSQSQL);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonHelp.WriteLOG("水情" + ex.Message.ToString());
            }


            try
            {
                string s2 = CommonHelp.HttpGet(strSZURL);

                JObject szdata = (JObject)JsonConvert.DeserializeObject(s2);

                //水质
                string[] arr = szdata.Last.ToArray()[0].ToString().Replace("\r\n", "").Replace(" ", "").Split(',');

                string strsjtemp = arr[3].ToString().Trim('"');
                if (strsjtemp.Length == 10)
                {
                    strsjtemp = arr[3].ToString().Trim('"').Substring(0, 5) + " " + arr[3].ToString().Trim('"').Substring(5);
                }

                string strSZSQL = "INSERT INTO [qj_shuizhi] ([CRUser], [DCode], [DName], [CRUserName], [CRDate], [intProcessStanceid], [ComID], [sf], [ly], [dmmc], [szsj], [szlb],[sw], [pH], [rjy], [ddl], [zd], [gmsyzs], [ad], [zl], [zongd], [yls], [cmd],zdqk) VALUES ('administrator', '1', '灌溉中心', '超级管理员', '" + DateTime.Now.ToString() + "', '0', '1', '陕西省', '黄河流域', '渠首站(沙王渡断面)', '" + strsjtemp + "', '" + arr[4].ToString().Trim('"') + "', '" + CommonHelp.RemoveHtml(arr[5].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[6].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[7].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[8].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[9].ToString().Trim('"')) + "'," +
                    " '" + CommonHelp.RemoveHtml(arr[10].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[11].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[12].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[13].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[14].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[15].ToString().Trim('"')) + "', '" + CommonHelp.RemoveHtml(arr[16].ToString().Trim('"')).Trim(']') + "')";

                DataTable dttmp = new JH_Auth_BranchB().GetDTByCommand("select * from  qj_shuizhi where szsj='" + arr[3].ToString() + "'");
                if (dttmp.Rows.Count == 0)
                {
                    new JH_Auth_BranchB().ExsSclarSql(strSZSQL);
                }

            }
            catch (Exception ex)
            {

                CommonHelp.WriteLOG("水质" + ex.Message.ToString());
            }


        }




        /// <summary>
        /// 获取当日水质及雨晴信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSZYQNOW(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strDate = P1;

            DataTable dtsz = new JH_Auth_QYB().GetDTByCommand("SELECT *  FROM qj_shuizhi  WHERE   CRDate BETWEEN '" + strDate + " 00:00:00' AND '" + strDate + " 23:59:59' ");
            msg.Result = dtsz;


            DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT *  FROM qj_shuiqin WHERE   CRDate BETWEEN '" + strDate + " 00:00:00' AND '" + strDate + " 23:59:59' ");
            msg.Result1 = dtsq;

        }

        /// <summary>
        /// 配水站上报数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SBPSZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            if (UserInfo.QYinfo.FinanceLXR == "N")
            {
                msg.ErrorMsg = "当前数据上报功能处于关闭状态!";
                return;
            }
            JArray sbdata = (JArray)JsonConvert.DeserializeObject(P1);
            foreach (var item in sbdata)
            {
                string ID = (string)item["id"] ?? "0";
                DBFactory db = new BI_DB_SourceB().GetDB(0);
                var dt = new Dictionary<string, object>();
                string strdbid = (string)item["zid"].ToString().Split('-')[0].ToString();
                string strdbname = (string)item["zname"].ToString();

                dt.Add("zzname", (string)item["zname"].ToString());
                dt.Add("zzid", (string)item["zid"].ToString().Split('-')[0].ToString());
                dt.Add("shijian", (string)item["sbdate"].ToString());
                dt.Add("ysl", (string)item["hjysl"].ToString());
                dt.Add("drsl", (string)item["jrll"].ToString());
                dt.Add("zt", "待确认");
                dt.Add("ll", (string)item["dkll"].ToString());
                dt.Add("starttime", (string)item["stime"].ToString());
                dt.Add("endtime", (string)item["etime"].ToString());
                dt.Add("czid", (string)item["czid"].ToString());
                dt.Add("czname", (string)item["czname"].ToString());
                dt.Add("remark1", (string)item["beizhu"].ToString());

                dt.Add("sjddata", JsonConvert.SerializeObject(item["sjddata"]));
                dt.Add("remark", P2);
                if (P2 == "3")
                {
                    dt.Add("shr", (string)item["zgdz"].ToString());
                }
                if (ID != "0")
                {
                    dt.Add("id", ID);
                    db.UpdateData(dt, "qj_zzsb");
                }
                else
                {
                    dt.Add("CRUser", UserInfo.User.UserName);
                    dt.Add("DCode", UserInfo.BranchInfo.DeptCode.ToString());
                    dt.Add("zid", UserInfo.BranchInfo.DeptCode.ToString());
                    dt.Add("DName", UserInfo.BranchInfo.DeptName.ToString());
                    dt.Add("CRUserName", UserInfo.User.UserRealName);
                    dt.Add("CRDate", DateTime.Now.ToString());
                    dt.Add("intProcessStanceid", "0");
                    dt.Add("ComID", "10334");
                    int sbid = db.InserData(dt, "qj_zzsb");
                    if (P2 == "3")
                    {
                        JArray prlist = (JArray)item["sjddata"];
                        string strDate = (string)item["sbdate"].ToString();
                        foreach (var itemcz in prlist)
                        {
                            string czname = (string)itemcz["czname"];
                            string czid = (string)itemcz["czid"];
                            string dkll = (string)itemcz["dkll"];
                            string ysl = (string)itemcz["ysl"];

                            var dtcz = new Dictionary<string, object>();
                            dtcz.Add("CRUser", UserInfo.User.UserName);
                            dtcz.Add("sbid", sbid);
                            dtcz.Add("CRDate", DateTime.Now.ToString());

                            dtcz.Add("czname", czname);
                            dtcz.Add("czid", czid);
                            dtcz.Add("ysl", ysl);
                            dtcz.Add("dkll", dkll);
                            dtcz.Add("khdate", strDate);
                            dtcz.Add("dbid", strdbid);
                            dtcz.Add("dbname", strdbname);

                            db.InserData(dtcz, "qj_zzsbcz");
                        }




                    }
                }



            }

        }






        /// <summary>
        /// 撤销上报数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void CANCELSB(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string UP = context.Request("UP");
            DataTable dtUser = GetUserInfo(UP, UserInfo);
            string strids = P1;
            if (dtUser.Rows.Count > 0)
            {
                new JH_Auth_QYB().GetDTByCommand("DELETE qj_zzsb  WHERE id in ('" + strids.ToFormatLike() + "')");
            }

        }


        /// <summary>
        /// 配水站上报历史
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBDATAPSZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string UP = context.Request("UP");
            string zzname = context.Request("zzname");

            DataTable dtUser = GetUserInfo(UP, UserInfo);
            string strUser = dtUser.Rows[0]["username"].ToString();

            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            string strWhere = "";
            if (!string.IsNullOrEmpty(SDATE))
            {
                strWhere = " AND  CRDate BETWEEN '" + SDATE + " 00:00:00' AND  '" + EDATE + " 23:59:59'";
            }
            if (!string.IsNullOrEmpty(zzname))
            {
                strWhere = strWhere + " AND zzname LIKE '%" + zzname + "%'";
            }
            if (dtUser.Rows.Count > 0)
            {

                msg.Result = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM qj_zzsb WHERE zt='待确认' and remark='" + P2 + "' AND   CRUser='" + strUser + "'" + strWhere + "  ORDER BY CRDate DESC");
                msg.Result1 = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM qj_zzsb WHERE zt='已确认' and  remark='" + P2 + "' AND  CRUser='" + strUser + "'" + strWhere + "  ORDER BY CRDate DESC");

            }

        }




        /// <summary>
        /// 获取待确认得上报数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">类型1:配水员上报2:总站上报3:斗长上报</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBDATAQR(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string UP = context.Request("UP");
            string zzname = context.Request("zzname");

            DataTable dtUser = GetUserInfo(UP, UserInfo);
            string strUser = dtUser.Rows[0]["username"].ToString();

            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            string strWhere = "";
            if (!string.IsNullOrEmpty(SDATE))
            {
                strWhere = " AND  CRDate BETWEEN '" + SDATE + " 00:00:00' AND  '" + EDATE + " 23:59:59'";
            }

            if (dtUser.Rows.Count > 0)
            {
                if (P2 == "3")
                {
                    msg.Result = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM qj_zzsb WHERE   remark='3'  AND shr = '" + UserInfo.User.UserName + "' AND zt='待确认' " + strWhere + "  ORDER BY CRDate DESC");
                    msg.Result1 = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM qj_zzsb WHERE  remark='3'  AND shr = '" + UserInfo.User.UserName + "' AND zt='已确认' " + strWhere + "  ORDER BY CRDate DESC");

                }
                else
                {
                    msg.Result = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM qj_zzsb WHERE   remark='" + P2 + "'  AND zzid in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "') AND zt='待确认' " + strWhere + "  ORDER BY CRDate DESC");
                    msg.Result1 = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM qj_zzsb WHERE  remark='" + P2 + "'  AND zzid in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "') AND zt='已确认' " + strWhere + "  ORDER BY CRDate DESC");

                }
            }

        }

        /// <summary>
        /// 确认上报数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void QRSBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string UP = context.Request("UP");
            DataTable dtUser = GetUserInfo(UP, UserInfo);
            string strids = P1;
            if (dtUser.Rows.Count > 0)
            {
                new JH_Auth_QYB().GetDTByCommand("update qj_zzsb set zt='已确认',shr='" + UserInfo.User.UserName + "',shtime='" + DateTime.Now.ToString() + "',shxm='" + UserInfo.User.UserRealName + "' WHERE id in ('" + strids.ToFormatLike() + "')");
            }

        }



        /// <summary>
        /// 获取总站统计数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETGGTJZZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            DataTable dtUser = GetUserInfo("", UserInfo);
            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            DataTable dtzz = new DataTable();
            if (P2 == "1")
            {
                dtzz = new JH_Auth_QYB().GetDTByCommand("SELECT DeptName, '0' AS gksl  FROM jh_auth_branch WHERE deptname like '%总站%'");
            }
            if (P2 == "2")
            {
                dtzz = new JH_Auth_QYB().GetDTByCommand("SELECT  DeptName, '0' AS gksl FROM jh_auth_branch WHERE remark2='Y' and deptcode in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "')");

            }
            for (int i = 0; i < dtzz.Rows.Count; i++)
            {
                string strzzname = dtzz.Rows[i]["DeptName"].ToString();
                dtzz.Rows[i]["gksl"] = getdata(strzzname, SDATE, EDATE, "4", "Y");

            }
            msg.Result = dtzz;
        }



        /// <summary>
        /// 获取管理站统计数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETGGTJGLZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            DataTable dtUser = GetUserInfo("", UserInfo);
            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            DataTable dtzz = new JH_Auth_QYB().GetDTByCommand("SELECT DeptName, '0' AS ysl,'0' AS ggms ,'0' AS zje  FROM jh_auth_branch WHERE deptcode in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "')");
            for (int i = 0; i < dtzz.Rows.Count; i++)
            {
                string strzzname = dtzz.Rows[i]["DeptName"].ToString();
                dtzz.Rows[i]["ysl"] = getdata(strzzname, SDATE, EDATE, "1", "Y");
                dtzz.Rows[i]["ggms"] = getdata(strzzname, SDATE, EDATE, "2", "Y");
                dtzz.Rows[i]["zje"] = getdata(strzzname, SDATE, EDATE, "3", "Y");
            }
            msg.Result = dtzz;
        }


        /// <summary>
        /// //
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETGGTJGLZ1(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            DataTable dtUser = GetUserInfo("", UserInfo);
            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            DataTable dtzz = new JH_Auth_QYB().GetDTByCommand("SELECT DeptName, '0' AS ysl,'0' AS ggms ,'0' AS zje  FROM jh_auth_branch WHERE deptcode in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "')");
            for (int i = 0; i < dtzz.Rows.Count; i++)
            {
                string strzzname = dtzz.Rows[i]["DeptName"].ToString();
                dtzz.Rows[i]["ysl"] = getdata(strzzname, SDATE, EDATE, "1", "Y");
                dtzz.Rows[i]["ggms"] = getdata(strzzname, SDATE, EDATE, "2", "Y");
                dtzz.Rows[i]["zje"] = getdata(strzzname, SDATE, EDATE, "3", "Y");
            }
            msg.Result = dtzz;
        }



        /// <summary>
        /// //获取斗别信息或者村组信息,用于通讯录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETDBCZXX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            DataTable dtUser = GetUserInfo("", UserInfo);
            string type = context.Request("t") ?? "1";
            string zid = context.Request("zid") ?? "";

            if (string.IsNullOrEmpty(zid))
            {
                zid = dtUser.Rows[0]["zid"].ToString();
            }
            DataTable dtr = new DataTable();
            if (type == "1")
            {
                dtr = new JH_Auth_QYB().GetDTByCommand("SELECT qj_db.*,jh_auth_user.UserName,jh_auth_user.mobphone FROM qj_db LEFT JOIN jh_auth_user ON qj_db.dgly=jh_auth_user.UserName where (qj_db.dbname like '" + P1 + "%' OR jh_auth_user.UserRealName like '" + P1 + "%') and  qj_db.zid='" + zid + "' ORDER BY mobphone DESC");
            }
            else
            {
                dtr = new JH_Auth_QYB().GetDTByCommand("SELECT qj_cz.*,jh_auth_user.UserName,jh_auth_user.mobphone FROM qj_cz LEFT JOIN jh_auth_user ON qj_cz.czgly=jh_auth_user.UserName  where  (qj_cz.xzname like '" + P1 + "%' OR jh_auth_user.UserRealName like '" + P1 + "%')   and  qj_cz.zid='" + zid + "' ORDER BY mobphone DESC");

            }

            msg.Result = dtr;
        }


        /// <summary>
        ///获取配水记录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETDBPSJL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            DataTable dtUser = GetUserInfo("", UserInfo);
            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            string type = context.Request("t") ?? "1";
            string zid = context.Request("zid") ?? "";
            if (string.IsNullOrEmpty(zid))
            {
                zid = dtUser.Rows[0]["zid"].ToString();
            }
            DataTable dtr = new DataTable();
            SDATE = SDATE + " 00:00:00";
            EDATE = EDATE + " 23:59:59";

            if (type == "1")
            {
                //管理站查看
                dtr = new JH_Auth_QYB().GetDTByCommand("SELECT zzid,zzname,ll,drsl, shijian,qj_zzsb.CRDate,qj_zzsb.CRUser,qj_zzsb.CRUserName  from qj_zzsb INNER JOIN qj_db ON qj_zzsb.zzid=qj_db.ID  where  qj_db.zid='" + zid + "' AND  qj_zzsb.remark='3'   AND shijian BETWEEN '" + SDATE + "' AND '" + EDATE + "'");
            }
            if (type == "2")
            {
                //总站查看
                dtr = new JH_Auth_QYB().GetDTByCommand("SELECT zzid,zzname,ll,drsl, shijian,qj_zzsb.CRDate,qj_zzsb.CRUser,qj_zzsb.CRUserName  from qj_zzsb   where  zzid in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "') AND  qj_zzsb.remark='2'  AND shijian BETWEEN '" + SDATE + "' AND '" + EDATE + "'");
            }
            if (type == "3")
            {
                //配水站查看
                dtr = new JH_Auth_QYB().GetDTByCommand("SELECT zzid,zzname,ll, drsl,shijian,qj_zzsb.CRDate,qj_zzsb.CRUser,qj_zzsb.CRUserName  from qj_zzsb  where  qj_zzsb.remark='1'  AND shijian BETWEEN '" + SDATE + "' AND '" + EDATE + "'");
            }
            msg.Result = dtr;
        }


        /// <summary>
        /// 初始化斗长用水申请数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void INITDZSBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            DataTable dtUser = GetUserInfo("", UserInfo);
            string strDIDS = dtUser.Rows[0]["dbids"].ToString();
            DataTable dtdb = new DataTable();
            dtdb = new JH_Auth_QYB().GetDTByCommand("SELECT id as did,(gzqname+'/'+dbname) AS dname  FROM qj_db WHERE ID IN ('" + strDIDS.ToFormatLike() + "')");

            msg.Result = dtdb;
        }



        /// <summary>
        /// 斗长用水申请
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SBDZYSSQ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            if (UserInfo.QYinfo.FinanceLXR == "N")
            {
                msg.ErrorMsg = "当前数据上报功能处于关闭状态!";
                return;
            }


            DBFactory db = new BI_DB_SourceB().GetDB(0);
            var dt = new Dictionary<string, object>();
            dt.Add("CRUser", UserInfo.User.UserName);
            dt.Add("DCode", UserInfo.BranchInfo.DeptCode.ToString());
            dt.Add("DName", UserInfo.BranchInfo.DeptName.ToString());
            dt.Add("CRUserName", UserInfo.User.UserRealName);
            dt.Add("CRDate", DateTime.Now.ToString());
            dt.Add("intProcessStanceid", "0");
            dt.Add("ComID", "10334");
            dt.Add("did", context.Request("did"));
            dt.Add("dbname", context.Request("dbname"));
            dt.Add("lx", context.Request("lx"));
            dt.Add("sqrq", context.Request("sqrq"));
            dt.Add("sqll", context.Request("sqll"));
            db.InserData(dt, "qj_ysxq ");

        }



        /// <summary>
        /// 获取斗长用水申请
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETDZSBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            DataTable dtUser = GetUserInfo("", UserInfo);
            string strDIDS = dtUser.Rows[0]["dbids"].ToString();
            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            string dbname = context.Request("zzname") ?? "";

            string strWhere = "";
            if (dtUser.Rows[0]["usertype"].ToString().Contains("斗长"))
            {
                strWhere = " and  qj_ysxq.CRUser='" + UserInfo.User.UserName + "' ";
            }
            DataTable dtdby = new DataTable();
            dtdby = new JH_Auth_QYB().GetDTByCommand("SELECT qj_ysxq.*,qj_db.zid,qj_db.zname FROM qj_ysxq LEFT JOIN qj_db ON  qj_ysxq.did=qj_db.ID  WHERE  lx='用水' and sqrq BETWEEN '" + SDATE + "' AND '" + EDATE + "' " + strWhere);

            DataTable dtdbt = new DataTable();
            dtdbt = new JH_Auth_QYB().GetDTByCommand("SELECT qj_ysxq.*,qj_db.zid,qj_db.zname FROM qj_ysxq LEFT JOIN qj_db ON  qj_ysxq.did=qj_db.ID  WHERE   lx='退水' and  sqrq BETWEEN '" + SDATE + "' AND '" + EDATE + "'" + strWhere);

            msg.Result = dtdby;
            msg.Result1 = dtdbt;

        }




        /// <summary>
        /// 用水需求统计
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYSXQTJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string glzid = context.Request("glz") ?? "";
            string SDATE = context.Request("tjrq") ?? "";
            if (P1 == "1")
            {
                //管理站统计
                string[] colsHeJi = new string[] { "YSLL", "TSLL" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT gqname,dqname,YSLL,TSLL,sqrq,CRUserName from  vw_dbysxq   WHERE zid='" + glzid + "' AND tjrq ='" + SDATE + "' ");
                CommonHelp.dbDataTableSubSumRowsWithColList(dtsq, "gqname", "dqname", colsHeJi);

                msg.Result = dtsq;
            }
            if (P1 == "2")
            {
                //总站统计
                string[] colsHeJi = new string[] { "YSLL", "TSLL" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT glzname, gqname, SUM(Convert(decimal(18, 1), YSLL)) AS YSLL, SUM(Convert(decimal(18, 1), TSLL)) AS TSLL , (SUM(Convert(decimal(18, 1), YSLL))-SUM(Convert(decimal(18, 1), TSLL))) as xqfx from  vw_dbysxq   WHERE zzname='" + glzid + "' AND tjrq  ='" + SDATE + "' GROUP BY glzname, gqname ");
                CommonHelp.dbDataTableSubSumRowsWithColList(dtsq, "glzname", "gqname", colsHeJi);

                msg.Result = dtsq;
            }
            if (P1 == "3")
            {
                //配水站统计
                string[] colsHeJi = new string[] { "YSLL", "TSLL" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT  zzname, glzname, SUM(Convert(decimal(18, 1), YSLL)) AS YSLL, SUM(Convert(decimal(18, 1), TSLL)) AS TSLL, (SUM(Convert(decimal(18, 1), YSLL))-SUM(Convert(decimal(18, 1), TSLL))) as xqfx  from  vw_dbysxq  WHERE  tjrq  ='" + SDATE + "' GROUP BY zzname,glzname ");
                CommonHelp.dbDataTableSubSumRowsWithColList(dtsq, "zzname", "glzname", colsHeJi);
                msg.Result = dtsq;
            }
        }







        /// <summary>
        /// 用水需求统计
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETPSTJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string glzid = context.Request("glz") ?? "";
            string SDATE = context.Request("tjrq") ?? "";
            //string strSE = GetGJDate();

            //string strSDate = strSE.Split(',')[0];
            //string strEDate = strSE.Split(',')[1];
            if (P1 == "1")
            {
                //管理站统计
                string[] colsHeJi = new string[] { "ll" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT   substring(qj_zzsb.zzname,0,charindex('/',qj_zzsb.zzname))   AS gqname,reverse(substring(reverse(qj_zzsb.zzname),1,charindex('/',reverse(qj_zzsb.zzname)) - 1)) AS dqname,ll,shijian,qj_zzsb.CRUserName,qj_zzsb.CRUser FROM qj_zzsb LEFT JOIN qj_db on qj_zzsb.zzid=qj_db.ID WHERE qj_zzsb.remark='3'  AND qj_db.zid='" + glzid + "' AND CONVERT(VARCHAR(10),shijian,120)  ='" + SDATE + "' ");
                CommonHelp.dbDataTableSubSumRowsWithColList(dtsq, "gqname", "dqname", colsHeJi);
                msg.Result = dtsq;
            }
            if (P1 == "2")
            {
                //总站统计
                string[] colsHeJi = new string[] { "ll", "dkljll" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT   glzname,gqname,CRUserName,CRUser, SUM(Convert(decimal(18, 1), ll)) AS LL,'0' AS dkljll FROM (SELECT  substring(qj_zzsb.zzname,0,charindex('-',qj_zzsb.zzname))   AS glzname, reverse(substring(reverse(qj_zzsb.zzname),1,charindex('-',reverse(qj_zzsb.zzname)) - 1)) AS gqname, qj_zzsb.CRUserName,qj_zzsb.CRUser, ll  FROM qj_zzsb LEFT JOIN jh_auth_branch on qj_zzsb.zzid=jh_auth_branch.DeptCode WHERE qj_zzsb.remark='2'   AND CONVERT(VARCHAR(10),shijian,120)  ='" + SDATE + "' and jh_auth_branch.deptroot='" + glzid + "' ) A GROUP BY glzname,gqname,CRUserName,CRUser ");

                for (int i = 0; i < dtsq.Rows.Count; i++)
                {
                    string glzname = dtsq.Rows[i]["glzname"].ToString();
                    string gqname = dtsq.Rows[i]["gqname"].ToString();
                    DataTable dtdkhj = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ll)),0) AS LL  FROM qj_zzsb INNER JOIN qj_db ON qj_zzsb.zzid=qj_db.ID   WHERE qj_zzsb.remark='3'  AND qj_db.zname like '%/" + glzname + "%'  AND  ZZNAME LIKE '" + gqname + "%'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + SDATE + "' ");
                    dtsq.Rows[i]["dkljll"] = dtdkhj.Rows[0][0].ToString();
                }

                CommonHelp.dbDataTableSubSumRowsWithColList(dtsq, "glzname", "gqname", colsHeJi);

                msg.Result = dtsq;
            }
            if (P1 == "3")
            {

                //配水站统计
                string[] colsHeJi = new string[] { "ll", "zkljll", "dkljll" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT zzname ,'' AS zzid ,SUM(Convert(decimal(18, 1), ll)) AS LL,'0' AS zkljll,'0' AS dkljll FROM qj_zzsb  WHERE qj_zzsb.remark='1'   AND CONVERT(VARCHAR(10),shijian,120)  ='" + SDATE + "' GROUP BY zzname ");
                for (int i = 0; i < dtsq.Rows.Count; i++)
                {
                    string zzname = dtsq.Rows[i]["zzname"].ToString();
                    DataTable dtzztemp = getZZ(zzname);
                    dtsq.Rows[i]["zzid"] = dtzztemp.Rows[0]["deptcode"].ToString();
                    string zzid = dtsq.Rows[i]["zzid"].ToString();

                    DataTable zkljll = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ll)),0) AS LL  FROM qj_zzsb  LEFT JOIN jh_auth_branch ON qj_zzsb.zzid=jh_auth_branch.DeptCode  WHERE qj_zzsb.remark='2'   AND jh_auth_branch.DeptRoot='" + zzid + "'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + SDATE + "'");
                    dtsq.Rows[i]["zkljll"] = zkljll.Rows[0][0].ToString();

                    DataTable dtdkhj = new JH_Auth_QYB().GetDTByCommand("SELECT ISNULL(SUM(Convert(decimal(18, 1), ll)),0) AS LL FROM qj_zzsb LEFT JOIN qj_db ON qj_zzsb.zzid=qj_db.ID LEFT JOIN jh_auth_branch ON qj_db.zid=jh_auth_branch.DeptCode   WHERE qj_zzsb.remark='3'    AND jh_auth_branch.DeptRoot='" + zzid + "'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + SDATE + "' ");
                    dtsq.Rows[i]["dkljll"] = dtdkhj.Rows[0][0].ToString();
                }
                CommonHelp.dbDataTableSumRowsWithColList(dtsq, "zzname", colsHeJi);
                msg.Result = dtsq;
            }
        }


        /// <summary>
        /// 用水量统计
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYSLTJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSDate = context.Request("sdate") ?? "";
            string glzid = context.Request("glz") ?? "";

            string EDATE = context.Request("tjrq") ?? "";
            if (strSDate == "")
            {
                string strSE = GetGJDate(EDATE);
                strSDate = strSE.Split(',')[0];
                string strEDate = strSE.Split(',')[1];
                string LB = strSE.Split(',')[2];
                string year = strSE.Split(',')[3];
            }
            if (P1 == "1")
            {
                //管理站统计
                string[] colsHeJi = new string[] { "drdksl", "ljdksl" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT  zzid as dbid, substring(qj_zzsb.zzname,0,charindex('/',qj_zzsb.zzname))   AS gqname,reverse(substring(reverse(qj_zzsb.zzname),1,charindex('/',reverse(qj_zzsb.zzname)) - 1)) AS dqname, ysl as drdksl,'0' as ljdksl,shijian,qj_zzsb.CRUserName,qj_zzsb.CRUser FROM qj_zzsb LEFT JOIN qj_db on qj_zzsb.zzid=qj_db.ID WHERE qj_zzsb.remark='3'  AND qj_db.zid in ('" + glzid + "') AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");

                for (int i = 0; i < dtsq.Rows.Count; i++)
                {
                    string dbid = dtsq.Rows[i]["dbid"].ToString();
                    DataTable dttemp = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb    WHERE qj_zzsb.remark='3'  AND  zzid='" + dbid + "'  AND  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' ");
                    dtsq.Rows[i]["ljdksl"] = dttemp.Rows[0][0].ToString();
                }

                CommonHelp.dbDataTableSubSumRowsWithColList(dtsq, "gqname", "dqname", colsHeJi);
                msg.Result = dtsq;
            }
            if (P1 == "2")
            {
                //总站统计
                string[] colsHeJi = new string[] { "drzksl", "ljzksl", "drdksl", "ljdksl" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT   glzid,glzname,gqname,SUM(Convert(decimal(18, 1), ysl)) AS ljzksl,'0' AS drzksl,'0' AS drdksl,'0' AS ljdksl FROM (SELECT zzid as glzid, substring(qj_zzsb.zzname,0,charindex('-',qj_zzsb.zzname))   AS glzname, reverse(substring(reverse(qj_zzsb.zzname),1,charindex('-',reverse(qj_zzsb.zzname)) - 1)) AS gqname, qj_zzsb.CRUserName,qj_zzsb.CRUser, ysl  FROM qj_zzsb LEFT JOIN jh_auth_branch on qj_zzsb.zzid=jh_auth_branch.DeptCode WHERE qj_zzsb.remark='2'   AND  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59'   and jh_auth_branch.deptroot in ('" + glzid + "' )) A GROUP BY glzid,glzname,gqname ");

                for (int i = 0; i < dtsq.Rows.Count; i++)
                {
                    string glz = dtsq.Rows[i]["glzid"].ToString();
                    string glzname = dtsq.Rows[i]["glzname"].ToString();
                    string gqname = dtsq.Rows[i]["gqname"].ToString();
                    DataTable dttemp = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb    WHERE qj_zzsb.remark='3'  AND  zzid  ='" + glz + "' AND  ZZNAME LIKE '" + gqname + "%'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");
                    dtsq.Rows[i]["drzksl"] = dttemp.Rows[0][0].ToString();


                    DataTable dtdkhj = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb INNER JOIN qj_db ON qj_zzsb.zzid=qj_db.ID   WHERE qj_zzsb.remark='3'  AND qj_db.zname like '%/" + glzname + "%'  AND  ZZNAME LIKE '" + gqname + "%'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");
                    dtsq.Rows[i]["drdksl"] = dtdkhj.Rows[0][0].ToString();

                    DataTable dtdkhj1 = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb INNER JOIN qj_db ON qj_zzsb.zzid=qj_db.ID   WHERE qj_zzsb.remark='3'  AND qj_db.zname like '%/" + glzname + "%'  AND  ZZNAME LIKE '" + gqname + "%' and  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' ");
                    dtsq.Rows[i]["ljdksl"] = dtdkhj1.Rows[0][0].ToString();

                }

                CommonHelp.dbDataTableSubSumRowsWithColList(dtsq, "gqname", "glzname", colsHeJi);

                msg.Result = dtsq;
            }
            if (P1 == "3")
            {

                //配水站统计
                string[] colsHeJi = new string[] { "drgksl", "ljgksl", "drzksl", "ljzksl", "drdksl", "ljdksl" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT zzname ,'' AS zzid ,SUM(Convert(decimal(18, 1), ysl)) AS ljgksl,'0' AS drgksl,'0' AS drzksl,'0' AS ljzksl,'0' AS drdksl,'0' AS ljdksl FROM qj_zzsb  WHERE qj_zzsb.remark='1'   AND  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' GROUP BY zzname ");
                for (int i = 0; i < dtsq.Rows.Count; i++)
                {
                    string zzname = dtsq.Rows[i]["zzname"].ToString();
                    DataTable dtzztemp = getZZ(zzname);
                    dtsq.Rows[i]["zzid"] = dtzztemp.Rows[0]["deptcode"].ToString();
                    string zzid = dtsq.Rows[i]["zzid"].ToString();

                    DataTable dttemp = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb    WHERE qj_zzsb.remark='1'  AND  zzname='" + zzname + "'  AND  CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");
                    dtsq.Rows[i]["drgksl"] = dttemp.Rows[0][0].ToString();

                    DataTable zkljll = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb  LEFT JOIN jh_auth_branch ON qj_zzsb.zzid=jh_auth_branch.DeptCode  WHERE qj_zzsb.remark='2'   AND jh_auth_branch.DeptRoot='" + zzid + "'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "'");
                    dtsq.Rows[i]["drzksl"] = zkljll.Rows[0][0].ToString();

                    DataTable zkljll1 = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb  LEFT JOIN jh_auth_branch ON qj_zzsb.zzid=jh_auth_branch.DeptCode  WHERE qj_zzsb.remark='2'   AND jh_auth_branch.DeptRoot='" + zzid + "'  and  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' ");
                    dtsq.Rows[i]["ljzksl"] = zkljll1.Rows[0][0].ToString();

                    DataTable dtdkhj = new JH_Auth_QYB().GetDTByCommand("SELECT ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl FROM qj_zzsb LEFT JOIN qj_db ON qj_zzsb.zzid=qj_db.ID LEFT JOIN jh_auth_branch ON qj_db.zid=jh_auth_branch.DeptCode   WHERE qj_zzsb.remark='3'    AND jh_auth_branch.DeptRoot='" + zzid + "'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");
                    dtsq.Rows[i]["drdksl"] = dtdkhj.Rows[0][0].ToString();

                    DataTable dtdkhj1 = new JH_Auth_QYB().GetDTByCommand("SELECT ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl FROM qj_zzsb LEFT JOIN qj_db ON qj_zzsb.zzid=qj_db.ID LEFT JOIN jh_auth_branch ON qj_db.zid=jh_auth_branch.DeptCode   WHERE qj_zzsb.remark='3'    AND jh_auth_branch.DeptRoot='" + zzid + "'  and  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' ");
                    dtsq.Rows[i]["ljdksl"] = dtdkhj1.Rows[0][0].ToString();
                }
                CommonHelp.dbDataTableSumRowsWithColList(dtsq, "zzname", colsHeJi);
                msg.Result = dtsq;
            }
        }

        public void GETYSLTJYD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSDate = context.Request("sdate") ?? "";
            string glzid = context.Request("glz") ?? "";

            string EDATE = context.Request("tjrq") ?? "";
            if (strSDate == "")
            {
                string strSE = GetGJDate(EDATE);
                strSDate = strSE.Split(',')[0];
                string strEDate = strSE.Split(',')[1];
                string LB = strSE.Split(',')[2];
                string year = strSE.Split(',')[3];
            }
            if (P1 == "1")
            {
                //管理站统计
                string[] colsHeJi = new string[] { "drdksl", "ljdksl" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT  zzid as dbid, substring(qj_zzsb.zzname,0,charindex('/',qj_zzsb.zzname))   AS gqname,reverse(substring(reverse(qj_zzsb.zzname),1,charindex('/',reverse(qj_zzsb.zzname)) - 1)) AS dqname, ysl as drdksl,'0' as ljdksl,shijian,qj_zzsb.CRUserName,qj_zzsb.CRUser FROM qj_zzsb LEFT JOIN qj_db on qj_zzsb.zzid=qj_db.ID WHERE qj_zzsb.remark='3'  AND qj_db.zid in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "') AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");

                for (int i = 0; i < dtsq.Rows.Count; i++)
                {
                    string dbid = dtsq.Rows[i]["dbid"].ToString();
                    DataTable dttemp = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb    WHERE qj_zzsb.remark='3'  AND  zzid='" + dbid + "'  AND  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' ");
                    dtsq.Rows[i]["ljdksl"] = dttemp.Rows[0][0].ToString();
                }

                CommonHelp.dbDataTableSubSumRowsWithColList(dtsq, "gqname", "dqname", colsHeJi);
                msg.Result = dtsq;
            }
            if (P1 == "2")
            {
                //总站统计
                string[] colsHeJi = new string[] { "drzksl", "ljzksl", "drdksl", "ljdksl" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT   glzid,glzname,gqname,SUM(Convert(decimal(18, 1), ysl)) AS ljzksl,'0' AS drzksl,'0' AS drdksl,'0' AS ljdksl FROM (SELECT zzid as glzid, substring(qj_zzsb.zzname,0,charindex('-',qj_zzsb.zzname))   AS glzname, reverse(substring(reverse(qj_zzsb.zzname),1,charindex('-',reverse(qj_zzsb.zzname)) - 1)) AS gqname, qj_zzsb.CRUserName,qj_zzsb.CRUser, ysl  FROM qj_zzsb LEFT JOIN jh_auth_branch on qj_zzsb.zzid=jh_auth_branch.DeptCode WHERE qj_zzsb.remark='2'   AND  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59'   and jh_auth_branch.deptroot in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "' )) A GROUP BY glzid,glzname,gqname ");

                for (int i = 0; i < dtsq.Rows.Count; i++)
                {
                    string glz = dtsq.Rows[i]["glzid"].ToString();
                    string glzname = dtsq.Rows[i]["glzname"].ToString();
                    string gqname = dtsq.Rows[i]["gqname"].ToString();
                    DataTable dttemp = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb    WHERE qj_zzsb.remark='3'  AND  zzid  ='" + glz + "' AND  ZZNAME LIKE '" + gqname + "%'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");
                    dtsq.Rows[i]["drzksl"] = dttemp.Rows[0][0].ToString();


                    DataTable dtdkhj = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb INNER JOIN qj_db ON qj_zzsb.zzid=qj_db.ID   WHERE qj_zzsb.remark='3'  AND qj_db.zname like '%/" + glzname + "%'  AND  ZZNAME LIKE '" + gqname + "%'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");
                    dtsq.Rows[i]["drdksl"] = dtdkhj.Rows[0][0].ToString();

                    DataTable dtdkhj1 = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb INNER JOIN qj_db ON qj_zzsb.zzid=qj_db.ID   WHERE qj_zzsb.remark='3'  AND qj_db.zname like '%/" + glzname + "%'  AND  ZZNAME LIKE '" + gqname + "%' and  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' ");
                    dtsq.Rows[i]["ljdksl"] = dtdkhj1.Rows[0][0].ToString();

                }

                CommonHelp.dbDataTableSubSumRowsWithColList(dtsq, "gqname", "glzname", colsHeJi);

                msg.Result = dtsq;
            }
            if (P1 == "3")
            {

                //配水站统计
                string[] colsHeJi = new string[] { "drgksl", "ljgksl", "drzksl", "ljzksl", "drdksl", "ljdksl" };
                DataTable dtsq = new JH_Auth_QYB().GetDTByCommand("SELECT zzname ,'' AS zzid ,SUM(Convert(decimal(18, 1), ysl)) AS ljgksl,'0' AS drgksl,'0' AS drzksl,'0' AS ljzksl,'0' AS drdksl,'0' AS ljdksl FROM qj_zzsb  WHERE qj_zzsb.remark='1'   AND  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' GROUP BY zzname ");
                for (int i = 0; i < dtsq.Rows.Count; i++)
                {
                    string zzname = dtsq.Rows[i]["zzname"].ToString();
                    DataTable dtzztemp = getZZ(zzname);
                    dtsq.Rows[i]["zzid"] = dtzztemp.Rows[0]["deptcode"].ToString();
                    string zzid = dtsq.Rows[i]["zzid"].ToString();

                    DataTable dttemp = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb    WHERE qj_zzsb.remark='1'  AND  zzname='" + zzname + "'  AND  CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");
                    dtsq.Rows[i]["drgksl"] = dttemp.Rows[0][0].ToString();

                    DataTable zkljll = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb  LEFT JOIN jh_auth_branch ON qj_zzsb.zzid=jh_auth_branch.DeptCode  WHERE qj_zzsb.remark='2'   AND jh_auth_branch.DeptRoot='" + zzid + "'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "'");
                    dtsq.Rows[i]["drzksl"] = zkljll.Rows[0][0].ToString();

                    DataTable zkljll1 = new JH_Auth_QYB().GetDTByCommand("SELECT   ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl  FROM qj_zzsb  LEFT JOIN jh_auth_branch ON qj_zzsb.zzid=jh_auth_branch.DeptCode  WHERE qj_zzsb.remark='2'   AND jh_auth_branch.DeptRoot='" + zzid + "'  and  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' ");
                    dtsq.Rows[i]["ljzksl"] = zkljll1.Rows[0][0].ToString();

                    DataTable dtdkhj = new JH_Auth_QYB().GetDTByCommand("SELECT ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl FROM qj_zzsb LEFT JOIN qj_db ON qj_zzsb.zzid=qj_db.ID LEFT JOIN jh_auth_branch ON qj_db.zid=jh_auth_branch.DeptCode   WHERE qj_zzsb.remark='3'    AND jh_auth_branch.DeptRoot='" + zzid + "'  AND CONVERT(VARCHAR(10),shijian,120)  ='" + EDATE + "' ");
                    dtsq.Rows[i]["drdksl"] = dtdkhj.Rows[0][0].ToString();

                    DataTable dtdkhj1 = new JH_Auth_QYB().GetDTByCommand("SELECT ISNULL(SUM(Convert(decimal(18, 1), ysl)),0) AS ysl FROM qj_zzsb LEFT JOIN qj_db ON qj_zzsb.zzid=qj_db.ID LEFT JOIN jh_auth_branch ON qj_db.zid=jh_auth_branch.DeptCode   WHERE qj_zzsb.remark='3'    AND jh_auth_branch.DeptRoot='" + zzid + "'  and  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + EDATE + " 23:59:59' ");
                    dtsq.Rows[i]["ljdksl"] = dtdkhj1.Rows[0][0].ToString();
                }
                CommonHelp.dbDataTableSumRowsWithColList(dtsq, "zzname", colsHeJi);
                msg.Result = dtsq;
            }
        }

        /// <summary>
        /// 用水量统计(移动端)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYDYSTJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            DataTable dtUser = GetUserInfo("", UserInfo);
            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            DataTable dtzz = new DataTable();

            SDATE = SDATE + " 00:00:00";
            EDATE = EDATE + " 23:59:59";
            if (P2 == "1")
            {
                //管理站
                dtzz = new JH_Auth_QYB().GetDTByCommand("SELECT dbname as danwei, ISNULL(SUM(Convert(decimal(18, 1), ysll)),0) AS ysll, ISNULL(SUM(Convert(decimal(18, 1), tsll)),0) AS tsll  FROM vw_dbysxq  where  zid in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "')  and sqrq BETWEEN '" + SDATE + "' and '" + EDATE + "' GROUP BY  dbname");

            }
            if (P2 == "2")
            {
                //统计管理站
                dtzz = new JH_Auth_QYB().GetDTByCommand("SELECT glzname as danwei, ISNULL(SUM(Convert(decimal(18, 1), ysll)),0) AS ysll, ISNULL(SUM(Convert(decimal(18, 1), tsll)),0) AS tsll  FROM vw_dbysxq  where  zid in ('" + UserInfo.UserBMQXCode.ToFormatLike() + "')  and sqrq BETWEEN '" + SDATE + "' and '" + EDATE + "' GROUP BY  glzname");

            }
            if (P2 == "3")
            {
                //统计总站
                dtzz = new JH_Auth_QYB().GetDTByCommand("SELECT zzname as danwei, ISNULL(SUM(Convert(decimal(18, 1), ysll)),0) AS ysll, ISNULL(SUM(Convert(decimal(18, 1), tsll)),0) AS tsll  FROM vw_dbysxq  where sqrq BETWEEN '" + SDATE + "' and '" + EDATE + "' GROUP BY  zzname");
            }
            msg.Result = dtzz;
        }

        /// <summary>
        /// 获取斗别用水申请
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETDBYSLTJ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            DataTable dtUser = GetUserInfo("", UserInfo);
            string strDIDS = dtUser.Rows[0]["dbids"].ToString();
            string strZID = dtUser.Rows[0]["zid"].ToString();

            string strWhere = "";
            if (dtUser.Rows[0]["usertype"].ToString() == "斗长")
            {
                strWhere = "and qj_db.ID in ('" + strDIDS.ToFormatLike() + "')";
            }

            string SDATE = context.Request("sdate") ?? "";
            string EDATE = context.Request("edate") ?? "";
            string dbname = context.Request("zzname") ?? "";

            DataTable dtdby = new DataTable();
            dtdby = new JH_Auth_QYB().GetDTByCommand(" SELECT qj_db.gzqname, qj_db.dbname, qj_db.ID, SUM(ysl) AS ysl FROM vw_tjst INNER JOIN qj_db ON vw_tjst.dbid=qj_db.ID  WHERE 1=1 " + strWhere + " and  qj_db.zid='" + strZID + "' AND  shijian BETWEEN '" + SDATE + " 00:00:00' AND  '" + EDATE + " 23:59:59'  GROUP BY  qj_db.gzqname, qj_db.dbname, qj_db.ID ORDER BY qj_db.gzqname, qj_db.dbname");
            dtdby.Columns.Add("czs", Type.GetType("System.Object"));

            for (int i = 0; i < dtdby.Rows.Count; i++)
            {
                string strdbid = dtdby.Rows[i]["ID"].ToString();
                DataTable dtTemp = new JH_Auth_QYB().GetDTByCommand(" SELECT   vw_tjst.czname,czid,ISNULL(SUM(Convert(decimal(18, 2), ysl)),0) AS ysl,ISNULL(SUM(Convert(decimal(18, 2), ZSL)),0) AS zsl,ISNULL(SUM(Convert(decimal(18, 2), hjje)),0) AS hjje  FROM vw_tjst WHERE  dbid='" + strdbid + "' AND  shijian BETWEEN '" + SDATE + " 00:00:00' AND  '" + EDATE + " 23:59:59' GROUP BY vw_tjst.czname,czid");
                dtTemp.Columns.Add("yjfy");
                dtTemp.Columns.Add("qjfy");


                for (int m = 0; m < dtTemp.Rows.Count; m++)
                {
                    string czid = dtTemp.Rows[m]["czid"].ToString();
                    string yjfy = GetXZTJData(strdbid, czid, SDATE, EDATE);
                    dtTemp.Rows[m]["yjfy"] = yjfy;
                    dtTemp.Rows[m]["qjfy"] = Math.Round(decimal.Parse(dtTemp.Rows[m]["hjje"].ToString()) - decimal.Parse(yjfy), 2);
                }
                dtdby.Rows[i]["czs"] = dtTemp;
            }

            msg.Result = dtdby;

        }


        /// <summary>
        /// 获取村组已支付金额
        /// </summary>
        /// <param name="strdID"></param>
        /// <param name="strXZID"></param>
        /// <param name="strSDate"></param>
        /// <param name="Edate"></param>
        /// <returns></returns>
        public string GetXZTJData(string strdID, string strXZID, string strSDate, string Edate)
        {
            DataTable dt = new DataTable();
            DataTable dtTemp = new JH_Auth_QYB().GetDTByCommand(" SELECT ISNULL(SUM(Convert(decimal(18, 2), HJJE)),0) AS zfje FROM vw_tjst WHERE czid='" + strXZID + "' AND dbid='" + strdID + "' AND zfzt='已支付' AND  shijian BETWEEN '" + strSDate + " 00:00:00' AND  '" + Edate + " 23:59:59' ");
            return dtTemp.Rows[0][0].ToString();
        }
        #endregion


        #region 石头河专用

        /// <summary>
        /// 获取制度内容
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETZD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dtsz = new JH_Auth_QYB().GetDTByCommand("SELECT *  FROM qj_zhidu where ID='" + P1 + "' ");
            msg.Result = dtsz;
        }



        /// <summary>
        /// 设置管理站协会，小组奖励折扣
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GLZSETJLZK(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strZID = P1;
            string TYPE = P2;//0:计价类别，1：上报资格
            string xzfl = context.Request("xzfl") ?? "";
            string xhfl = context.Request("xhfl") ?? "";
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM JH_Auth_Branch WHERE DeptCode in ('" + strZID.ToFormatLike() + "') ");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string intid = dt.Rows[i]["DeptCode"].ToString();
                new JH_Auth_BranchB().ExsSclarSql("UPDATE JH_Auth_Branch SET xhfl='" + xhfl + "',xzfl='" + xzfl + "' WHERE DeptCode='" + intid + "'");

            }


        }

        #endregion



        #region 支付订单生成
        public void ZFORDER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP, UserInfo);
            if (dtUser.Rows.Count > 0)
            {
                string dataid = context.Request("dataid") ?? "";
                string orderid = context.Request("orderid") ?? "";
                string jfje = context.Request("jfje") ?? "0";
                string strUser = dtUser.Rows[0]["username"].ToString();
                string strXM = dtUser.Rows[0]["userrealname"].ToString();

                string zid = dtUser.Rows[0]["zid"].ToString();
                string zname = dtUser.Rows[0]["zname"].ToString();

                DBFactory db = new BI_DB_SourceB().GetDB(0);
                var dt = new Dictionary<string, object>();
                dt.Add("CRUser", strUser);
                dt.Add("DCode", zid);
                dt.Add("DName", zname);
                dt.Add("CRUserName", strXM);
                dt.Add("CRDate", DateTime.Now.ToString());
                dt.Add("intProcessStanceid", "0");
                dt.Add("ComID", "10334");
                dt.Add("ddje", jfje);
                dt.Add("ddzt", "等待付款");
                dt.Add("fksj", DateTime.Now.ToString());
                dt.Add("fkms", "建行在线");
                dt.Add("fkje", jfje);
                dt.Add("fkr", strUser);
                dt.Add("BusinessNo", orderid);
                dt.Add("mxids", dataid.Trim(','));
                db.InserData(dt, "qj_order");
            }



        }

        /// <summary>
        /// 支付反馈
        /// </summary>
        public void zfjs(string strOrder, string strzfje, string strzffs)
        {
            DataTable dtOrder = new JH_Auth_QYB().GetDTByCommand("select * from qj_order where BusinessNo='" + strOrder + "'");
            if (dtOrder.Rows.Count > 0)
            {
                string strOrderMXID = dtOrder.Rows[0]["mxids"].ToString();
                new JH_Auth_QYB().ExsSclarSql("update qj_order set fkje='" + strzfje + "',ddzt='已支付',fkms='" + strzffs + "' ,fksj='" + DateTime.Now.ToString() + "' where BusinessNo='" + strOrder + "'");
                new JH_Auth_QYB().ExsSclarSql("update qj_sfjs_ysdata set  zfzt='已支付',zffs='" + strzffs + "' ,zfsj='" + DateTime.Now.ToString() + "',BusinessNo='" + strOrder + "' where ID IN ('" + strOrderMXID.ToFormatLike() + "')");

            }
        }

        #endregion

    }
    #region 帮助类
    public class TJModel
    {
        public string ZNAME { get; set; }

        public string DCTITLE { get; set; }
        public string DNAME { get; set; }
        public string CNAME { get; set; }
        public string LYDE { get; set; }
        public string JJDE { get; set; }

        public string LYJG1 { get; set; }
        public string LYJG2 { get; set; }
        public string LYJG3 { get; set; }
        public string LYJG4 { get; set; }

        public string JJJG1 { get; set; }
        public string JJJG2 { get; set; }
        public string JJJG3 { get; set; }
        public string JJJG4 { get; set; }

        public string TJDATE { get; set; }
        public string JJNF { get; set; }


        public string Year { get; set; }
        public string GJ { get; set; }

        public string LYJG { get; set; }

        public string JJJG { get; set; }

        public string remark1 { get; set; }
        public string remark2 { get; set; }



    }
    #endregion
}