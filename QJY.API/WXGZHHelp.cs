﻿using QJY.Common;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP.Entities;
using System;

namespace QJY.API
{
    public class WXGZHHelp
    {


        public AccessTokenResult GetWXToken(string corpid, string corpSecret)
        {
            AccessTokenResult Token = new AccessTokenResult();
            Token = CommonApi.GetToken(corpid, corpSecret);
            return Token;
        }
        public string GetOpenIDByCode(string code)
        {
            string wxappid = CommonHelp.GetConfig("wxappid");
            string wxsecret = CommonHelp.GetConfig("wxappsecret");
            OAuthAccessTokenResult result = null;

            //通过，用code换取access_token
            try
            {

                result = OAuthApi.GetAccessToken(wxappid, wxsecret, code);
                CommonHelp.WriteLOG(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                OAuthUserInfo userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                return userInfo.openid;

            }
            catch (Exception ex)
            {
                //msg.ErrorMsg = "CODEISUSERD" + ex.Message;
                string ret = "";
                CommonHelp.WriteLOG(ex.Message);

                if (ex.Message.Contains("40163"))
                {
                    ret = "CODEISUSERD";
                }
                return ret;
            }





        }
        public class ProductTemplateData
        {
            public TemplateDataItem first { get; set; }
            public TemplateDataItem keyword1 { get; set; }
            public TemplateDataItem keyword2 { get; set; }
            public TemplateDataItem keyword3 { get; set; }
            public TemplateDataItem keyword4 { get; set; }

            public TemplateDataItem remark { get; set; }

        }

        /// <summary>
        /// 发送用水信息
        /// </summary>
        /// <param name="Openid"></param>
        /// <param name="PageUrl"></param>
        /// <param name="title"></param>
        public void SENDWXMSG(string Openid, string PageUrl, string title, string remark, string ysdz, string ysl, string sfje, string yhnum)
        {
            try
            {
                string wxappid = CommonHelp.GetConfig("wxappid");
                string wxsecret = CommonHelp.GetConfig("wxappsecret");
                string wxmsgmb = CommonHelp.GetConfig("wxmsgmb");

                if (!string.IsNullOrEmpty(wxmsgmb))
                {
                    AccessTokenResult Token = GetWXToken(wxappid, wxsecret);
                    TemplateModel Msg = new TemplateModel();
                    var templateData = new ProductTemplateData()
                    {
                        first = new TemplateDataItem(title, "#000000"),
                        keyword1 = new TemplateDataItem(yhnum, "#000000"),
                        keyword2 = new TemplateDataItem(ysdz, "#000000"),
                        keyword3 = new TemplateDataItem(ysl, "#000000"),
                        keyword4 = new TemplateDataItem(sfje, "#000000"),
                        remark = new TemplateDataItem(remark, "#173177")
                    };
                    var objdata = TemplateApi.SendTemplateMessage(Token.access_token, Openid, wxmsgmb, PageUrl, templateData);
                }
               
            }
            catch (Exception ex)
            {
                CommonHelp.WriteLOG(ex.Message.ToString());
                throw;
            }

        }
    }
}
